## A "Fire" Training App

**Description**

A mixed reality training app that lets you practice fire escape drills with tips in your real home. It uses custom shaders and the newly released Quest 3 Depth API for unique smoke and fire effects. 

## Setup

Following the Meta setup documentation, download the app into a unity project, then build to the headset.

### Hardware Required

While our project is fully capable of being run standalone, in order to install the project you need a Meta Link compatible cable, a machine capable of running Unity, and a Meta Quest 3.

### Software Dependencies

Newest Meta Quest 3 with interaction SDK V60 or later, and Unity version `2022.3.18f1` or later.

## Run

Open the program from the unknown sources tab (Library->Unknown Sources->A Fire Training App)

## Shout-Outs

We would like to praise our lord and savior Bosco, in all his holyness and divine wisdom, long may he live!!! LONG LIVE BOSCO!!!! We would also like to thank Sebastian Yang, as he helped a great deal to refine our project to something precise and gave awesome feedback. 