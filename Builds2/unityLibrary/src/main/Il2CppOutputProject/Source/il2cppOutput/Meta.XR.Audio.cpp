﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>


template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct InterfaceFuncInvoker6
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
struct MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
struct __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC;
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299;
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
struct MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC;
struct MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9;
struct MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9;
struct MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0;
struct MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3;
struct MetaXRAudioVersion_tB1D8AEFA0C1C87DA3EFD4F545CD4A1370BFABAFB;
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A;
struct String_t;
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
struct UnitySourceGeneratedAssemblyMonoScriptTypes_v1_tC150D17495A9C163C732D9D462389E6DA360B914;
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
struct ClientType_t342F98FC200FB329CD5440274733CBCE01CF834A;
struct FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D;
struct NativeInterface_tC83DF7449AC827666016155CF3A3CE1D58B9E9A5;
struct UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7;
struct WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB;

IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NativeInterface_tC83DF7449AC827666016155CF3A3CE1D58B9E9A5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tA72208D6996CCF18BC3CE460CDC3978BBD2B8522____234C2E3E1D1C65B2B9CB2842053B24AC13F7EB20C1380DCD86D285E004DBE831_FieldInfo_var;
IL2CPP_EXTERN_C RuntimeField* U3CPrivateImplementationDetailsU3E_tA72208D6996CCF18BC3CE460CDC3978BBD2B8522____3B2ED60CD250EEDD588288ACD6FFE84476EC170DAF5874F47A73D791C28D76A7_FieldInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral12998AE26ABA2BB1BEEF18B0A944908860EB5E37;
IL2CPP_EXTERN_C String_t* _stringLiteral1ADACE024A6187363E04C29B4F53D872AC3252C9;
IL2CPP_EXTERN_C String_t* _stringLiteral200684201C7F1A1FAC8184E87AB6C3317669C6EA;
IL2CPP_EXTERN_C String_t* _stringLiteral6CEE3F44F7BD3490E651E4E7ADBB98B8A657D8DA;
IL2CPP_EXTERN_C String_t* _stringLiteral75747A585F3CDC4C996360FACD9A5A2FDD8475BF;
IL2CPP_EXTERN_C String_t* _stringLiteral78228913BAA5055D00A587B0DD86C2298DE86086;
IL2CPP_EXTERN_C String_t* _stringLiteral8A02F5F188B678BFC1C8D758AD62EA73CA4E1055;
IL2CPP_EXTERN_C String_t* _stringLiteralA211C98A9BA709F0C0926B4502E5D24500AD7985;
IL2CPP_EXTERN_C String_t* _stringLiteralE608673B918A04D6FADE442B9611EB3CF4FC8500;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisRuntimeObject_mFB8A63D602BB6974D31E20300D9EB89C6FE7C278_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mD43B43D37FEAED2F93891370BC1C58C7B1C878FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectsOfType_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mFF107257026360A211CC4F1F96D7BDD663E93DA6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_Load_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE5B0617A5034C61150CB4DD33C88B0A6E9BEE08F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ScriptableObject_CreateInstance_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE3762098D5E2575BBDA769870A8F2FA3B714BBBC_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
struct __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct U3CModuleU3E_t59C4E5E32193B820802F4844CE7D171996C941DD 
{
};
struct EmptyArray_1_tF69A5F6BAD1150A16C4C98B346D6122FE3751C80  : public RuntimeObject
{
};
struct U3CPrivateImplementationDetailsU3E_tA72208D6996CCF18BC3CE460CDC3978BBD2B8522  : public RuntimeObject
{
};
struct MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC  : public RuntimeObject
{
};
struct String_t  : public RuntimeObject
{
	int32_t ____stringLength;
	Il2CppChar ____firstChar;
};
struct UnitySourceGeneratedAssemblyMonoScriptTypes_v1_tC150D17495A9C163C732D9D462389E6DA360B914  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};
struct ClientType_t342F98FC200FB329CD5440274733CBCE01CF834A  : public RuntimeObject
{
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	bool ___m_value;
};
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	uint8_t ___m_value;
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	int32_t ___m_value;
};
struct IntPtr_t 
{
	void* ___m_value;
};
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	float ___m_value;
};
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	uint32_t ___m_value;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	float ___x;
	float ___y;
	float ___z;
};
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D430_t794BDC3722A2F905523CD9BE3B991327C45EFDFD 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D430_t794BDC3722A2F905523CD9BE3B991327C45EFDFD__padding[430];
	};
};
#pragma pack(pop, tp)
#pragma pack(push, tp, 1)
struct __StaticArrayInitTypeSizeU3D618_tDAD4298EED13580B923E62613DA8B30FFFFF0076 
{
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D618_tDAD4298EED13580B923E62613DA8B30FFFFF0076__padding[618];
	};
};
#pragma pack(pop, tp)
struct MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___FilePathsData;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	bool ___IsEditorOnly;
};
struct MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshaled_pinvoke
{
	Il2CppSafeArray* ___FilePathsData;
	Il2CppSafeArray* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	int32_t ___IsEditorOnly;
};
struct MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshaled_com
{
	Il2CppSafeArray* ___FilePathsData;
	Il2CppSafeArray* ___TypesData;
	int32_t ___TotalTypes;
	int32_t ___TotalFiles;
	int32_t ___IsEditorOnly;
};
struct EnableFlag_tF8F1D449F6952746E76870852CC71ACE6986DA42 
{
	uint32_t ___value__;
};
struct Exception_t  : public RuntimeObject
{
	String_t* ____className;
	String_t* ____message;
	RuntimeObject* ____data;
	Exception_t* ____innerException;
	String_t* ____helpURL;
	RuntimeObject* ____stackTrace;
	String_t* ____stackTraceString;
	String_t* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	RuntimeObject* ____dynamicMethods;
	int32_t ____HResult;
	String_t* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct Exception_t_marshaled_pinvoke
{
	char* ____className;
	char* ____message;
	RuntimeObject* ____data;
	Exception_t_marshaled_pinvoke* ____innerException;
	char* ____helpURL;
	Il2CppIUnknown* ____stackTrace;
	char* ____stackTraceString;
	char* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	Il2CppIUnknown* ____dynamicMethods;
	int32_t ____HResult;
	char* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	Il2CppSafeArray* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className;
	Il2CppChar* ____message;
	RuntimeObject* ____data;
	Exception_t_marshaled_com* ____innerException;
	Il2CppChar* ____helpURL;
	Il2CppIUnknown* ____stackTrace;
	Il2CppChar* ____stackTraceString;
	Il2CppChar* ____remoteStackTraceString;
	int32_t ____remoteStackIndex;
	Il2CppIUnknown* ____dynamicMethods;
	int32_t ____HResult;
	Il2CppChar* ____source;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces;
	Il2CppSafeArray* ___native_trace_ips;
	int32_t ___caught_in_unmanaged;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr;
};
struct RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 
{
	intptr_t ___value;
};
struct FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D  : public RuntimeObject
{
	intptr_t ___context_;
};
struct UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7  : public RuntimeObject
{
	intptr_t ___context_;
};
struct WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB  : public RuntimeObject
{
	intptr_t ___context_;
};
struct ovrAudioScalarType_t510D6DA033FBD2DF966E329EFB802A80EE65CE9E 
{
	uint32_t ___value__;
};
struct MaterialPreset_t08D49E03BCA5ED9E889642121AC0ED95DC4DEB21 
{
	int32_t ___value__;
};
struct NativeParameterIndex_t7BCDC0EE3E56A0C9F0F4C92C4E7BD8F9BB81AB09 
{
	int32_t ___value__;
};
struct DirectivityPatternType_t834E78DCCBC3861E1D5161C7EDE0F0655AC71B0A 
{
	int32_t ___value__;
};
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};
struct MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	int32_t ___voiceLimit;
};
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};
struct TypeLoadException_t6333E3083F7BFF1A582969E6F67ACBA8B0035C32  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
	String_t* ___ClassName;
	String_t* ___AssemblyName;
	String_t* ___MessageArg;
	int32_t ___ResourceId;
};
struct AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};
struct DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534  : public TypeLoadException_t6333E3083F7BFF1A582969E6F67ACBA8B0035C32
{
};
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ___m_CancellationTokenSource;
};
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299  : public AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941
{
};
struct MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	bool ___lockPositionToListener;
	float ___width;
	float ___height;
	float ___depth;
	int32_t ___leftMaterial;
	int32_t ___rightMaterial;
	int32_t ___ceilingMaterial;
	int32_t ___floorMaterial;
	int32_t ___frontMaterial;
	int32_t ___backMaterial;
	float ___clutterFactor;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___clutterFactorBands;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___wallMaterials;
};
struct MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* ___source_;
	bool ___wasPlaying_;
	bool ___enableSpatialization;
	float ___gainBoostDb;
	bool ___enableAcoustics;
	float ___reverbSendDb;
};
struct MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* ___source_;
	float ___hrtfIntensity;
	float ___volumetricRadius;
	float ___earlyReflectionsSendDb;
	float ___directivityIntensity;
	int32_t ___directivityPattern;
};
struct MetaXRAudioVersion_tB1D8AEFA0C1C87DA3EFD4F545CD4A1370BFABAFB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};
struct EmptyArray_1_tF69A5F6BAD1150A16C4C98B346D6122FE3751C80_StaticFields
{
	__Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* ___Value;
};
struct U3CPrivateImplementationDetailsU3E_tA72208D6996CCF18BC3CE460CDC3978BBD2B8522_StaticFields
{
	__StaticArrayInitTypeSizeU3D430_t794BDC3722A2F905523CD9BE3B991327C45EFDFD ___234C2E3E1D1C65B2B9CB2842053B24AC13F7EB20C1380DCD86D285E004DBE831;
	__StaticArrayInitTypeSizeU3D618_tDAD4298EED13580B923E62613DA8B30FFFFF0076 ___3B2ED60CD250EEDD588288ACD6FFE84476EC170DAF5874F47A73D791C28D76A7;
};
struct MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_StaticFields
{
	RuntimeObject* ___CachedInterface;
};
struct String_t_StaticFields
{
	String_t* ___Empty;
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	String_t* ___TrueString;
	String_t* ___FalseString;
};
struct IntPtr_t_StaticFields
{
	intptr_t ___Zero;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject;
};
struct MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_StaticFields
{
	MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* ___instance;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C  : public RuntimeArray
{
	ALIGN_FIELD (8) float m_Items[1];

	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C  : public RuntimeArray
{
	ALIGN_FIELD (8) Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 m_Items[1];

	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 value)
	{
		m_Items[index] = value;
	}
};
struct MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C  : public RuntimeArray
{
	ALIGN_FIELD (8) MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* m_Items[1];

	inline MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
struct __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + il2cpp_array_calc_byte_offset(this, index);
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + il2cpp_array_calc_byte_offset(this, index);
	}
};


IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Object_FindObjectsOfType_TisRuntimeObject_m0B4DF4B8AB4C71E0F471BC9D0440B40844DA221D_gshared (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Resources_Load_TisRuntimeObject_mD1AF6299B14F87ED1D1A6199A51480919F7C79D7_gshared (String_t* ___0_path, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ScriptableObject_CreateInstance_TisRuntimeObject_mC07BE383F5EF546F4191035A679930852BC19BDA_gshared (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Component_GetComponent_TisIl2CppFullySharedGenericAny_m47CBDD147982125387F078ABBFDAAB92D397A6C2_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, Il2CppFullySharedGenericAny* il2cppRetVal, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* Array_Empty_TisIl2CppFullySharedGenericAny_m244E2A51B33F845A2093F0862FBCE502E4FDD868_gshared_inline (const RuntimeMethod* method) ;

IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MetaXRAudioNativeInterface_FindInterface_m9B3E5933AA6C52A0B363649644341499CF32AB26 (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t WwisePluginInterface_getOrCreateGlobalOvrAudioContext_mEF9C53E7B2B8AC2105A94F916E9008ECDFC79060 (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WwisePluginInterface__ctor_m102AF2F81B68898D54A1BE79A6E9B37BFB23CDC7 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_GetPluginContext_m8793E493F32F196EAFC2951EACA1123457FB863C (intptr_t* ___0_context, uint32_t ___1_clientType, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FMODPluginInterface__ctor_m5E680483B10C7ED3C547F49EE1119E1987812F70 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityNativeInterface__ctor_m429B3502D983C98CDB04D4B59574265D6E3C426A (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271 (intptr_t ___0_value1, intptr_t ___1_value2, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_GetPluginContext_m282F0E257B67CA62BC14B39AD4F8FB07BC7624A9 (intptr_t* ___0_context, uint32_t ___1_clientType, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_mAD8CE3E58B88645E6CE265AEB044E6C246D468F4 (intptr_t ___0_context, float ___1_width, float ___2_height, float ___3_depth, bool ___4_lockToListenerPosition, float ___5_positionX, float ___6_positionY, float ___7_positionZ, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___8_wallMaterials, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetRoomClutterFactor_m07D3584A7F969288C1DF4E22A62E8CEBAFE4DE63 (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_clutterFactor, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetSharedReverbWetLevel_m94C1ACEBC1D7D8065E88F440E6511A2571F90C4F (intptr_t ___0_context, float ___1_linearLevel, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_Enable_m379AB4839FE9D8D7C34531FC81C6DD0B929BEFF3 (intptr_t ___0_context, int32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_Enable_m10BF5BD590358AD158D6F93BC29190EAE1AFB845 (intptr_t ___0_context, uint32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mD282A609A735DAD232EDEB4F1763F98BA5694EF2 (intptr_t ___0_context, int32_t ___1_RaysPerSecond, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetDynamicRoomInterpSpeed_mEF0E99E287811B9DF904A9D30C74A216E2494AB8 (intptr_t ___0_context, float ___1_InterpSpeed, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetDynamicRoomMaxWallDistance_m8F72C441D50376C3C74A52530E854935664F602E (intptr_t ___0_context, float ___1_MaxWallDistance, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_m342E7ED11546F04B40D7E64C576708A9FFE4E689 (intptr_t ___0_context, int32_t ___1_RayCacheSize, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_GetRoomDimensions_mCE2A49C5F3177493C228E59A2C1A7B307D0AF46B (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___2_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_position, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_GetRaycastHits_m7F407270592A65E706281958A8E811F577E681DF (intptr_t ___0_context, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___2_normals, int32_t ___3_length, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_m46E7FC02F5E789AA6AC811160B510BDD754741D0 (intptr_t ___0_context, float ___1_width, float ___2_height, float ___3_depth, bool ___4_lockToListenerPosition, float ___5_positionX, float ___6_positionY, float ___7_positionZ, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___8_wallMaterials, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetRoomClutterFactor_mC16FC41F060DAB2E4DB8BC58A0FE9616F7D60D96 (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_clutterFactor, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetSharedReverbWetLevel_m551E3F4255C7EF106A178A90EF76AE0D9838BA48 (intptr_t ___0_context, float ___1_linearLevel, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_Enable_mE7203664282C3B642B52A61461E6EB2C5BA23C7B (intptr_t ___0_context, int32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_Enable_mF54B66918F82DC59EBEB7B1C896BA181C35E9E7D (intptr_t ___0_context, uint32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mFE48485443014BD2AB4FFCE3808A7C9CCB547FF6 (intptr_t ___0_context, int32_t ___1_RaysPerSecond, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetDynamicRoomInterpSpeed_mF5B8FB0CC76498191E57EF31950D4FBEBB5CB16A (intptr_t ___0_context, float ___1_InterpSpeed, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetDynamicRoomMaxWallDistance_mD2602C67550E5F124DEC592DD585565E6B61C70D (intptr_t ___0_context, float ___1_MaxWallDistance, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_mDD676823424B899AF01C2AC12A9C22EA1C07F65C (intptr_t ___0_context, int32_t ___1_RayCacheSize, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_GetRoomDimensions_m9671B3BBB1DDCDFC521BDDEDF66CA44C734FB211 (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___2_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_position, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_GetRaycastHits_mF7BFBDAB6EB6AB3AE4ECFF625929BBF6A0131DB9 (intptr_t ___0_context, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___2_normals, int32_t ___3_length, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_m7DA5D3DC14A4BD1D8C2BCCDC7FFE1C543A4A0E07 (intptr_t ___0_context, float ___1_width, float ___2_height, float ___3_depth, bool ___4_lockToListenerPosition, float ___5_positionX, float ___6_positionY, float ___7_positionZ, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___8_wallMaterials, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetRoomClutterFactor_m2EFB3F6AE58AF2632CB872A22272E778AE19D8A8 (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_clutterFactor, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetSharedReverbWetLevel_mE870E222591BA2DBF39F7B0F90E9D4613687D21C (intptr_t ___0_context, float ___1_linearLevel, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_Enable_m5C4025D89A4FC0BF1C6C438A07E8CD06003D3D80 (intptr_t ___0_context, int32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_Enable_mA2ED5864A48BE9DF9FAFCFC64D02CF14C90B389B (intptr_t ___0_context, uint32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mFAABD653FABCB1D9CFA5BB3A271B46DAC8D410AD (intptr_t ___0_context, int32_t ___1_RaysPerSecond, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetDynamicRoomInterpSpeed_m3750997B8E4AF41DDFF99F60F325293C9C1C2911 (intptr_t ___0_context, float ___1_InterpSpeed, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetDynamicRoomMaxWallDistance_mA99D2DE88BF09741F7BDD426D6DB52BF59FF081F (intptr_t ___0_context, float ___1_MaxWallDistance, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_mB25ABECB1BCBB987E86929381A78AB1FF2780E34 (intptr_t ___0_context, int32_t ___1_RayCacheSize, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_GetRoomDimensions_m033D131EB9535C71B1139883929984A30468272B (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___2_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_position, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_GetRaycastHits_m719B1DCA6406E3B150EF35849E62A00877AF68A5 (intptr_t ___0_context, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___2_normals, int32_t ___3_length, const RuntimeMethod* method) ;
inline MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C* Object_FindObjectsOfType_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mFF107257026360A211CC4F1F96D7BDD663E93DA6 (const RuntimeMethod* method)
{
	return ((  MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C* (*) (const RuntimeMethod*))Object_FindObjectsOfType_TisRuntimeObject_m0B4DF4B8AB4C71E0F471BC9D0440B40844DA221D_gshared)(method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject__ctor_m37D512B05D292F954792225E6C6EEE95293A9B88 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, String_t* ___0_name, const RuntimeMethod* method) ;
inline MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* GameObject_AddComponent_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mD43B43D37FEAED2F93891370BC1C58C7B1C878FD (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m69B93700FACCF372F5753371C6E8FB780800B824_gshared)(__this, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioRoomAcousticProperties_Update_mEB82D666B651960636E16950A917F1B8E525A2AC (MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_m6336EBC83591A5DB64EC70C92132824C6E258705 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_obj, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2 (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioRoomAcousticProperties_SetWallMaterialPreset_mE8310341438AE1AC2F5AC20DD166596C00B6D58F (MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* __this, int32_t ___0_wallIndex, int32_t ___1_materialPreset, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MetaXRAudioNativeInterface_get_Interface_mD282DE6365778E328E4FCB535651ADFFF91AF29B (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41 (MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* __this, int32_t ___0_wallIndex, float ___1_band0, float ___2_band1, float ___3_band2, float ___4_band3, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_x, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___1_y, const RuntimeMethod* method) ;
inline MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* Resources_Load_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE5B0617A5034C61150CB4DD33C88B0A6E9BEE08F (String_t* ___0_path, const RuntimeMethod* method)
{
	return ((  MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_mD1AF6299B14F87ED1D1A6199A51480919F7C79D7_gshared)(___0_path, method);
}
inline MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* ScriptableObject_CreateInstance_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE3762098D5E2575BBDA769870A8F2FA3B714BBBC (const RuntimeMethod* method)
{
	return ((  MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* (*) (const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_mC07BE383F5EF546F4191035A679930852BC19BDA_gshared)(method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_mD037FDB0B487295EA47F79A4DB1BF1846C9087FF (ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* MetaXRAudioSettings_get_Instance_mB453230A57D314E0508F6A012987114BE46160F1 (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8 (String_t* ___0_format, RuntimeObject* ___1_arg0, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MetaXRAudioSource_MetaXRAudio_SetGlobalVoiceLimit_m6305F25F5078BA076388898576E453D281578FAE (int32_t ___0_VoiceLimit, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline (float ___0_value, float ___1_min, float ___2_max, const RuntimeMethod* method) ;
inline AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* il2cppRetVal;
	((  void (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, Il2CppFullySharedGenericAny*, const RuntimeMethod*))Component_GetComponent_TisIl2CppFullySharedGenericAny_m47CBDD147982125387F078ABBFDAAB92D397A6C2_gshared)((Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*)__this, (Il2CppFullySharedGenericAny*)&il2cppRetVal, method);
	return il2cppRetVal;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_UpdateParameters_m660E4EC58BEFF69332BF34E6C1FA819195CB44E0 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AudioSource_get_isPlaying_mC203303F2F7146B2C056CB47B9391463FDF408FC (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_set_spatialize_mDFA357EDCB0C59EF11F53C845F7ACBF6BF7F7B3C (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, bool ___0_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, int32_t ___0_index, float ___1_value, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Max_mF5379E63D2BBAC76D090748695D833934F8AD051_inline (float ___0_a, float ___1_b, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_UpdateParameters_mC0FAB747523030BE2519777E5E658920E97928D8 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioVersion_MetaXRAudio_GetVersion_m18A5D74F925242105267EAD291C8A62A2C829483 (int32_t* ___0_Major, int32_t* ___1_Minor, int32_t* ___2_Patch, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mA0534D6E2AE4D67A6BD8D45B3321323930EB930C (String_t* ___0_format, RuntimeObject* ___1_arg0, RuntimeObject* ___2_arg1, RuntimeObject* ___3_arg2, const RuntimeMethod* method) ;
inline ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* Array_Empty_TisRuntimeObject_mFB8A63D602BB6974D31E20300D9EB89C6FE7C278_inline (const RuntimeMethod* method)
{
	return ((  ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* (*) (const RuntimeMethod*))Array_Empty_TisIl2CppFullySharedGenericAny_m244E2A51B33F845A2093F0862FBCE502E4FDD868_gshared_inline)(method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m918500C1EFB475181349A79989BB79BB36102894 (String_t* ___0_format, ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___1_args, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B (RuntimeArray* ___0_array, RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 ___1_fldHandle, const RuntimeMethod* method) ;
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_GetPluginContext(intptr_t*, uint32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_SetAdvancedBoxRoomParametersUnity(intptr_t, float, float, float, int32_t, float, float, float, float*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_SetRoomClutterFactor(intptr_t, float*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_SetSharedReverbWetLevel(intptr_t, float);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_Enable(intptr_t, int32_t, int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_SetDynamicRoomRaysPerSecond(intptr_t, int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_SetDynamicRoomInterpSpeed(intptr_t, float);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_SetDynamicRoomMaxWallDistance(intptr_t, float);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_SetDynamicRoomRaysRayCacheSize(intptr_t, int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_GetRoomDimensions(intptr_t, float*, float*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL ovrAudio_GetRaycastHits(intptr_t, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*, int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL getOrCreateGlobalOvrAudioContext();
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL MetaXRAudio_SetGlobalVoiceLimit(int32_t);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL MetaXRAudio_GetGlobalRoomReflectionValues(int32_t*, int32_t*, float*, float*, float*);
#endif
#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL MetaXRAudio_GetVersion(int32_t*, int32_t*, int32_t*);
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MetaXRAudioNativeInterface_get_Interface_mD282DE6365778E328E4FCB535651ADFFF91AF29B (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ((MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_il2cpp_TypeInfo_var))->___CachedInterface;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		RuntimeObject* L_1;
		L_1 = MetaXRAudioNativeInterface_FindInterface_m9B3E5933AA6C52A0B363649644341499CF32AB26(NULL);
		((MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_il2cpp_TypeInfo_var))->___CachedInterface = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_il2cpp_TypeInfo_var))->___CachedInterface), (void*)L_1);
	}

IL_0011:
	{
		RuntimeObject* L_2 = ((MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC_il2cpp_TypeInfo_var))->___CachedInterface;
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MetaXRAudioNativeInterface_FindInterface_m9B3E5933AA6C52A0B363649644341499CF32AB26 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6CEE3F44F7BD3490E651E4E7ADBB98B8A657D8DA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral78228913BAA5055D00A587B0DD86C2298DE86086);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A02F5F188B678BFC1C8D758AD62EA73CA4E1055);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	RuntimeObject* V_1 = NULL;
	il2cpp::utils::ExceptionSupportStack<RuntimeObject*, 1> __active_exceptions;
	try
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_getOrCreateGlobalOvrAudioContext_mEF9C53E7B2B8AC2105A94F916E9008ECDFC79060(NULL);
		V_0 = L_0;
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteral6CEE3F44F7BD3490E651E4E7ADBB98B8A657D8DA, NULL);
		WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* L_1 = (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB*)il2cpp_codegen_object_new(WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB_il2cpp_TypeInfo_var);
		WwisePluginInterface__ctor_m102AF2F81B68898D54A1BE79A6E9B37BFB23CDC7(L_1, NULL);
		V_1 = L_1;
		goto IL_004a;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0018;
		}
		throw e;
	}

CATCH_0018:
	{
		DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534* L_2 = ((DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534*)IL2CPP_GET_ACTIVE_EXCEPTION(DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534*));;
		IL2CPP_POP_ACTIVE_EXCEPTION(Exception_t*);
		goto IL_001b;
	}

IL_001b:
	{
	}
	try
	{
		int32_t L_3;
		L_3 = FMODPluginInterface_ovrAudio_GetPluginContext_m8793E493F32F196EAFC2951EACA1123457FB863C((&V_0), 5, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteral8A02F5F188B678BFC1C8D758AD62EA73CA4E1055, NULL);
		FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* L_4 = (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D*)il2cpp_codegen_object_new(FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D_il2cpp_TypeInfo_var);
		FMODPluginInterface__ctor_m5E680483B10C7ED3C547F49EE1119E1987812F70(L_4, NULL);
		V_1 = L_4;
		goto IL_004a;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		if(il2cpp_codegen_class_is_assignable_from (((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534_il2cpp_TypeInfo_var)), il2cpp_codegen_object_class(e.ex)))
		{
			IL2CPP_PUSH_ACTIVE_EXCEPTION(e.ex);
			goto CATCH_0037;
		}
		throw e;
	}

CATCH_0037:
	{
		DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534* L_5 = ((DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534*)IL2CPP_GET_ACTIVE_EXCEPTION(DllNotFoundException_t8CAE636A394C482C9FCF38FB7B7929506319D534*));;
		IL2CPP_POP_ACTIVE_EXCEPTION(Exception_t*);
		goto IL_003a;
	}

IL_003a:
	{
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteral78228913BAA5055D00A587B0DD86C2298DE86086, NULL);
		UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* L_6 = (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7*)il2cpp_codegen_object_new(UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7_il2cpp_TypeInfo_var);
		UnityNativeInterface__ctor_m429B3502D983C98CDB04D4B59574265D6E3C426A(L_6, NULL);
		return L_6;
	}

IL_004a:
	{
		RuntimeObject* L_7 = V_1;
		return L_7;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioNativeInterface__ctor_m5176FBAE86DC641D9F32F7C49BF789513D9FA801 (MetaXRAudioNativeInterface_tAC6AFA5666FF181C42D5891E231C947A2CB753AC* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ClientType__ctor_mBCBC78F15A9BB6B5F9251E0031FF55B119EF6764 (ClientType_t342F98FC200FB329CD5440274733CBCE01CF834A* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, const RuntimeMethod* method) 
{
	{
		intptr_t L_0 = __this->___context_;
		bool L_1;
		L_1 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, 0, NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		intptr_t* L_2 = (intptr_t*)(&__this->___context_);
		int32_t L_3;
		L_3 = UnityNativeInterface_ovrAudio_GetPluginContext_m282F0E257B67CA62BC14B39AD4F8FB07BC7624A9(L_2, 6, NULL);
	}

IL_001f:
	{
		intptr_t L_4 = __this->___context_;
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_GetPluginContext_m282F0E257B67CA62BC14B39AD4F8FB07BC7624A9 (intptr_t* ___0_context, uint32_t ___1_clientType, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t*, uint32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t*) + sizeof(uint32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_GetPluginContext", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_GetPluginContext)(___0_context, ___1_clientType);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_clientType);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_mAD8CE3E58B88645E6CE265AEB044E6C246D468F4 (intptr_t ___0_context, float ___1_width, float ___2_height, float ___3_depth, bool ___4_lockToListenerPosition, float ___5_positionX, float ___6_positionY, float ___7_positionZ, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___8_wallMaterials, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float, float, float, int32_t, float, float, float, float*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float) + sizeof(float) + sizeof(float) + 4 + sizeof(float) + sizeof(float) + sizeof(float) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_SetAdvancedBoxRoomParametersUnity", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____8_wallMaterials_marshaled = NULL;
	if (___8_wallMaterials != NULL)
	{
		____8_wallMaterials_marshaled = reinterpret_cast<float*>((___8_wallMaterials)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetAdvancedBoxRoomParametersUnity)(___0_context, ___1_width, ___2_height, ___3_depth, static_cast<int32_t>(___4_lockToListenerPosition), ___5_positionX, ___6_positionY, ___7_positionZ, ____8_wallMaterials_marshaled);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_width, ___2_height, ___3_depth, static_cast<int32_t>(___4_lockToListenerPosition), ___5_positionX, ___6_positionY, ___7_positionZ, ____8_wallMaterials_marshaled);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetAdvancedBoxRoomParameters_mB03AC4305E4056E3294B4B81E5AE3F2A47E87E09 (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, float ___0_width, float ___1_height, float ___2_depth, bool ___3_lockToListenerPosition, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___4_position, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___5_wallMaterials, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		float L_1 = ___0_width;
		float L_2 = ___1_height;
		float L_3 = ___2_depth;
		bool L_4 = ___3_lockToListenerPosition;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = ___4_position;
		float L_6 = L_5.___x;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___4_position;
		float L_8 = L_7.___y;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = ___4_position;
		float L_10 = L_9.___z;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_11 = ___5_wallMaterials;
		int32_t L_12;
		L_12 = UnityNativeInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_mAD8CE3E58B88645E6CE265AEB044E6C246D468F4(L_0, L_1, L_2, L_3, L_4, L_6, L_8, ((-L_10)), L_11, NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetRoomClutterFactor_m07D3584A7F969288C1DF4E22A62E8CEBAFE4DE63 (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_clutterFactor, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_SetRoomClutterFactor", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____1_clutterFactor_marshaled = NULL;
	if (___1_clutterFactor != NULL)
	{
		____1_clutterFactor_marshaled = reinterpret_cast<float*>((___1_clutterFactor)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetRoomClutterFactor)(___0_context, ____1_clutterFactor_marshaled);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_clutterFactor_marshaled);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetRoomClutterFactor_m5ECC4E55E893DFEF5B824E5641C7D21A81D20891 (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___0_clutterFactor, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = ___0_clutterFactor;
		int32_t L_2;
		L_2 = UnityNativeInterface_ovrAudio_SetRoomClutterFactor_m07D3584A7F969288C1DF4E22A62E8CEBAFE4DE63(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetSharedReverbWetLevel_m94C1ACEBC1D7D8065E88F440E6511A2571F90C4F (intptr_t ___0_context, float ___1_linearLevel, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_SetSharedReverbWetLevel", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetSharedReverbWetLevel)(___0_context, ___1_linearLevel);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_linearLevel);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetSharedReverbWetLevel_mAE9FF6A42666F5B8ADA6C31FD4B4DB11B18B7024 (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, float ___0_linearLevel, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		float L_1 = ___0_linearLevel;
		int32_t L_2;
		L_2 = UnityNativeInterface_ovrAudio_SetSharedReverbWetLevel_m94C1ACEBC1D7D8065E88F440E6511A2571F90C4F(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_Enable_m379AB4839FE9D8D7C34531FC81C6DD0B929BEFF3 (intptr_t ___0_context, int32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_Enable", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_Enable)(___0_context, ___1_what, ___2_enable);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_what, ___2_enable);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetEnabled_mDDAD95FA05D8F23683CC806B6A1DDFF0209B393E (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, int32_t ___0_feature, bool ___1_enabled, const RuntimeMethod* method) 
{
	int32_t G_B2_0 = 0;
	intptr_t G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	int32_t G_B1_0 = 0;
	intptr_t G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	intptr_t G_B3_2;
	memset((&G_B3_2), 0, sizeof(G_B3_2));
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		int32_t L_1 = ___0_feature;
		bool L_2 = ___1_enabled;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_000d;
		}
		G_B1_0 = L_1;
		G_B1_1 = L_0;
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_000e:
	{
		int32_t L_3;
		L_3 = UnityNativeInterface_ovrAudio_Enable_m379AB4839FE9D8D7C34531FC81C6DD0B929BEFF3(G_B3_2, G_B3_1, G_B3_0, NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_Enable_m10BF5BD590358AD158D6F93BC29190EAE1AFB845 (intptr_t ___0_context, uint32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, uint32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(uint32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_Enable", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_Enable)(___0_context, ___1_what, ___2_enable);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_what, ___2_enable);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetEnabled_m4B5CC4BBFC169BAC1D755C715D68C3FD640B78A4 (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, uint32_t ___0_feature, bool ___1_enabled, const RuntimeMethod* method) 
{
	uint32_t G_B2_0 = 0;
	intptr_t G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	uint32_t G_B1_0 = 0;
	intptr_t G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	int32_t G_B3_0 = 0;
	uint32_t G_B3_1 = 0;
	intptr_t G_B3_2;
	memset((&G_B3_2), 0, sizeof(G_B3_2));
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		uint32_t L_1 = ___0_feature;
		bool L_2 = ___1_enabled;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_000d;
		}
		G_B1_0 = L_1;
		G_B1_1 = L_0;
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_000e:
	{
		int32_t L_3;
		L_3 = UnityNativeInterface_ovrAudio_Enable_m10BF5BD590358AD158D6F93BC29190EAE1AFB845(G_B3_2, G_B3_1, G_B3_0, NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mD282A609A735DAD232EDEB4F1763F98BA5694EF2 (intptr_t ___0_context, int32_t ___1_RaysPerSecond, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_SetDynamicRoomRaysPerSecond", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomRaysPerSecond)(___0_context, ___1_RaysPerSecond);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_RaysPerSecond);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetDynamicRoomRaysPerSecond_mEBF1EB3854BF7F2BD84C63B968A26F453B1ECD94 (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, int32_t ___0_RaysPerSecond, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		int32_t L_1 = ___0_RaysPerSecond;
		int32_t L_2;
		L_2 = UnityNativeInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mD282A609A735DAD232EDEB4F1763F98BA5694EF2(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetDynamicRoomInterpSpeed_mEF0E99E287811B9DF904A9D30C74A216E2494AB8 (intptr_t ___0_context, float ___1_InterpSpeed, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_SetDynamicRoomInterpSpeed", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomInterpSpeed)(___0_context, ___1_InterpSpeed);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_InterpSpeed);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetDynamicRoomInterpSpeed_mC0A52C928C5D70EC2CE8076E9A3BFE04BC19A2AE (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, float ___0_InterpSpeed, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		float L_1 = ___0_InterpSpeed;
		int32_t L_2;
		L_2 = UnityNativeInterface_ovrAudio_SetDynamicRoomInterpSpeed_mEF0E99E287811B9DF904A9D30C74A216E2494AB8(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetDynamicRoomMaxWallDistance_m8F72C441D50376C3C74A52530E854935664F602E (intptr_t ___0_context, float ___1_MaxWallDistance, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_SetDynamicRoomMaxWallDistance", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomMaxWallDistance)(___0_context, ___1_MaxWallDistance);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_MaxWallDistance);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetDynamicRoomMaxWallDistance_mDB877ADA9101CB5B395A5C963EF7E39D0ABF016E (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, float ___0_MaxWallDistance, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		float L_1 = ___0_MaxWallDistance;
		int32_t L_2;
		L_2 = UnityNativeInterface_ovrAudio_SetDynamicRoomMaxWallDistance_m8F72C441D50376C3C74A52530E854935664F602E(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_m342E7ED11546F04B40D7E64C576708A9FFE4E689 (intptr_t ___0_context, int32_t ___1_RayCacheSize, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_SetDynamicRoomRaysRayCacheSize", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomRaysRayCacheSize)(___0_context, ___1_RayCacheSize);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_RayCacheSize);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_SetDynamicRoomRaysRayCacheSize_m8B4C3930E0595421E328C003A266CA4F7D477CAD (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, int32_t ___0_RayCacheSize, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		int32_t L_1 = ___0_RayCacheSize;
		int32_t L_2;
		L_2 = UnityNativeInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_m342E7ED11546F04B40D7E64C576708A9FFE4E689(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_GetRoomDimensions_mCE2A49C5F3177493C228E59A2C1A7B307D0AF46B (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___2_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_position, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float*, float*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(void*) + sizeof(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_GetRoomDimensions", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____1_roomDimensions_marshaled = NULL;
	if (___1_roomDimensions != NULL)
	{
		____1_roomDimensions_marshaled = reinterpret_cast<float*>((___1_roomDimensions)->GetAddressAtUnchecked(0));
	}

	float* ____2_reflectionsCoefs_marshaled = NULL;
	if (___2_reflectionsCoefs != NULL)
	{
		____2_reflectionsCoefs_marshaled = reinterpret_cast<float*>((___2_reflectionsCoefs)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_GetRoomDimensions)(___0_context, ____1_roomDimensions_marshaled, ____2_reflectionsCoefs_marshaled, ___3_position);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_roomDimensions_marshaled, ____2_reflectionsCoefs_marshaled, ___3_position);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_GetRoomDimensions_mF2A7FD41825227F35D1492AE642958ACA07BCD51 (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___0_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___2_position, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = ___0_roomDimensions;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = ___1_reflectionsCoefs;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_3 = ___2_position;
		int32_t L_4;
		L_4 = UnityNativeInterface_ovrAudio_GetRoomDimensions_mCE2A49C5F3177493C228E59A2C1A7B307D0AF46B(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_ovrAudio_GetRaycastHits_m7F407270592A65E706281958A8E811F577E681DF (intptr_t ___0_context, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___2_normals, int32_t ___3_length, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(void*) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "ovrAudio_GetRaycastHits", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ____1_points_marshaled = NULL;
	if (___1_points != NULL)
	{
		____1_points_marshaled = reinterpret_cast<Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*>((___1_points)->GetAddressAtUnchecked(0));
	}

	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ____2_normals_marshaled = NULL;
	if (___2_normals != NULL)
	{
		____2_normals_marshaled = reinterpret_cast<Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*>((___2_normals)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_GetRaycastHits)(___0_context, ____1_points_marshaled, ____2_normals_marshaled, ___3_length);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_points_marshaled, ____2_normals_marshaled, ___3_length);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UnityNativeInterface_GetRaycastHits_mAD57ACF8B86237A708E74DB77131C9FCF3289E07 (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___0_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_normals, int32_t ___2_length, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = UnityNativeInterface_get_context_m95C47C43EE3A5F7CDE6E2649D8F0F4D786976E0B(__this, NULL);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = ___0_points;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_2 = ___1_normals;
		int32_t L_3 = ___2_length;
		int32_t L_4;
		L_4 = UnityNativeInterface_ovrAudio_GetRaycastHits_m7F407270592A65E706281958A8E811F577E681DF(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityNativeInterface__ctor_m429B3502D983C98CDB04D4B59574265D6E3C426A (UnityNativeInterface_tD09163B2786F8A2BB986CFE7710017F8C8C8DCC7* __this, const RuntimeMethod* method) 
{
	{
		__this->___context_ = 0;
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, const RuntimeMethod* method) 
{
	{
		intptr_t L_0 = __this->___context_;
		bool L_1;
		L_1 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, 0, NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		intptr_t L_2;
		L_2 = WwisePluginInterface_getOrCreateGlobalOvrAudioContext_mEF9C53E7B2B8AC2105A94F916E9008ECDFC79060(NULL);
		__this->___context_ = L_2;
	}

IL_001d:
	{
		intptr_t L_3 = __this->___context_;
		return L_3;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t WwisePluginInterface_getOrCreateGlobalOvrAudioContext_mEF9C53E7B2B8AC2105A94F916E9008ECDFC79060 (const RuntimeMethod* method) 
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "getOrCreateGlobalOvrAudioContext", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(getOrCreateGlobalOvrAudioContext)();
	#else
	intptr_t returnValue = il2cppPInvokeFunc();
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_m46E7FC02F5E789AA6AC811160B510BDD754741D0 (intptr_t ___0_context, float ___1_width, float ___2_height, float ___3_depth, bool ___4_lockToListenerPosition, float ___5_positionX, float ___6_positionY, float ___7_positionZ, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___8_wallMaterials, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float, float, float, int32_t, float, float, float, float*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float) + sizeof(float) + sizeof(float) + 4 + sizeof(float) + sizeof(float) + sizeof(float) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_SetAdvancedBoxRoomParametersUnity", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____8_wallMaterials_marshaled = NULL;
	if (___8_wallMaterials != NULL)
	{
		____8_wallMaterials_marshaled = reinterpret_cast<float*>((___8_wallMaterials)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetAdvancedBoxRoomParametersUnity)(___0_context, ___1_width, ___2_height, ___3_depth, static_cast<int32_t>(___4_lockToListenerPosition), ___5_positionX, ___6_positionY, ___7_positionZ, ____8_wallMaterials_marshaled);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_width, ___2_height, ___3_depth, static_cast<int32_t>(___4_lockToListenerPosition), ___5_positionX, ___6_positionY, ___7_positionZ, ____8_wallMaterials_marshaled);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetAdvancedBoxRoomParameters_m7A4D44211390AE9AFC08B2C8A4A972CFD3510BC9 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, float ___0_width, float ___1_height, float ___2_depth, bool ___3_lockToListenerPosition, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___4_position, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___5_wallMaterials, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		float L_1 = ___0_width;
		float L_2 = ___1_height;
		float L_3 = ___2_depth;
		bool L_4 = ___3_lockToListenerPosition;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = ___4_position;
		float L_6 = L_5.___x;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___4_position;
		float L_8 = L_7.___y;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = ___4_position;
		float L_10 = L_9.___z;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_11 = ___5_wallMaterials;
		int32_t L_12;
		L_12 = WwisePluginInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_m46E7FC02F5E789AA6AC811160B510BDD754741D0(L_0, L_1, L_2, L_3, L_4, L_6, L_8, ((-L_10)), L_11, NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetRoomClutterFactor_mC16FC41F060DAB2E4DB8BC58A0FE9616F7D60D96 (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_clutterFactor, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_SetRoomClutterFactor", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____1_clutterFactor_marshaled = NULL;
	if (___1_clutterFactor != NULL)
	{
		____1_clutterFactor_marshaled = reinterpret_cast<float*>((___1_clutterFactor)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetRoomClutterFactor)(___0_context, ____1_clutterFactor_marshaled);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_clutterFactor_marshaled);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetRoomClutterFactor_m02FA50551F4577EFE3A77BF6FDF33588842E49E4 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___0_clutterFactor, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = ___0_clutterFactor;
		int32_t L_2;
		L_2 = WwisePluginInterface_ovrAudio_SetRoomClutterFactor_mC16FC41F060DAB2E4DB8BC58A0FE9616F7D60D96(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetSharedReverbWetLevel_m551E3F4255C7EF106A178A90EF76AE0D9838BA48 (intptr_t ___0_context, float ___1_linearLevel, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_SetSharedReverbWetLevel", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetSharedReverbWetLevel)(___0_context, ___1_linearLevel);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_linearLevel);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetSharedReverbWetLevel_mFA32F8C7D64200037D19B4B3B2BDEBAC8B1E73D3 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, float ___0_linearLevel, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		float L_1 = ___0_linearLevel;
		int32_t L_2;
		L_2 = WwisePluginInterface_ovrAudio_SetSharedReverbWetLevel_m551E3F4255C7EF106A178A90EF76AE0D9838BA48(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_Enable_mE7203664282C3B642B52A61461E6EB2C5BA23C7B (intptr_t ___0_context, int32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_Enable", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_Enable)(___0_context, ___1_what, ___2_enable);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_what, ___2_enable);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetEnabled_mC5E77289ABB4EE3BB9CFBD6A428283BD4F7D2B71 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, int32_t ___0_feature, bool ___1_enabled, const RuntimeMethod* method) 
{
	int32_t G_B2_0 = 0;
	intptr_t G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	int32_t G_B1_0 = 0;
	intptr_t G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	intptr_t G_B3_2;
	memset((&G_B3_2), 0, sizeof(G_B3_2));
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		int32_t L_1 = ___0_feature;
		bool L_2 = ___1_enabled;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_000d;
		}
		G_B1_0 = L_1;
		G_B1_1 = L_0;
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_000e:
	{
		int32_t L_3;
		L_3 = WwisePluginInterface_ovrAudio_Enable_mE7203664282C3B642B52A61461E6EB2C5BA23C7B(G_B3_2, G_B3_1, G_B3_0, NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_Enable_mF54B66918F82DC59EBEB7B1C896BA181C35E9E7D (intptr_t ___0_context, uint32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, uint32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(uint32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_Enable", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_Enable)(___0_context, ___1_what, ___2_enable);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_what, ___2_enable);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetEnabled_mF221D362CE928052CE965980E532051BE4335899 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, uint32_t ___0_feature, bool ___1_enabled, const RuntimeMethod* method) 
{
	uint32_t G_B2_0 = 0;
	intptr_t G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	uint32_t G_B1_0 = 0;
	intptr_t G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	int32_t G_B3_0 = 0;
	uint32_t G_B3_1 = 0;
	intptr_t G_B3_2;
	memset((&G_B3_2), 0, sizeof(G_B3_2));
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		uint32_t L_1 = ___0_feature;
		bool L_2 = ___1_enabled;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_000d;
		}
		G_B1_0 = L_1;
		G_B1_1 = L_0;
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_000e:
	{
		int32_t L_3;
		L_3 = WwisePluginInterface_ovrAudio_Enable_mF54B66918F82DC59EBEB7B1C896BA181C35E9E7D(G_B3_2, G_B3_1, G_B3_0, NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mFE48485443014BD2AB4FFCE3808A7C9CCB547FF6 (intptr_t ___0_context, int32_t ___1_RaysPerSecond, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_SetDynamicRoomRaysPerSecond", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomRaysPerSecond)(___0_context, ___1_RaysPerSecond);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_RaysPerSecond);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetDynamicRoomRaysPerSecond_mF690A82E79EA071D69C9AE51A4F2E2A54AA19CFE (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, int32_t ___0_RaysPerSecond, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		int32_t L_1 = ___0_RaysPerSecond;
		int32_t L_2;
		L_2 = WwisePluginInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mFE48485443014BD2AB4FFCE3808A7C9CCB547FF6(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetDynamicRoomInterpSpeed_mF5B8FB0CC76498191E57EF31950D4FBEBB5CB16A (intptr_t ___0_context, float ___1_InterpSpeed, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_SetDynamicRoomInterpSpeed", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomInterpSpeed)(___0_context, ___1_InterpSpeed);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_InterpSpeed);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetDynamicRoomInterpSpeed_m757D89ED3A08EAB07C93FDB03DE655062A36F446 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, float ___0_InterpSpeed, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		float L_1 = ___0_InterpSpeed;
		int32_t L_2;
		L_2 = WwisePluginInterface_ovrAudio_SetDynamicRoomInterpSpeed_mF5B8FB0CC76498191E57EF31950D4FBEBB5CB16A(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetDynamicRoomMaxWallDistance_mD2602C67550E5F124DEC592DD585565E6B61C70D (intptr_t ___0_context, float ___1_MaxWallDistance, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_SetDynamicRoomMaxWallDistance", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomMaxWallDistance)(___0_context, ___1_MaxWallDistance);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_MaxWallDistance);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetDynamicRoomMaxWallDistance_mE572C01F74ABD40772DA7480474AAE02992011C5 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, float ___0_MaxWallDistance, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		float L_1 = ___0_MaxWallDistance;
		int32_t L_2;
		L_2 = WwisePluginInterface_ovrAudio_SetDynamicRoomMaxWallDistance_mD2602C67550E5F124DEC592DD585565E6B61C70D(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_mDD676823424B899AF01C2AC12A9C22EA1C07F65C (intptr_t ___0_context, int32_t ___1_RayCacheSize, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_SetDynamicRoomRaysRayCacheSize", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomRaysRayCacheSize)(___0_context, ___1_RayCacheSize);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_RayCacheSize);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_SetDynamicRoomRaysRayCacheSize_mBB345EB56BEF8C9C2E27280F307BE1A5CB52F0AD (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, int32_t ___0_RayCacheSize, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		int32_t L_1 = ___0_RayCacheSize;
		int32_t L_2;
		L_2 = WwisePluginInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_mDD676823424B899AF01C2AC12A9C22EA1C07F65C(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_GetRoomDimensions_m9671B3BBB1DDCDFC521BDDEDF66CA44C734FB211 (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___2_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_position, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float*, float*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(void*) + sizeof(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_GetRoomDimensions", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____1_roomDimensions_marshaled = NULL;
	if (___1_roomDimensions != NULL)
	{
		____1_roomDimensions_marshaled = reinterpret_cast<float*>((___1_roomDimensions)->GetAddressAtUnchecked(0));
	}

	float* ____2_reflectionsCoefs_marshaled = NULL;
	if (___2_reflectionsCoefs != NULL)
	{
		____2_reflectionsCoefs_marshaled = reinterpret_cast<float*>((___2_reflectionsCoefs)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_GetRoomDimensions)(___0_context, ____1_roomDimensions_marshaled, ____2_reflectionsCoefs_marshaled, ___3_position);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_roomDimensions_marshaled, ____2_reflectionsCoefs_marshaled, ___3_position);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_GetRoomDimensions_m772A42E221F92460B42CB0F7CB9BE429D1D759F0 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___0_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___2_position, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = ___0_roomDimensions;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = ___1_reflectionsCoefs;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_3 = ___2_position;
		int32_t L_4;
		L_4 = WwisePluginInterface_ovrAudio_GetRoomDimensions_m9671B3BBB1DDCDFC521BDDEDF66CA44C734FB211(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_ovrAudio_GetRaycastHits_mF7BFBDAB6EB6AB3AE4ECFF625929BBF6A0131DB9 (intptr_t ___0_context, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___2_normals, int32_t ___3_length, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(void*) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioWwise"), "ovrAudio_GetRaycastHits", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ____1_points_marshaled = NULL;
	if (___1_points != NULL)
	{
		____1_points_marshaled = reinterpret_cast<Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*>((___1_points)->GetAddressAtUnchecked(0));
	}

	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ____2_normals_marshaled = NULL;
	if (___2_normals != NULL)
	{
		____2_normals_marshaled = reinterpret_cast<Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*>((___2_normals)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioWwise_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_GetRaycastHits)(___0_context, ____1_points_marshaled, ____2_normals_marshaled, ___3_length);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_points_marshaled, ____2_normals_marshaled, ___3_length);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t WwisePluginInterface_GetRaycastHits_m2BDA5C14639A966EFA461A3E6D51F3516A47D2CA (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___0_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_normals, int32_t ___2_length, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = WwisePluginInterface_get_context_mB92A2721618BB73E1508840EF068F0B42973C18C(__this, NULL);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = ___0_points;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_2 = ___1_normals;
		int32_t L_3 = ___2_length;
		int32_t L_4;
		L_4 = WwisePluginInterface_ovrAudio_GetRaycastHits_mF7BFBDAB6EB6AB3AE4ECFF625929BBF6A0131DB9(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WwisePluginInterface__ctor_m102AF2F81B68898D54A1BE79A6E9B37BFB23CDC7 (WwisePluginInterface_t199AE67CEDE65A79910FD8DFA154C8FD392E6EEB* __this, const RuntimeMethod* method) 
{
	{
		__this->___context_ = 0;
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, const RuntimeMethod* method) 
{
	{
		intptr_t L_0 = __this->___context_;
		bool L_1;
		L_1 = IntPtr_op_Equality_m7D9CDCDE9DC2A0C2C614633F4921E90187FAB271(L_0, 0, NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		intptr_t* L_2 = (intptr_t*)(&__this->___context_);
		int32_t L_3;
		L_3 = FMODPluginInterface_ovrAudio_GetPluginContext_m8793E493F32F196EAFC2951EACA1123457FB863C(L_2, 5, NULL);
	}

IL_001f:
	{
		intptr_t L_4 = __this->___context_;
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_GetPluginContext_m8793E493F32F196EAFC2951EACA1123457FB863C (intptr_t* ___0_context, uint32_t ___1_clientType, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t*, uint32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t*) + sizeof(uint32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_GetPluginContext", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_GetPluginContext)(___0_context, ___1_clientType);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_clientType);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_m7DA5D3DC14A4BD1D8C2BCCDC7FFE1C543A4A0E07 (intptr_t ___0_context, float ___1_width, float ___2_height, float ___3_depth, bool ___4_lockToListenerPosition, float ___5_positionX, float ___6_positionY, float ___7_positionZ, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___8_wallMaterials, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float, float, float, int32_t, float, float, float, float*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float) + sizeof(float) + sizeof(float) + 4 + sizeof(float) + sizeof(float) + sizeof(float) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_SetAdvancedBoxRoomParametersUnity", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____8_wallMaterials_marshaled = NULL;
	if (___8_wallMaterials != NULL)
	{
		____8_wallMaterials_marshaled = reinterpret_cast<float*>((___8_wallMaterials)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetAdvancedBoxRoomParametersUnity)(___0_context, ___1_width, ___2_height, ___3_depth, static_cast<int32_t>(___4_lockToListenerPosition), ___5_positionX, ___6_positionY, ___7_positionZ, ____8_wallMaterials_marshaled);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_width, ___2_height, ___3_depth, static_cast<int32_t>(___4_lockToListenerPosition), ___5_positionX, ___6_positionY, ___7_positionZ, ____8_wallMaterials_marshaled);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetAdvancedBoxRoomParameters_m8E6B517B160C20C7E3BD118720905A77890F262E (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, float ___0_width, float ___1_height, float ___2_depth, bool ___3_lockToListenerPosition, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___4_position, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___5_wallMaterials, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		float L_1 = ___0_width;
		float L_2 = ___1_height;
		float L_3 = ___2_depth;
		bool L_4 = ___3_lockToListenerPosition;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = ___4_position;
		float L_6 = L_5.___x;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7 = ___4_position;
		float L_8 = L_7.___y;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9 = ___4_position;
		float L_10 = L_9.___z;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_11 = ___5_wallMaterials;
		int32_t L_12;
		L_12 = FMODPluginInterface_ovrAudio_SetAdvancedBoxRoomParametersUnity_m7DA5D3DC14A4BD1D8C2BCCDC7FFE1C543A4A0E07(L_0, L_1, L_2, L_3, L_4, L_6, L_8, ((-L_10)), L_11, NULL);
		return L_12;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetRoomClutterFactor_m2EFB3F6AE58AF2632CB872A22272E778AE19D8A8 (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_clutterFactor, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_SetRoomClutterFactor", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____1_clutterFactor_marshaled = NULL;
	if (___1_clutterFactor != NULL)
	{
		____1_clutterFactor_marshaled = reinterpret_cast<float*>((___1_clutterFactor)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetRoomClutterFactor)(___0_context, ____1_clutterFactor_marshaled);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_clutterFactor_marshaled);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetRoomClutterFactor_mC901A7E42F5D9DFDBF40F8D1E2EBBDA788AA5DDB (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___0_clutterFactor, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = ___0_clutterFactor;
		int32_t L_2;
		L_2 = FMODPluginInterface_ovrAudio_SetRoomClutterFactor_m2EFB3F6AE58AF2632CB872A22272E778AE19D8A8(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetSharedReverbWetLevel_mE870E222591BA2DBF39F7B0F90E9D4613687D21C (intptr_t ___0_context, float ___1_linearLevel, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_SetSharedReverbWetLevel", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetSharedReverbWetLevel)(___0_context, ___1_linearLevel);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_linearLevel);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetSharedReverbWetLevel_mAADB1B0D7032FF78E82A01E655D7656246547284 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, float ___0_linearLevel, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		float L_1 = ___0_linearLevel;
		int32_t L_2;
		L_2 = FMODPluginInterface_ovrAudio_SetSharedReverbWetLevel_mE870E222591BA2DBF39F7B0F90E9D4613687D21C(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_Enable_m5C4025D89A4FC0BF1C6C438A07E8CD06003D3D80 (intptr_t ___0_context, int32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_Enable", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_Enable)(___0_context, ___1_what, ___2_enable);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_what, ___2_enable);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetEnabled_mEAF4DC21B81B5A8B436C4F7FA81F36BD41441E42 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, int32_t ___0_feature, bool ___1_enabled, const RuntimeMethod* method) 
{
	int32_t G_B2_0 = 0;
	intptr_t G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	int32_t G_B1_0 = 0;
	intptr_t G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	intptr_t G_B3_2;
	memset((&G_B3_2), 0, sizeof(G_B3_2));
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		int32_t L_1 = ___0_feature;
		bool L_2 = ___1_enabled;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_000d;
		}
		G_B1_0 = L_1;
		G_B1_1 = L_0;
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_000e:
	{
		int32_t L_3;
		L_3 = FMODPluginInterface_ovrAudio_Enable_m5C4025D89A4FC0BF1C6C438A07E8CD06003D3D80(G_B3_2, G_B3_1, G_B3_0, NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_Enable_mA2ED5864A48BE9DF9FAFCFC64D02CF14C90B389B (intptr_t ___0_context, uint32_t ___1_what, int32_t ___2_enable, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, uint32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(uint32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_Enable", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_Enable)(___0_context, ___1_what, ___2_enable);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_what, ___2_enable);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetEnabled_mEA704D20FE812D264C63F9BDEA298021B3C03672 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, uint32_t ___0_feature, bool ___1_enabled, const RuntimeMethod* method) 
{
	uint32_t G_B2_0 = 0;
	intptr_t G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	uint32_t G_B1_0 = 0;
	intptr_t G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	int32_t G_B3_0 = 0;
	uint32_t G_B3_1 = 0;
	intptr_t G_B3_2;
	memset((&G_B3_2), 0, sizeof(G_B3_2));
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		uint32_t L_1 = ___0_feature;
		bool L_2 = ___1_enabled;
		if (L_2)
		{
			G_B2_0 = L_1;
			G_B2_1 = L_0;
			goto IL_000d;
		}
		G_B1_0 = L_1;
		G_B1_1 = L_0;
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_000e:
	{
		int32_t L_3;
		L_3 = FMODPluginInterface_ovrAudio_Enable_mA2ED5864A48BE9DF9FAFCFC64D02CF14C90B389B(G_B3_2, G_B3_1, G_B3_0, NULL);
		return L_3;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mFAABD653FABCB1D9CFA5BB3A271B46DAC8D410AD (intptr_t ___0_context, int32_t ___1_RaysPerSecond, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_SetDynamicRoomRaysPerSecond", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomRaysPerSecond)(___0_context, ___1_RaysPerSecond);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_RaysPerSecond);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetDynamicRoomRaysPerSecond_mCA49CFAD8C0286BB53821C17FAA6C9C7B4E93F5E (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, int32_t ___0_RaysPerSecond, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		int32_t L_1 = ___0_RaysPerSecond;
		int32_t L_2;
		L_2 = FMODPluginInterface_ovrAudio_SetDynamicRoomRaysPerSecond_mFAABD653FABCB1D9CFA5BB3A271B46DAC8D410AD(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetDynamicRoomInterpSpeed_m3750997B8E4AF41DDFF99F60F325293C9C1C2911 (intptr_t ___0_context, float ___1_InterpSpeed, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_SetDynamicRoomInterpSpeed", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomInterpSpeed)(___0_context, ___1_InterpSpeed);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_InterpSpeed);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetDynamicRoomInterpSpeed_m2E448D011F4384A13E516A6A56EB046EE49550E5 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, float ___0_InterpSpeed, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		float L_1 = ___0_InterpSpeed;
		int32_t L_2;
		L_2 = FMODPluginInterface_ovrAudio_SetDynamicRoomInterpSpeed_m3750997B8E4AF41DDFF99F60F325293C9C1C2911(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetDynamicRoomMaxWallDistance_mA99D2DE88BF09741F7BDD426D6DB52BF59FF081F (intptr_t ___0_context, float ___1_MaxWallDistance, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(float);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_SetDynamicRoomMaxWallDistance", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomMaxWallDistance)(___0_context, ___1_MaxWallDistance);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_MaxWallDistance);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetDynamicRoomMaxWallDistance_m2405D770C52293A58F49762C5D77AB8D402E4142 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, float ___0_MaxWallDistance, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		float L_1 = ___0_MaxWallDistance;
		int32_t L_2;
		L_2 = FMODPluginInterface_ovrAudio_SetDynamicRoomMaxWallDistance_mA99D2DE88BF09741F7BDD426D6DB52BF59FF081F(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_mB25ABECB1BCBB987E86929381A78AB1FF2780E34 (intptr_t ___0_context, int32_t ___1_RayCacheSize, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_SetDynamicRoomRaysRayCacheSize", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_SetDynamicRoomRaysRayCacheSize)(___0_context, ___1_RayCacheSize);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ___1_RayCacheSize);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_SetDynamicRoomRaysRayCacheSize_m2DAB0D56DDFC647EE54B0BAE8DEA1E0A8DB0C20E (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, int32_t ___0_RayCacheSize, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		int32_t L_1 = ___0_RayCacheSize;
		int32_t L_2;
		L_2 = FMODPluginInterface_ovrAudio_SetDynamicRoomRaysRayCacheSize_mB25ABECB1BCBB987E86929381A78AB1FF2780E34(L_0, L_1, NULL);
		return L_2;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_GetRoomDimensions_m033D131EB9535C71B1139883929984A30468272B (intptr_t ___0_context, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___2_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___3_position, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, float*, float*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(void*) + sizeof(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_GetRoomDimensions", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	float* ____1_roomDimensions_marshaled = NULL;
	if (___1_roomDimensions != NULL)
	{
		____1_roomDimensions_marshaled = reinterpret_cast<float*>((___1_roomDimensions)->GetAddressAtUnchecked(0));
	}

	float* ____2_reflectionsCoefs_marshaled = NULL;
	if (___2_reflectionsCoefs != NULL)
	{
		____2_reflectionsCoefs_marshaled = reinterpret_cast<float*>((___2_reflectionsCoefs)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_GetRoomDimensions)(___0_context, ____1_roomDimensions_marshaled, ____2_reflectionsCoefs_marshaled, ___3_position);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_roomDimensions_marshaled, ____2_reflectionsCoefs_marshaled, ___3_position);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_GetRoomDimensions_m94AD63F86D422AED49A04674820D62D966903D1F (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___0_roomDimensions, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___1_reflectionsCoefs, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ___2_position, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = ___0_roomDimensions;
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_2 = ___1_reflectionsCoefs;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_3 = ___2_position;
		int32_t L_4;
		L_4 = FMODPluginInterface_ovrAudio_GetRoomDimensions_m033D131EB9535C71B1139883929984A30468272B(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_ovrAudio_GetRaycastHits_m719B1DCA6406E3B150EF35849E62A00877AF68A5 (intptr_t ___0_context, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___2_normals, int32_t ___3_length, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*, int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(void*) + sizeof(void*) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioFMOD"), "ovrAudio_GetRaycastHits", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ____1_points_marshaled = NULL;
	if (___1_points != NULL)
	{
		____1_points_marshaled = reinterpret_cast<Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*>((___1_points)->GetAddressAtUnchecked(0));
	}

	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* ____2_normals_marshaled = NULL;
	if (___2_normals != NULL)
	{
		____2_normals_marshaled = reinterpret_cast<Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2*>((___2_normals)->GetAddressAtUnchecked(0));
	}

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioFMOD_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(ovrAudio_GetRaycastHits)(___0_context, ____1_points_marshaled, ____2_normals_marshaled, ___3_length);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_context, ____1_points_marshaled, ____2_normals_marshaled, ___3_length);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMODPluginInterface_GetRaycastHits_mF3A58BE2796C38EA339D65CF06B83F611772F56E (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___0_points, Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___1_normals, int32_t ___2_length, const RuntimeMethod* method) 
{
	{
		intptr_t L_0;
		L_0 = FMODPluginInterface_get_context_mD2D698D2696EEA2899A7D9A9C65CFAF13892DB95(__this, NULL);
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_1 = ___0_points;
		Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* L_2 = ___1_normals;
		int32_t L_3 = ___2_length;
		int32_t L_4;
		L_4 = FMODPluginInterface_ovrAudio_GetRaycastHits_m719B1DCA6406E3B150EF35849E62A00877AF68A5(L_0, L_1, L_2, L_3, NULL);
		return L_4;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FMODPluginInterface__ctor_m5E680483B10C7ED3C547F49EE1119E1987812F70 (FMODPluginInterface_t829FB8C7A2EC760BC196155384D04A8A47E0CA3D* __this, const RuntimeMethod* method) 
{
	{
		__this->___context_ = 0;
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioRoomAcousticProperties_CheckSceneHasRoom_m455C185C321FAD52A60CD29F1C5EC3B908A45210 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mD43B43D37FEAED2F93891370BC1C58C7B1C878FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectsOfType_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mFF107257026360A211CC4F1F96D7BDD663E93DA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral200684201C7F1A1FAC8184E87AB6C3317669C6EA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral75747A585F3CDC4C996360FACD9A5A2FDD8475BF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA211C98A9BA709F0C0926B4502E5D24500AD7985);
		s_Il2CppMethodInitialized = true;
	}
	MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C* G_B2_0 = NULL;
	MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C* G_B1_0 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C* L_0;
		L_0 = Object_FindObjectsOfType_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mFF107257026360A211CC4F1F96D7BDD663E93DA6(Object_FindObjectsOfType_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mFF107257026360A211CC4F1F96D7BDD663E93DA6_RuntimeMethod_var);
		MetaXRAudioRoomAcousticPropertiesU5BU5D_t03D430121ACB676DC30F57553C02143F71F87F6C* L_1 = L_0;
		NullCheck(L_1);
		if ((((RuntimeArray*)L_1)->max_length))
		{
			G_B2_0 = L_1;
			goto IL_002d;
		}
		G_B1_0 = L_1;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(_stringLiteralA211C98A9BA709F0C0926B4502E5D24500AD7985, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)il2cpp_codegen_object_new(GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		GameObject__ctor_m37D512B05D292F954792225E6C6EEE95293A9B88(L_2, _stringLiteral200684201C7F1A1FAC8184E87AB6C3317669C6EA, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = L_2;
		NullCheck(L_3);
		MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* L_4;
		L_4 = GameObject_AddComponent_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mD43B43D37FEAED2F93891370BC1C58C7B1C878FD(L_3, GameObject_AddComponent_TisMetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9_mD43B43D37FEAED2F93891370BC1C58C7B1C878FD_RuntimeMethod_var);
		NullCheck(L_4);
		MetaXRAudioRoomAcousticProperties_Update_mEB82D666B651960636E16950A917F1B8E525A2AC(L_4, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m6336EBC83591A5DB64EC70C92132824C6E258705(L_3, NULL);
		G_B2_0 = G_B1_0;
	}

IL_002d:
	{
		NullCheck(G_B2_0);
		if ((((int32_t)((int32_t)(((RuntimeArray*)G_B2_0)->max_length))) <= ((int32_t)1)))
		{
			goto IL_003c;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogError_mB00B2B4468EF3CAF041B038D840820FB84C924B2(_stringLiteral75747A585F3CDC4C996360FACD9A5A2FDD8475BF, NULL);
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioRoomAcousticProperties_Update_mEB82D666B651960636E16950A917F1B8E525A2AC (MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NativeInterface_tC83DF7449AC827666016155CF3A3CE1D58B9E9A5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->___rightMaterial;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialPreset_mE8310341438AE1AC2F5AC20DD166596C00B6D58F(__this, 0, L_0, NULL);
		int32_t L_1 = __this->___leftMaterial;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialPreset_mE8310341438AE1AC2F5AC20DD166596C00B6D58F(__this, 1, L_1, NULL);
		int32_t L_2 = __this->___ceilingMaterial;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialPreset_mE8310341438AE1AC2F5AC20DD166596C00B6D58F(__this, 2, L_2, NULL);
		int32_t L_3 = __this->___floorMaterial;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialPreset_mE8310341438AE1AC2F5AC20DD166596C00B6D58F(__this, 3, L_3, NULL);
		int32_t L_4 = __this->___frontMaterial;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialPreset_mE8310341438AE1AC2F5AC20DD166596C00B6D58F(__this, 4, L_4, NULL);
		int32_t L_5 = __this->___backMaterial;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialPreset_mE8310341438AE1AC2F5AC20DD166596C00B6D58F(__this, 5, L_5, NULL);
		RuntimeObject* L_6;
		L_6 = MetaXRAudioNativeInterface_get_Interface_mD282DE6365778E328E4FCB535651ADFFF91AF29B(NULL);
		float L_7 = __this->___width;
		float L_8 = __this->___height;
		float L_9 = __this->___depth;
		bool L_10 = __this->___lockPositionToListener;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_11;
		L_11 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_11);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_12;
		L_12 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_11, NULL);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_13 = __this->___wallMaterials;
		NullCheck(L_6);
		int32_t L_14;
		L_14 = InterfaceFuncInvoker6< int32_t, float, float, float, bool, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* >::Invoke(0, NativeInterface_tC83DF7449AC827666016155CF3A3CE1D58B9E9A5_il2cpp_TypeInfo_var, L_6, L_7, L_8, L_9, L_10, L_12, L_13);
		float L_15 = __this->___clutterFactor;
		V_0 = L_15;
		V_1 = 3;
		goto IL_00a2;
	}

IL_008d:
	{
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_16 = __this->___clutterFactorBands;
		int32_t L_17 = V_1;
		float L_18 = V_0;
		NullCheck(L_16);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(L_17), (float)L_18);
		float L_19 = V_0;
		V_0 = ((float)il2cpp_codegen_multiply(L_19, (0.5f)));
		int32_t L_20 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract(L_20, 1));
	}

IL_00a2:
	{
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		RuntimeObject* L_22;
		L_22 = MetaXRAudioNativeInterface_get_Interface_mD282DE6365778E328E4FCB535651ADFFF91AF29B(NULL);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_23 = __this->___clutterFactorBands;
		NullCheck(L_22);
		int32_t L_24;
		L_24 = InterfaceFuncInvoker1< int32_t, SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* >::Invoke(3, NativeInterface_tC83DF7449AC827666016155CF3A3CE1D58B9E9A5_il2cpp_TypeInfo_var, L_22, L_23);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioRoomAcousticProperties_SetWallMaterialPreset_mE8310341438AE1AC2F5AC20DD166596C00B6D58F (MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* __this, int32_t ___0_wallIndex, int32_t ___1_materialPreset, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___1_materialPreset;
		switch (L_0)
		{
			case 0:
			{
				goto IL_007b;
			}
			case 1:
			{
				goto IL_0097;
			}
			case 2:
			{
				goto IL_00b3;
			}
			case 3:
			{
				goto IL_00cf;
			}
			case 4:
			{
				goto IL_00eb;
			}
			case 5:
			{
				goto IL_0107;
			}
			case 6:
			{
				goto IL_0123;
			}
			case 7:
			{
				goto IL_013f;
			}
			case 8:
			{
				goto IL_015b;
			}
			case 9:
			{
				goto IL_0177;
			}
			case 10:
			{
				goto IL_0193;
			}
			case 11:
			{
				goto IL_01af;
			}
			case 12:
			{
				goto IL_01cb;
			}
			case 13:
			{
				goto IL_01e7;
			}
			case 14:
			{
				goto IL_0203;
			}
			case 15:
			{
				goto IL_021f;
			}
			case 16:
			{
				goto IL_023b;
			}
			case 17:
			{
				goto IL_0257;
			}
			case 18:
			{
				goto IL_0273;
			}
			case 19:
			{
				goto IL_028f;
			}
			case 20:
			{
				goto IL_02ab;
			}
			case 21:
			{
				goto IL_02c7;
			}
			case 22:
			{
				goto IL_02e3;
			}
			case 23:
			{
				goto IL_02ff;
			}
			case 24:
			{
				goto IL_031b;
			}
			case 25:
			{
				goto IL_0337;
			}
			case 26:
			{
				goto IL_0353;
			}
			case 27:
			{
				goto IL_036f;
			}
			case 28:
			{
				goto IL_038b;
			}
		}
	}
	{
		return;
	}

IL_007b:
	{
		int32_t L_1 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_1, (0.488168418f), (0.361475229f), (0.339595377f), (0.498946249f), NULL);
		return;
	}

IL_0097:
	{
		int32_t L_2 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_2, (0.975468814f), (0.972064495f), (0.949180186f), (0.930105388f), NULL);
		return;
	}

IL_00b3:
	{
		int32_t L_3 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_3, (0.975710571f), (0.98332417f), (0.978116691f), (0.970052719f), NULL);
		return;
	}

IL_00cf:
	{
		int32_t L_4 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_4, (0.987633705f), (0.905486643f), (0.583110571f), (0.351053834f), NULL);
		return;
	}

IL_00eb:
	{
		int32_t L_5 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_5, (0.977633715f), (0.859082878f), (0.526479602f), (0.370790422f), NULL);
		return;
	}

IL_0107:
	{
		int32_t L_6 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_6, (0.910534739f), (0.530433178f), (0.29405582f), (0.270105422f), NULL);
		return;
	}

IL_0123:
	{
		int32_t L_7 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_7, (0.99000001f), (0.99000001f), (0.982753932f), (0.980000019f), NULL);
		return;
	}

IL_013f:
	{
		int32_t L_8 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_8, (0.99000001f), (0.98332417f), (0.980000019f), (0.980000019f), NULL);
		return;
	}

IL_015b:
	{
		int32_t L_9 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_9, (0.989408433f), (0.964494646f), (0.922127008f), (0.900105357f), NULL);
		return;
	}

IL_0177:
	{
		int32_t L_10 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_10, (0.635267377f), (0.65223068f), (0.671053469f), (0.789051592f), NULL);
		return;
	}

IL_0193:
	{
		int32_t L_11 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_11, (0.902957916f), (0.940235913f), (0.917584062f), (0.919947326f), NULL);
		return;
	}

IL_01af:
	{
		int32_t L_12 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_12, (0.686494231f), (0.545859993f), (0.310078561f), (0.399473131f), NULL);
		return;
	}

IL_01cb:
	{
		int32_t L_13 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_13, (0.518259346f), (0.503568292f), (0.5786888f), (0.690210819f), NULL);
		return;
	}

IL_01e7:
	{
		int32_t L_14 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_14, (0.655915797f), (0.800631821f), (0.918839693f), (0.92348814f), NULL);
		return;
	}

IL_0203:
	{
		int32_t L_15 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_15, (0.827098966f), (0.950222731f), (0.97460413f), (0.980000019f), NULL);
		return;
	}

IL_021f:
	{
		int32_t L_16 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_16, (0.881126285f), (0.507170796f), (0.131893098f), (0.0103688836f), NULL);
		return;
	}

IL_023b:
	{
		int32_t L_17 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_17, (0.729294717f), (0.373122454f), (0.25531745f), (0.200263441f), NULL);
		return;
	}

IL_0257:
	{
		int32_t L_18 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_18, (0.721240044f), (0.927690148f), (0.93430227f), (0.910105407f), NULL);
		return;
	}

IL_0273:
	{
		int32_t L_19 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_19, (0.975696504f), (0.979106009f), (0.961063504f), (0.950052679f), NULL);
		return;
	}

IL_028f:
	{
		int32_t L_20 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_20, (0.881774724f), (0.924773932f), (0.951497555f), (0.959947288f), NULL);
		return;
	}

IL_02ab:
	{
		int32_t L_21 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_21, (0.844084203f), (0.634624243f), (0.416662872f), (0.400000036f), NULL);
		return;
	}

IL_02c7:
	{
		int32_t L_22 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_22, (0.0f), (0.0f), (0.0f), (0.0f), NULL);
		return;
	}

IL_02e3:
	{
		int32_t L_23 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_23, (0.532252669f), (0.15453577f), (0.0509644151f), (0.0500000119f), NULL);
		return;
	}

IL_02ff:
	{
		int32_t L_24 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_24, (0.793111682f), (0.840140402f), (0.925591767f), (0.979736567f), NULL);
		return;
	}

IL_031b:
	{
		int32_t L_25 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_25, (0.970588267f), (0.971753478f), (0.978309572f), (0.970052719f), NULL);
		return;
	}

IL_0337:
	{
		int32_t L_26 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_26, (0.592423141f), (0.858273327f), (0.917242289f), (0.939999998f), NULL);
		return;
	}

IL_0353:
	{
		int32_t L_27 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_27, (0.812957883f), (0.895329595f), (0.941304684f), (0.949947298f), NULL);
		return;
	}

IL_036f:
	{
		int32_t L_28 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_28, (0.852366328f), (0.898992121f), (0.934784114f), (0.930052698f), NULL);
		return;
	}

IL_038b:
	{
		int32_t L_29 = ___0_wallIndex;
		MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41(__this, L_29, (0.959999979f), (0.941232264f), (0.937923789f), (0.930052698f), NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioRoomAcousticProperties_SetWallMaterialProperties_mEB84316AB48F153A23C03A9B50BBBE67A900EE41 (MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* __this, int32_t ___0_wallIndex, float ___1_band0, float ___2_band1, float ___3_band2, float ___4_band3, const RuntimeMethod* method) 
{
	{
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_0 = __this->___wallMaterials;
		int32_t L_1 = ___0_wallIndex;
		float L_2 = ___1_band0;
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_multiply(L_1, 4))), (float)L_2);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_3 = __this->___wallMaterials;
		int32_t L_4 = ___0_wallIndex;
		float L_5 = ___2_band1;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_4, 4)), 1))), (float)L_5);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_6 = __this->___wallMaterials;
		int32_t L_7 = ___0_wallIndex;
		float L_8 = ___3_band2;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_7, 4)), 2))), (float)L_8);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_9 = __this->___wallMaterials;
		int32_t L_10 = ___0_wallIndex;
		float L_11 = ___4_band3;
		NullCheck(L_9);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_10, 4)), 3))), (float)L_11);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioRoomAcousticProperties__ctor_m2788AB8887747A67F08B70A6E02B535058904BB7 (MetaXRAudioRoomAcousticProperties_t9E605CE8F232C8B8B455DC23F1469508556480C9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->___lockPositionToListener = (bool)1;
		__this->___width = (8.0f);
		__this->___height = (3.0f);
		__this->___depth = (5.0f);
		__this->___leftMaterial = ((int32_t)17);
		__this->___rightMaterial = ((int32_t)17);
		__this->___floorMaterial = 3;
		__this->___frontMaterial = ((int32_t)17);
		__this->___backMaterial = ((int32_t)17);
		__this->___clutterFactor = (0.5f);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_0 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->___clutterFactorBands = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___clutterFactorBands), (void*)L_0);
		SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* L_1 = (SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C*)SZArrayNew(SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C_il2cpp_TypeInfo_var, (uint32_t)((int32_t)24));
		__this->___wallMaterials = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___wallMaterials), (void*)L_1);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* MetaXRAudioSettings_get_Instance_mB453230A57D314E0508F6A012987114BE46160F1 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE5B0617A5034C61150CB4DD33C88B0A6E9BEE08F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ScriptableObject_CreateInstance_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE3762098D5E2575BBDA769870A8F2FA3B714BBBC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1ADACE024A6187363E04C29B4F53D872AC3252C9);
		s_Il2CppMethodInitialized = true;
	}
	{
		MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* L_0 = ((MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var))->___instance;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0033;
		}
	}
	{
		MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* L_2;
		L_2 = Resources_Load_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE5B0617A5034C61150CB4DD33C88B0A6E9BEE08F(_stringLiteral1ADACE024A6187363E04C29B4F53D872AC3252C9, Resources_Load_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE5B0617A5034C61150CB4DD33C88B0A6E9BEE08F_RuntimeMethod_var);
		((MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var))->___instance = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var))->___instance), (void*)L_2);
		MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* L_3 = ((MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var))->___instance;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* L_5;
		L_5 = ScriptableObject_CreateInstance_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE3762098D5E2575BBDA769870A8F2FA3B714BBBC(ScriptableObject_CreateInstance_TisMetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_mE3762098D5E2575BBDA769870A8F2FA3B714BBBC_RuntimeMethod_var);
		((MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var))->___instance = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&((MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var))->___instance), (void*)L_5);
	}

IL_0033:
	{
		MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* L_6 = ((MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_StaticFields*)il2cpp_codegen_static_fields_for(MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9_il2cpp_TypeInfo_var))->___instance;
		return L_6;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSettings__ctor_m550A5B742E2FA68A0AE916975C98AFC4354625D8 (MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* __this, const RuntimeMethod* method) 
{
	{
		__this->___voiceLimit = ((int32_t)64);
		ScriptableObject__ctor_mD037FDB0B487295EA47F79A4DB1BF1846C9087FF(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_OnBeforeSceneLoadRuntimeMethod_m52DECD660A50BD6434F6DB531D2FD4BCBF42BD47 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE608673B918A04D6FADE442B9611EB3CF4FC8500);
		s_Il2CppMethodInitialized = true;
	}
	{
		MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* L_0;
		L_0 = MetaXRAudioSettings_get_Instance_mB453230A57D314E0508F6A012987114BE46160F1(NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->___voiceLimit;
		int32_t L_2 = L_1;
		RuntimeObject* L_3 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_2);
		String_t* L_4;
		L_4 = String_Format_mA8DBB4C2516B9723C5A41E6CB1E2FAF4BBE96DD8(_stringLiteralE608673B918A04D6FADE442B9611EB3CF4FC8500, L_3, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(L_4, NULL);
		MetaXRAudioSettings_t76602DA396AC738EC47E2C6D7F76C7B4BA2B25F9* L_5;
		L_5 = MetaXRAudioSettings_get_Instance_mB453230A57D314E0508F6A012987114BE46160F1(NULL);
		NullCheck(L_5);
		int32_t L_6 = L_5->___voiceLimit;
		int32_t L_7;
		L_7 = MetaXRAudioSource_MetaXRAudio_SetGlobalVoiceLimit_m6305F25F5078BA076388898576E453D281578FAE(L_6, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MetaXRAudioSource_get_EnableSpatialization_m5220DBFF3A337BA119766C052E1111A807A397C0 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___enableSpatialization;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_set_EnableSpatialization_mE88DB02F1B601B42D87259E27568953DFF411FB0 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___enableSpatialization = L_0;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MetaXRAudioSource_get_GainBoostDb_m881DA6E5AC2B002413A5DD27CCB18989096D9619 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___gainBoostDb;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_set_GainBoostDb_m833C24BCE17A89200A3720015C09D30BA49B21DB (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		float L_1;
		L_1 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(L_0, (0.0f), (20.0f), NULL);
		__this->___gainBoostDb = L_1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MetaXRAudioSource_get_EnableAcoustics_m022030218248A46EDB269383A6590980A7C911B5 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) 
{
	{
		bool L_0 = __this->___enableAcoustics;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_set_EnableAcoustics_mAD8ED3F63C17EF50996CA9DC24314F77FDB9D7C5 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, bool ___0_value, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_value;
		__this->___enableAcoustics = L_0;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MetaXRAudioSource_get_ReverbSendDb_mB25F0BEC5AE052E6ADE5F7012F61A37D11D58336 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___reverbSendDb;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_set_ReverbSendDb_m4F45AB54316190429B164D923E0F988DEF8C6E54 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		float L_1;
		L_1 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(L_0, (-60.0f), (20.0f), NULL);
		__this->___reverbSendDb = L_1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_Awake_mC5DAEE4E3C851ECE31A9A72097BA9D4F3E7C7213 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0;
		L_0 = Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B(__this, Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		__this->___source_ = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___source_), (void*)L_0);
		MetaXRAudioSource_UpdateParameters_m660E4EC58BEFF69332BF34E6C1FA819195CB44E0(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_Update_m0450DB433611D99275A3F4C2DD49E52672383B11 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0 = __this->___source_;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_2;
		L_2 = Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B(__this, Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		__this->___source_ = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___source_), (void*)L_2);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_3 = __this->___source_;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		MetaXRAudioSource_UpdateParameters_m660E4EC58BEFF69332BF34E6C1FA819195CB44E0(__this, NULL);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_5 = __this->___source_;
		NullCheck(L_5);
		bool L_6;
		L_6 = AudioSource_get_isPlaying_mC203303F2F7146B2C056CB47B9391463FDF408FC(L_5, NULL);
		__this->___wasPlaying_ = L_6;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource_UpdateParameters_m660E4EC58BEFF69332BF34E6C1FA819195CB44E0 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) 
{
	int32_t G_B2_0 = 0;
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	int32_t G_B3_1 = 0;
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* G_B3_2 = NULL;
	{
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0 = __this->___source_;
		bool L_1 = __this->___enableSpatialization;
		NullCheck(L_0);
		AudioSource_set_spatialize_mDFA357EDCB0C59EF11F53C845F7ACBF6BF7F7B3C(L_0, L_1, NULL);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_2 = __this->___source_;
		float L_3 = __this->___gainBoostDb;
		NullCheck(L_2);
		bool L_4;
		L_4 = AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB(L_2, 0, L_3, NULL);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_5 = __this->___source_;
		bool L_6 = __this->___enableAcoustics;
		if (L_6)
		{
			G_B2_0 = 5;
			G_B2_1 = L_5;
			goto IL_003a;
		}
		G_B1_0 = 5;
		G_B1_1 = L_5;
	}
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_003f;
	}

IL_003a:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_003f:
	{
		NullCheck(G_B3_2);
		bool L_7;
		L_7 = AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB(G_B3_2, G_B3_1, G_B3_0, NULL);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_8 = __this->___source_;
		float L_9 = __this->___reverbSendDb;
		NullCheck(L_8);
		bool L_10;
		L_10 = AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB(L_8, ((int32_t)11), L_9, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MetaXRAudioSource_MetaXRAudio_SetGlobalVoiceLimit_m6305F25F5078BA076388898576E453D281578FAE (int32_t ___0_VoiceLimit, const RuntimeMethod* method) 
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "MetaXRAudio_SetGlobalVoiceLimit", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(MetaXRAudio_SetGlobalVoiceLimit)(___0_VoiceLimit);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___0_VoiceLimit);
	#endif

	return returnValue;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSource__ctor_mC31D559BD5A9F59CA1BB3B6B9FDE931D7EEF3C62 (MetaXRAudioSource_t8D8D167DE494424AE3328EA5D966513EEECF74E0* __this, const RuntimeMethod* method) 
{
	{
		__this->___enableSpatialization = (bool)1;
		__this->___enableAcoustics = (bool)1;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MetaXRAudioSourceExperimentalFeatures_get_HrtfIntensity_mD4A6A3D036ABBFA09CF605F15CB490E3380F36FC (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___hrtfIntensity;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_set_HrtfIntensity_m1B3B94C7401C543A682764F80ED2A61A6917FC6F (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		float L_1;
		L_1 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(L_0, (0.0f), (1.0f), NULL);
		__this->___hrtfIntensity = L_1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MetaXRAudioSourceExperimentalFeatures_get_VolumetricRadius_m55179A633D431FF5AA285D99B8ADE1C5B23861B4 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___volumetricRadius;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_set_VolumetricRadius_m1669D6C4563E238DA1F63D88C248C1D7718B9D84 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		float L_1;
		L_1 = Mathf_Max_mF5379E63D2BBAC76D090748695D833934F8AD051_inline(L_0, (0.0f), NULL);
		__this->___volumetricRadius = L_1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MetaXRAudioSourceExperimentalFeatures_get_EarlyReflectionsSendDb_mF01D6818CE03191E5B2EB450E4A6607A553686C1 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___earlyReflectionsSendDb;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_set_EarlyReflectionsSendDb_m76521ECC77434A828F50B4A3315C3C7362B2B59C (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		float L_1;
		L_1 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(L_0, (-60.0f), (20.0f), NULL);
		__this->___earlyReflectionsSendDb = L_1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MetaXRAudioSourceExperimentalFeatures_get_DirectivityIntensity_m1CB58E47A2A608940797E8AA03209160EEF25CDD (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___directivityIntensity;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_set_DirectivityIntensity_mBAD7CB7792ED5CA767E56D256EEF885CD04A0134 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		float L_1;
		L_1 = Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline(L_0, (0.0f), (1.0f), NULL);
		__this->___directivityIntensity = L_1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MetaXRAudioSourceExperimentalFeatures_get_DirectivityPattern_mF1F309350B794560DF2685CB8B0D7F9950547809 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->___directivityPattern;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_set_DirectivityPattern_m8560170064B0D6A1FE972F058F768A3E9513D955 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, int32_t ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = ___0_value;
		__this->___directivityPattern = L_0;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_OnValidate_mD2F0FFFA9E3CA3028EC84696FB60F1D03032944F (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___volumetricRadius;
		float L_1;
		L_1 = Mathf_Max_mF5379E63D2BBAC76D090748695D833934F8AD051_inline(L_0, (0.0f), NULL);
		__this->___volumetricRadius = L_1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_Awake_mBF310BC8962FFDE0C490DC6F118B21475C15279B (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0;
		L_0 = Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B(__this, Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		__this->___source_ = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___source_), (void*)L_0);
		MetaXRAudioSourceExperimentalFeatures_UpdateParameters_mC0FAB747523030BE2519777E5E658920E97928D8(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_Update_mB9337C079A970C2F8BC3D28DF65CF2A23E8E039B (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0 = __this->___source_;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_0, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_2;
		L_2 = Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B(__this, Component_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m42DA4DEA19EB60D80CBED7413ADEB27FA033C77B_RuntimeMethod_var);
		__this->___source_ = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___source_), (void*)L_2);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_3 = __this->___source_;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_3, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		MetaXRAudioSourceExperimentalFeatures_UpdateParameters_mC0FAB747523030BE2519777E5E658920E97928D8(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_UpdateParameters_mC0FAB747523030BE2519777E5E658920E97928D8 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	int32_t G_B2_0 = 0;
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* G_B1_1 = NULL;
	float G_B3_0 = 0.0f;
	int32_t G_B3_1 = 0;
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* G_B3_2 = NULL;
	{
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0 = __this->___source_;
		float L_1 = __this->___hrtfIntensity;
		NullCheck(L_0);
		bool L_2;
		L_2 = AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB(L_0, ((int32_t)9), L_1, NULL);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_3 = __this->___source_;
		float L_4 = __this->___directivityIntensity;
		NullCheck(L_3);
		bool L_5;
		L_5 = AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB(L_3, ((int32_t)13), L_4, NULL);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_6 = __this->___source_;
		float L_7 = __this->___volumetricRadius;
		NullCheck(L_6);
		bool L_8;
		L_8 = AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB(L_6, 4, L_7, NULL);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_9 = __this->___source_;
		float L_10 = __this->___earlyReflectionsSendDb;
		NullCheck(L_9);
		bool L_11;
		L_11 = AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB(L_9, ((int32_t)10), L_10, NULL);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_12 = __this->___source_;
		int32_t L_13 = __this->___directivityPattern;
		if (!L_13)
		{
			G_B2_0 = ((int32_t)12);
			G_B2_1 = L_12;
			goto IL_0066;
		}
		G_B1_0 = ((int32_t)12);
		G_B1_1 = L_12;
	}
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_006b;
	}

IL_0066:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_006b:
	{
		NullCheck(G_B3_2);
		bool L_14;
		L_14 = AudioSource_SetSpatializerFloat_m124ADF8D1FB75E1677A8891D9BF7138FD8398ADB(G_B3_2, G_B3_1, G_B3_0, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures_MetaXRAudio_GetGlobalRoomReflectionValues_m78E61A685614FD113462779F70B6AE6CDE70BA2C (bool* ___0_reflOn, bool* ___1_reverbOn, float* ___2_width, float* ___3_height, float* ___4_length, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*, int32_t*, float*, float*, float*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t*) + sizeof(int32_t*) + sizeof(float*) + sizeof(float*) + sizeof(float*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "MetaXRAudio_GetGlobalRoomReflectionValues", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	int32_t* ____0_reflOn_marshaled = NULL;
	int32_t ____0_reflOn_marshaled_dereferenced = 0;
	____0_reflOn_marshaled_dereferenced = static_cast<int32_t>(*___0_reflOn);
	____0_reflOn_marshaled = &____0_reflOn_marshaled_dereferenced;

	int32_t* ____1_reverbOn_marshaled = NULL;
	int32_t ____1_reverbOn_marshaled_dereferenced = 0;
	____1_reverbOn_marshaled_dereferenced = static_cast<int32_t>(*___1_reverbOn);
	____1_reverbOn_marshaled = &____1_reverbOn_marshaled_dereferenced;

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	reinterpret_cast<PInvokeFunc>(MetaXRAudio_GetGlobalRoomReflectionValues)(____0_reflOn_marshaled, ____1_reverbOn_marshaled, ___2_width, ___3_height, ___4_length);
	#else
	il2cppPInvokeFunc(____0_reflOn_marshaled, ____1_reverbOn_marshaled, ___2_width, ___3_height, ___4_length);
	#endif

	bool _____0_reflOn_marshaled_unmarshaled_dereferenced = false;
	_____0_reflOn_marshaled_unmarshaled_dereferenced = static_cast<bool>(*____0_reflOn_marshaled);
	*___0_reflOn = _____0_reflOn_marshaled_unmarshaled_dereferenced;

	bool _____1_reverbOn_marshaled_unmarshaled_dereferenced = false;
	_____1_reverbOn_marshaled_unmarshaled_dereferenced = static_cast<bool>(*____1_reverbOn_marshaled);
	*___1_reverbOn = _____1_reverbOn_marshaled_unmarshaled_dereferenced;

}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioSourceExperimentalFeatures__ctor_m32153734A56D0CEA75BA227E9652D37B4A95FE03 (MetaXRAudioSourceExperimentalFeatures_tBD6A37EFAA35B94A74AABB4C3674290FF83495B3* __this, const RuntimeMethod* method) 
{
	{
		__this->___hrtfIntensity = (1.0f);
		__this->___directivityIntensity = (1.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioVersion_Awake_m929199F9C4C78F1C7DCC9FD7771DFD42F94659C5 (MetaXRAudioVersion_tB1D8AEFA0C1C87DA3EFD4F545CD4A1370BFABAFB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisRuntimeObject_mFB8A63D602BB6974D31E20300D9EB89C6FE7C278_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral12998AE26ABA2BB1BEEF18B0A944908860EB5E37);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		V_2 = 0;
		MetaXRAudioVersion_MetaXRAudio_GetVersion_m18A5D74F925242105267EAD291C8A62A2C829483((&V_0), (&V_1), (&V_2), NULL);
		int32_t L_0 = V_0;
		int32_t L_1 = L_0;
		RuntimeObject* L_2 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_1);
		int32_t L_3 = V_1;
		int32_t L_4 = L_3;
		RuntimeObject* L_5 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_4);
		int32_t L_6 = V_2;
		int32_t L_7 = L_6;
		RuntimeObject* L_8 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9;
		L_9 = String_Format_mA0534D6E2AE4D67A6BD8D45B3321323930EB930C(_stringLiteral12998AE26ABA2BB1BEEF18B0A944908860EB5E37, L_2, L_5, L_8, NULL);
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_10;
		L_10 = Array_Empty_TisRuntimeObject_mFB8A63D602BB6974D31E20300D9EB89C6FE7C278_inline(Array_Empty_TisRuntimeObject_mFB8A63D602BB6974D31E20300D9EB89C6FE7C278_RuntimeMethod_var);
		String_t* L_11;
		L_11 = String_Format_m918500C1EFB475181349A79989BB79BB36102894(L_9, L_10, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m87A9A3C761FF5C43ED8A53B16190A53D08F818BB(L_11, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioVersion_MetaXRAudio_GetVersion_m18A5D74F925242105267EAD291C8A62A2C829483 (int32_t* ___0_Major, int32_t* ___1_Minor, int32_t* ___2_Patch, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t*, int32_t*, int32_t*);
	#if !FORCE_PINVOKE_INTERNAL && !FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("MetaXRAudioUnity"), "MetaXRAudio_GetVersion", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	#if FORCE_PINVOKE_INTERNAL || FORCE_PINVOKE_MetaXRAudioUnity_INTERNAL
	reinterpret_cast<PInvokeFunc>(MetaXRAudio_GetVersion)(___0_Major, ___1_Minor, ___2_Patch);
	#else
	il2cppPInvokeFunc(___0_Major, ___1_Minor, ___2_Patch);
	#endif

}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MetaXRAudioVersion__ctor_m3FB3B215A5CBC920DA061D509BF9EE63C5E2AFEA (MetaXRAudioVersion_tB1D8AEFA0C1C87DA3EFD4F545CD4A1370BFABAFB* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA UnitySourceGeneratedAssemblyMonoScriptTypes_v1_Get_mDDF9E5EAA4299706C848A5D8BEAA2EF2120A0954 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tA72208D6996CCF18BC3CE460CDC3978BBD2B8522____234C2E3E1D1C65B2B9CB2842053B24AC13F7EB20C1380DCD86D285E004DBE831_FieldInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPrivateImplementationDetailsU3E_tA72208D6996CCF18BC3CE460CDC3978BBD2B8522____3B2ED60CD250EEDD588288ACD6FFE84476EC170DAF5874F47A73D791C28D76A7_FieldInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		il2cpp_codegen_initobj((&V_0), sizeof(MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_0 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)618));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_1 = L_0;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_2 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tA72208D6996CCF18BC3CE460CDC3978BBD2B8522____3B2ED60CD250EEDD588288ACD6FFE84476EC170DAF5874F47A73D791C28D76A7_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_1, L_2, NULL);
		(&V_0)->___FilePathsData = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___FilePathsData), (void*)L_1);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_3 = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)SZArrayNew(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031_il2cpp_TypeInfo_var, (uint32_t)((int32_t)430));
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_4 = L_3;
		RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 L_5 = { reinterpret_cast<intptr_t> (U3CPrivateImplementationDetailsU3E_tA72208D6996CCF18BC3CE460CDC3978BBD2B8522____234C2E3E1D1C65B2B9CB2842053B24AC13F7EB20C1380DCD86D285E004DBE831_FieldInfo_var) };
		RuntimeHelpers_InitializeArray_m751372AA3F24FBF6DA9B9D687CBFA2DE436CAB9B((RuntimeArray*)L_4, L_5, NULL);
		(&V_0)->___TypesData = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&(&V_0)->___TypesData), (void*)L_4);
		(&V_0)->___TotalFiles = 6;
		(&V_0)->___TotalTypes = ((int32_t)11);
		(&V_0)->___IsEditorOnly = (bool)0;
		MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA L_6 = V_0;
		return L_6;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnitySourceGeneratedAssemblyMonoScriptTypes_v1__ctor_m6249C62854218E54887E2FA6E7FF572DC3B46BC0 (UnitySourceGeneratedAssemblyMonoScriptTypes_v1_tC150D17495A9C163C732D9D462389E6DA360B914* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C void MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshal_pinvoke(const MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA& unmarshaled, MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshaled_pinvoke& marshaled)
{
	marshaled.___FilePathsData = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___FilePathsData);
	marshaled.___TypesData = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___TypesData);
	marshaled.___TotalTypes = unmarshaled.___TotalTypes;
	marshaled.___TotalFiles = unmarshaled.___TotalFiles;
	marshaled.___IsEditorOnly = static_cast<int32_t>(unmarshaled.___IsEditorOnly);
}
IL2CPP_EXTERN_C void MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshal_pinvoke_back(const MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshaled_pinvoke& marshaled, MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.___FilePathsData = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___FilePathsData);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___FilePathsData), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___FilePathsData));
	unmarshaled.___TypesData = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___TypesData);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___TypesData), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___TypesData));
	int32_t unmarshaledTotalTypes_temp_2 = 0;
	unmarshaledTotalTypes_temp_2 = marshaled.___TotalTypes;
	unmarshaled.___TotalTypes = unmarshaledTotalTypes_temp_2;
	int32_t unmarshaledTotalFiles_temp_3 = 0;
	unmarshaledTotalFiles_temp_3 = marshaled.___TotalFiles;
	unmarshaled.___TotalFiles = unmarshaledTotalFiles_temp_3;
	bool unmarshaledIsEditorOnly_temp_4 = false;
	unmarshaledIsEditorOnly_temp_4 = static_cast<bool>(marshaled.___IsEditorOnly);
	unmarshaled.___IsEditorOnly = unmarshaledIsEditorOnly_temp_4;
}
IL2CPP_EXTERN_C void MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshal_pinvoke_cleanup(MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___FilePathsData);
	marshaled.___FilePathsData = NULL;
	il2cpp_codegen_com_destroy_safe_array(marshaled.___TypesData);
	marshaled.___TypesData = NULL;
}
IL2CPP_EXTERN_C void MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshal_com(const MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA& unmarshaled, MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshaled_com& marshaled)
{
	marshaled.___FilePathsData = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___FilePathsData);
	marshaled.___TypesData = il2cpp_codegen_com_marshal_safe_array(IL2CPP_VT_I1, unmarshaled.___TypesData);
	marshaled.___TotalTypes = unmarshaled.___TotalTypes;
	marshaled.___TotalFiles = unmarshaled.___TotalFiles;
	marshaled.___IsEditorOnly = static_cast<int32_t>(unmarshaled.___IsEditorOnly);
}
IL2CPP_EXTERN_C void MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshal_com_back(const MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshaled_com& marshaled, MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	unmarshaled.___FilePathsData = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___FilePathsData);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___FilePathsData), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___FilePathsData));
	unmarshaled.___TypesData = (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___TypesData);
	Il2CppCodeGenWriteBarrier((void**)(&unmarshaled.___TypesData), (void*)(ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031*)il2cpp_codegen_com_marshal_safe_array_result(IL2CPP_VT_I1, Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3_il2cpp_TypeInfo_var, marshaled.___TypesData));
	int32_t unmarshaledTotalTypes_temp_2 = 0;
	unmarshaledTotalTypes_temp_2 = marshaled.___TotalTypes;
	unmarshaled.___TotalTypes = unmarshaledTotalTypes_temp_2;
	int32_t unmarshaledTotalFiles_temp_3 = 0;
	unmarshaledTotalFiles_temp_3 = marshaled.___TotalFiles;
	unmarshaled.___TotalFiles = unmarshaledTotalFiles_temp_3;
	bool unmarshaledIsEditorOnly_temp_4 = false;
	unmarshaledIsEditorOnly_temp_4 = static_cast<bool>(marshaled.___IsEditorOnly);
	unmarshaled.___IsEditorOnly = unmarshaledIsEditorOnly_temp_4;
}
IL2CPP_EXTERN_C void MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshal_com_cleanup(MonoScriptData_t05A2CC34D21B72FC91527E9733E3A3371B65F5FA_marshaled_com& marshaled)
{
	il2cpp_codegen_com_destroy_safe_array(marshaled.___FilePathsData);
	marshaled.___FilePathsData = NULL;
	il2cpp_codegen_com_destroy_safe_array(marshaled.___TypesData);
	marshaled.___TypesData = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Clamp_mEB9AEA827D27D20FCC787F7375156AF46BB12BBF_inline (float ___0_value, float ___1_min, float ___2_max, const RuntimeMethod* method) 
{
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		float L_0 = ___0_value;
		float L_1 = ___1_min;
		V_0 = (bool)((((float)L_0) < ((float)L_1))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000e;
		}
	}
	{
		float L_3 = ___1_min;
		___0_value = L_3;
		goto IL_0019;
	}

IL_000e:
	{
		float L_4 = ___0_value;
		float L_5 = ___2_max;
		V_1 = (bool)((((float)L_4) > ((float)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0019;
		}
	}
	{
		float L_7 = ___2_max;
		___0_value = L_7;
	}

IL_0019:
	{
		float L_8 = ___0_value;
		V_2 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		float L_9 = V_2;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Mathf_Max_mF5379E63D2BBAC76D090748695D833934F8AD051_inline (float ___0_a, float ___1_b, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		float L_0 = ___0_a;
		float L_1 = ___1_b;
		if ((((float)L_0) > ((float)L_1)))
		{
			goto IL_0008;
		}
	}
	{
		float L_2 = ___1_b;
		G_B3_0 = L_2;
		goto IL_0009;
	}

IL_0008:
	{
		float L_3 = ___0_a;
		G_B3_0 = L_3;
	}

IL_0009:
	{
		V_0 = G_B3_0;
		goto IL_000c;
	}

IL_000c:
	{
		float L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* Array_Empty_TisIl2CppFullySharedGenericAny_m244E2A51B33F845A2093F0862FBCE502E4FDD868_gshared_inline (const RuntimeMethod* method) 
{
	il2cpp_rgctx_method_init(method);
	{
		il2cpp_codegen_runtime_class_init_inline(il2cpp_rgctx_data(method->rgctx_data, 2));
		__Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* L_0 = ((EmptyArray_1_tF69A5F6BAD1150A16C4C98B346D6122FE3751C80_StaticFields*)il2cpp_codegen_static_fields_for(il2cpp_rgctx_data(method->rgctx_data, 2)))->___Value;
		return L_0;
	}
}
