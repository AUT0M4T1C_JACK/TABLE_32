﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif





struct Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87;
struct Dictionary_2_tB18B7D2DF248968973DD36692A4049966CA9F348;
struct Dictionary_2_t41165BF747F041590086BE39A59BE164430A3CEF;
struct HashSet_1_t2EC13BE6E93BB0C99D5CF97A25799B40FD6CBAF4;
struct HashSet_1_t2F33BEB06EEA4A872E2FAF464382422AA39AE885;
struct IEnumerable_1_tF95C9E01A913DD50575531C8305932628663D9E9;
struct IEnumerable_1_tDFE91FA569BD0F8AF243E1AB885C63ABE1F96024;
struct IReadOnlyList_1_t096750C6D09536A8131A83E4ACF863B54ADEE544;
struct ISet_1_t99684AFB4328AA5E791E8A6257C00E6756E425AE;
struct List_1_t79088B486A8568CE52C04FFE5C5A7CFE76331E80;
struct List_1_t70EE7982F45810D4B024CF720D910E67974A3094;
struct Task_1_t4C228DE57804012969575431CFF12D57C875552D;
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C;
struct QuatfU5BU5D_t866C516DA0FC85581934D10E587D323B1B89E3BF;
struct SpaceComponentTypeU5BU5D_t0B1B0FC97F1326A6BC34EEDFB77B9BB241D9EB80;
struct Vector3fU5BU5D_tD8395E99259411E2F0A4F513559CC986FD7AB92E;
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
struct Font_tC95270EA3198038970422D78B74A7F2E218A96B6;
struct FontAsset_t61A6446D934E582651044E33D250EA8D306AB958;
struct Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E;
struct Hashtable_tEFC3B6496E6747787D8BB761B51F2AE3A8CFFE2D;
struct IPinnable_tA3989EA495C0118966BAAF8848C0009947BB49C0;
struct JSONNode_t09FA149506F31AC2019A0E463804342305FA71A6;
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
struct MemberInfo_t;
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
struct String_t;
struct StyleComplexSelector_tE46C29F65FDBA48D3152781187401C8B55B7D8AD;
struct StyleSheet_t6FAF43FCDB45BC6BED0522A222FD4C1A9BB10428;
struct StyleValueCollection_t5ADC08D23E648FBE78F2C161494786E6C83E1377;
struct Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572;
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
struct Type_t;
struct VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC;
struct VisualElement_t2667F9D19E62C7A315927506C06F223AB9234115;
struct VisualTreeAsset_tFB5BF81F0780A412AE5A7C2C552B3EEA64EA2EEB;
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
struct unitytls_tlsctx_read_callback_tDBE877327789CABE940C2A724EC9A5D142318851;
struct unitytls_tlsctx_write_callback_t5D4B64AD846D04E819A49689F7EAA47365636611;

struct Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E_marshaled_com;
struct Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A;
struct Vector3f_t232AF83B4642C67BE8EFF85D8E1599D3B06BD562;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};
struct ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ____array;
	int32_t ____offset;
	int32_t ____count;
};
struct CustomStyleProperty_1_tE4B20CAB5BCFEE711EB4A26F077DC700987C0C2D 
{
	String_t* ___U3CnameU3Ek__BackingField;
};
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField;
};
#endif
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField;
};
#endif
struct CustomStyleProperty_1_t6871E5DBF19AB4DC7E1134B32A03B7A458D52E9F 
{
	String_t* ___U3CnameU3Ek__BackingField;
};
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField;
};
#endif
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField;
};
#endif
struct CustomStyleProperty_1_t697913D630F101B4E464B7E9EA06368015C9C266 
{
	String_t* ___U3CnameU3Ek__BackingField;
};
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField;
};
#endif
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField;
};
#endif
struct CustomStyleProperty_1_t21332918528099194FD36C74FF0FA14696F39493 
{
	String_t* ___U3CnameU3Ek__BackingField;
};
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField;
};
#endif
#ifndef CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
#define CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com_define
struct CustomStyleProperty_1_t8315EF5D1C5F5FB5F920B77E40695C07DAAB349A_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField;
};
#endif
struct Enumerator_t0C52A6D0DB109DF239C883D1C65BF615A5F42109 
{
	List_1_t79088B486A8568CE52C04FFE5C5A7CFE76331E80* ____list;
	int32_t ____index;
	int32_t ____version;
	JSONNode_t09FA149506F31AC2019A0E463804342305FA71A6* ____current;
};
struct Enumerator_t72556E98D7DDBE118A973D782D523D15A96461C8 
{
	HashSet_1_t2F33BEB06EEA4A872E2FAF464382422AA39AE885* ____set;
	int32_t ____index;
	int32_t ____version;
	RuntimeObject* ____current;
};
struct InteractableSet_t934434D7D540BCC55D05BEF15E0116ADEEF0F987 
{
	RuntimeObject* ____data;
	RuntimeObject* ____onlyInclude;
	RuntimeObject* ____testAgainst;
};
struct JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5 
{
	RuntimeObject* ____enumerable;
};
struct KeyValuePair_2_tDC26B09C26BA829DDE331BCB6AF7C508C763D7A3 
{
	int32_t ___key;
	RuntimeObject* ___value;
};
struct KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 
{
	RuntimeObject* ___key;
	RuntimeObject* ___value;
};
struct KeyValuePair_2_tC86602D5307A611B8935C881185C39812E7D59DF 
{
	String_t* ___key;
	JSONNode_t09FA149506F31AC2019A0E463804342305FA71A6* ___value;
};
struct Memory_1_tB7CEF4416F5014E364267478CEF016A4AC5C0036 
{
	RuntimeObject* ____object;
	int32_t ____index;
	int32_t ____length;
};
#ifndef Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_pinvoke_define
#define Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_pinvoke_define
struct Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_pinvoke
{
	Il2CppIUnknown* ____object;
	int32_t ____index;
	int32_t ____length;
};
#endif
#ifndef Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_com_define
#define Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_com_define
struct Memory_1_t56F63672B8E752B13E0BBBBD034BA3C1F6CFDC17_marshaled_com
{
	Il2CppIUnknown* ____object;
	int32_t ____index;
	int32_t ____length;
};
#endif
struct Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 
{
	bool ___hasValue;
	bool ___value;
};
struct Nullable_1_tEB6689CC9747A3600689077DCBF77B8E8B510505 
{
	bool ___hasValue;
	uint8_t ___value;
};
struct Nullable_1_tD52F1D0FC7EBB336F119BE953E59F426766032C1 
{
	bool ___hasValue;
	Il2CppChar ___value;
};
struct Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 
{
	bool ___hasValue;
	double ___value;
};
struct Nullable_1_t57D99A484501B89DA27E67D6D9A89722D5A7DE2C 
{
	bool ___hasValue;
	int16_t ___value;
};
struct Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 
{
	bool ___hasValue;
	int32_t ___value;
};
struct Nullable_1_t365991B3904FDA7642A788423B28692FDC7CDB17 
{
	bool ___hasValue;
	int64_t ___value;
};
struct Nullable_1_tCF16C2638810B89EAA3EEFE6B35FC71B6AE96B2C 
{
	bool ___hasValue;
	int8_t ___value;
};
struct Nullable_1_t3D746CBB6123D4569FF4DEA60BC4240F32C6FE75 
{
	bool ___hasValue;
	float ___value;
};
struct Nullable_1_t70F850DEE49B62D1B877D3C32F9E0EC724ADC4D9 
{
	bool ___hasValue;
	uint16_t ___value;
};
struct Nullable_1_tD043F01310E483091D0E9A5526C3425F13EF2099 
{
	bool ___hasValue;
	uint32_t ___value;
};
struct Nullable_1_tF8BFF19FF240C9F0A45168187CD7106BAA146A99 
{
	bool ___hasValue;
	uint64_t ___value;
};
struct ReadOnly_t1D4689336F49F434532D72398BFBE7BF4D6059D4 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
};
struct ReadOnlyMemory_1_t63F301BF893B0AB689953D86A641168CA66D2399 
{
	RuntimeObject* ____object;
	int32_t ____index;
	int32_t ____length;
};
#ifndef ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_pinvoke_define
#define ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_pinvoke_define
struct ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_pinvoke
{
	Il2CppIUnknown* ____object;
	int32_t ____index;
	int32_t ____length;
};
#endif
#ifndef ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_com_define
#define ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_com_define
struct ReadOnlyMemory_1_t766DD3EE24B08138FB23CBC5B315D83C6E1272F5_marshaled_com
{
	Il2CppIUnknown* ____object;
	int32_t ____index;
	int32_t ____length;
};
#endif
struct ValueTask_1_t823DE87C36EA952D24C4E64F532E9D4425B72F21 
{
	RuntimeObject* ____obj;
	int32_t ____result;
	int16_t ____token;
	bool ____continueOnCapturedContext;
};
struct ValueTuple_2_tB358DB210B9947851BE1C2586AD7532BEB639942 
{
	int32_t ___Item1;
	bool ___Item2;
};
struct ValueTuple_2_tC57529B8C1EE84CA3D138FBE3836C013C6DC40AC 
{
	RuntimeObject* ___Item1;
	int32_t ___Item2;
};
struct ValueTuple_2_tC3717D4552EE1E5FC27BFBA3F5155741BC04557A 
{
	RuntimeObject* ___Item1;
	RuntimeObject* ___Item2;
};
struct Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___m_RenderTexture;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___m_VectorImage;
};
struct Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8_marshaled_pinvoke
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___m_RenderTexture;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___m_VectorImage;
};
struct Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8_marshaled_com
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___m_Texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* ___m_RenderTexture;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___m_VectorImage;
};
struct BatchPackedCullingViewID_t1E7EE8631C02555CAA181FA566CDC604B9FEFEBB 
{
	uint64_t ___handle;
};
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	uint8_t ___m_value;
};
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED 
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source;
};
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_pinvoke
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source;
};
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_marshaled_com
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ____source;
};
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	float ___r;
	float ___g;
	float ___b;
	float ___a;
};
struct Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___rgba;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			uint8_t ___r;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_OffsetPadding[1];
			uint8_t ___g;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_OffsetPadding[2];
			uint8_t ___b;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_OffsetPadding[3];
			uint8_t ___a;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_forAlignmentOnly;
		};
	};
};
struct CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 
{
	VisualElement_t2667F9D19E62C7A315927506C06F223AB9234115* ___U3CtargetU3Ek__BackingField;
	VisualTreeAsset_tFB5BF81F0780A412AE5A7C2C552B3EEA64EA2EEB* ___U3CvisualTreeAssetU3Ek__BackingField;
	Dictionary_2_t41165BF747F041590086BE39A59BE164430A3CEF* ___U3CslotInsertionPointsU3Ek__BackingField;
	List_1_t70EE7982F45810D4B024CF720D910E67974A3094* ___U3CattributeOverridesU3Ek__BackingField;
};
struct CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257_marshaled_pinvoke
{
	VisualElement_t2667F9D19E62C7A315927506C06F223AB9234115* ___U3CtargetU3Ek__BackingField;
	VisualTreeAsset_tFB5BF81F0780A412AE5A7C2C552B3EEA64EA2EEB* ___U3CvisualTreeAssetU3Ek__BackingField;
	Dictionary_2_t41165BF747F041590086BE39A59BE164430A3CEF* ___U3CslotInsertionPointsU3Ek__BackingField;
	List_1_t70EE7982F45810D4B024CF720D910E67974A3094* ___U3CattributeOverridesU3Ek__BackingField;
};
struct CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257_marshaled_com
{
	VisualElement_t2667F9D19E62C7A315927506C06F223AB9234115* ___U3CtargetU3Ek__BackingField;
	VisualTreeAsset_tFB5BF81F0780A412AE5A7C2C552B3EEA64EA2EEB* ___U3CvisualTreeAssetU3Ek__BackingField;
	Dictionary_2_t41165BF747F041590086BE39A59BE164430A3CEF* ___U3CslotInsertionPointsU3Ek__BackingField;
	List_1_t70EE7982F45810D4B024CF720D910E67974A3094* ___U3CattributeOverridesU3Ek__BackingField;
};
struct CullingGroupEvent_tC79BA328A8280C29F6002F591614081A0E87D110 
{
	int32_t ___m_Index;
	uint8_t ___m_PrevState;
	uint8_t ___m_ThisState;
};
struct CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F 
{
	Type_t* ___U3CArgumentTypeU3Ek__BackingField;
	RuntimeObject* ___U3CValueU3Ek__BackingField;
};
struct CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F_marshaled_pinvoke
{
	Type_t* ___U3CArgumentTypeU3Ek__BackingField;
	Il2CppIUnknown* ___U3CValueU3Ek__BackingField;
};
struct CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F_marshaled_com
{
	Type_t* ___U3CArgumentTypeU3Ek__BackingField;
	Il2CppIUnknown* ___U3CValueU3Ek__BackingField;
};
struct DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___P;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Q;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___G;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Y;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___J;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___X;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Seed;
	int32_t ___Counter;
};
struct DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9_marshaled_pinvoke
{
	Il2CppSafeArray* ___P;
	Il2CppSafeArray* ___Q;
	Il2CppSafeArray* ___G;
	Il2CppSafeArray* ___Y;
	Il2CppSafeArray* ___J;
	Il2CppSafeArray* ___X;
	Il2CppSafeArray* ___Seed;
	int32_t ___Counter;
};
struct DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9_marshaled_com
{
	Il2CppSafeArray* ___P;
	Il2CppSafeArray* ___Q;
	Il2CppSafeArray* ___G;
	Il2CppSafeArray* ___Y;
	Il2CppSafeArray* ___J;
	Il2CppSafeArray* ___X;
	Il2CppSafeArray* ___Seed;
	int32_t ___Counter;
};
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D 
{
	uint64_t ____dateData;
};
struct Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F 
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___flags;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___flags_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___hi_OffsetPadding[4];
			int32_t ___hi;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___hi_OffsetPadding_forAlignmentOnly[4];
			int32_t ___hi_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___lo_OffsetPadding[8];
			int32_t ___lo;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___lo_OffsetPadding_forAlignmentOnly[8];
			int32_t ___lo_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___mid_OffsetPadding[12];
			int32_t ___mid;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___mid_OffsetPadding_forAlignmentOnly[12];
			int32_t ___mid_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___ulomidLE_OffsetPadding[8];
			uint64_t ___ulomidLE;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___ulomidLE_OffsetPadding_forAlignmentOnly[8];
			uint64_t ___ulomidLE_forAlignmentOnly;
		};
	};
};
struct DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB 
{
	RuntimeObject* ____key;
	RuntimeObject* ____value;
};
struct DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB_marshaled_pinvoke
{
	Il2CppIUnknown* ____key;
	Il2CppIUnknown* ____value;
};
struct DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB_marshaled_com
{
	Il2CppIUnknown* ____key;
	Il2CppIUnknown* ____value;
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 
{
	bool ___U3CwantsMouseMoveU3Ek__BackingField;
	bool ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField;
	bool ___U3CwantsLessLayoutEventsU3Ek__BackingField;
};
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_pinvoke
{
	int32_t ___U3CwantsMouseMoveU3Ek__BackingField;
	int32_t ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField;
	int32_t ___U3CwantsLessLayoutEventsU3Ek__BackingField;
};
struct EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8_marshaled_com
{
	int32_t ___U3CwantsMouseMoveU3Ek__BackingField;
	int32_t ___U3CwantsMouseEnterLeaveWindowU3Ek__BackingField;
	int32_t ___U3CwantsLessLayoutEventsU3Ek__BackingField;
};
struct FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C 
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___m_FontAsset;
};
struct FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___m_FontAsset;
};
struct FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Font;
	FontAsset_t61A6446D934E582651044E33D250EA8D306AB958* ___m_FontAsset;
};
struct GrabPoseScore_tD56B46FDCBB34DFDDCDEF7D353038F5AA21A4A82 
{
	float ____translationScore;
	float ____rotationScore;
	float ____rotationWeight;
};
struct Guid_t 
{
	int32_t ____a;
	int16_t ____b;
	int16_t ____c;
	uint8_t ____d;
	uint8_t ____e;
	uint8_t ____f;
	uint8_t ____g;
	uint8_t ____h;
	uint8_t ____i;
	uint8_t ____j;
	uint8_t ____k;
};
struct Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 
{
	uint64_t ___u64_0;
	uint64_t ___u64_1;
};
struct HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 
{
	float ___r;
	float ___g;
	float ___b;
	float ___a;
};
struct HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 
{
	float ___m0;
	float ___m1;
	float ___m2;
	float ___m3;
	float ___m4;
	float ___m5;
	float ___m6;
	float ___m7;
	float ___m8;
	float ___m9;
	float ___m10;
	float ___m11;
};
struct HmdMatrix44_t_tF23EB340D2BFF58C56BDAB1354E8BBF7FCB16FF4 
{
	float ___m0;
	float ___m1;
	float ___m2;
	float ___m3;
	float ___m4;
	float ___m5;
	float ___m6;
	float ___m7;
	float ___m8;
	float ___m9;
	float ___m10;
	float ___m11;
	float ___m12;
	float ___m13;
	float ___m14;
	float ___m15;
};
struct HmdVector2_t_tCCEF5F67B49C6ABAC22E7757A470D9B127936833 
{
	float ___v0;
	float ___v1;
};
struct IntPtr_t 
{
	void* ___m_value;
};
struct JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 
{
	uint64_t ___jobGroup;
	int32_t ___version;
};
struct LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB 
{
	int32_t ___m_Mask;
};
struct LightStats_tBB72AF16728E19482A5C8A6B65A94F7FFB9DA80C 
{
	int32_t ___totalLights;
	int32_t ___totalNormalMapUsage;
	int32_t ___totalVolumetricUsage;
	uint32_t ___blendStylesUsed;
	uint32_t ___blendStylesWithLights;
};
struct LineInfo_t415DCF0EAD0FB3806F779BA170EC9058E47CCB24 
{
	int32_t ___lineNo;
	int32_t ___linePos;
};
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	float ___m00;
	float ___m10;
	float ___m20;
	float ___m30;
	float ___m01;
	float ___m11;
	float ___m21;
	float ___m31;
	float ___m02;
	float ___m12;
	float ___m22;
	float ___m32;
	float ___m03;
	float ___m13;
	float ___m23;
	float ___m33;
};
struct OVRSpaceUser_tF145982B655F69985F22D9AB527F17FC76CDC90F 
{
	uint64_t ____handle;
};
struct PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE 
{
	int32_t ___m_Handle;
};
struct PoseMeasureParameters_t93F958B174ABC0A954B8A6B9B3DFA73DFE894363 
{
	float ____positionRotationWeight;
};
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF 
{
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Exponent;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Modulus;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___P;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___Q;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___DP;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___DQ;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___InverseQ;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___D;
};
struct RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF_marshaled_pinvoke
{
	Il2CppSafeArray* ___Exponent;
	Il2CppSafeArray* ___Modulus;
	Il2CppSafeArray* ___P;
	Il2CppSafeArray* ___Q;
	Il2CppSafeArray* ___DP;
	Il2CppSafeArray* ___DQ;
	Il2CppSafeArray* ___InverseQ;
	Il2CppSafeArray* ___D;
};
struct RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF_marshaled_com
{
	Il2CppSafeArray* ___Exponent;
	Il2CppSafeArray* ___Modulus;
	Il2CppSafeArray* ___P;
	Il2CppSafeArray* ___Q;
	Il2CppSafeArray* ___DP;
	Il2CppSafeArray* ___DQ;
	Il2CppSafeArray* ___InverseQ;
	Il2CppSafeArray* ___D;
};
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	float ___m_XMin;
	float ___m_YMin;
	float ___m_Width;
	float ___m_Height;
};
struct RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 
{
	int32_t ___m_XMin;
	int32_t ___m_YMin;
	int32_t ___m_Width;
	int32_t ___m_Height;
};
struct RuleMatcher_t327CFEB02C81AA20E639DE949DCBBAB5E92FF28E 
{
	StyleSheet_t6FAF43FCDB45BC6BED0522A222FD4C1A9BB10428* ___sheet;
	StyleComplexSelector_tE46C29F65FDBA48D3152781187401C8B55B7D8AD* ___complexSelector;
};
struct RuleMatcher_t327CFEB02C81AA20E639DE949DCBBAB5E92FF28E_marshaled_pinvoke
{
	StyleSheet_t6FAF43FCDB45BC6BED0522A222FD4C1A9BB10428* ___sheet;
	StyleComplexSelector_tE46C29F65FDBA48D3152781187401C8B55B7D8AD* ___complexSelector;
};
struct RuleMatcher_t327CFEB02C81AA20E639DE949DCBBAB5E92FF28E_marshaled_com
{
	StyleSheet_t6FAF43FCDB45BC6BED0522A222FD4C1A9BB10428* ___sheet;
	StyleComplexSelector_tE46C29F65FDBA48D3152781187401C8B55B7D8AD* ___complexSelector;
};
struct ShaderTagId_t453E2085B5EE9448FF75E550CAB111EFF690ECB0 
{
	int32_t ___m_Id;
};
struct StyleValues_t4AED947A53B84B62EF2B589A40B74911CA77D11A 
{
	StyleValueCollection_t5ADC08D23E648FBE78F2C161494786E6C83E1377* ___m_StyleValues;
};
struct StyleValues_t4AED947A53B84B62EF2B589A40B74911CA77D11A_marshaled_pinvoke
{
	StyleValueCollection_t5ADC08D23E648FBE78F2C161494786E6C83E1377* ___m_StyleValues;
};
struct StyleValues_t4AED947A53B84B62EF2B589A40B74911CA77D11A_marshaled_com
{
	StyleValueCollection_t5ADC08D23E648FBE78F2C161494786E6C83E1377* ___m_StyleValues;
};
struct TextureId_tFF4B4AAE53408AB10B0B89CCA5F7B50CF2535E58 
{
	int32_t ___m_Index;
};
struct TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A 
{
	int64_t ____ticks;
};
struct TimerState_t82C7C29B095D6ACDC06AC172C269E9D5F0508ECE 
{
	int64_t ___U3CstartU3Ek__BackingField;
	int64_t ___U3CnowU3Ek__BackingField;
};
struct UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC 
{
	int32_t ___startCharIdx;
	int32_t ___height;
	float ___topY;
	float ___leading;
};
struct ValueTask_t10B4B5DDF5C582607D0E634FA912F8CB94FCD49F 
{
	RuntimeObject* ____obj;
	int16_t ____token;
	bool ____continueOnCapturedContext;
};
struct Vec3_t7EDAE0ABBD2BC4C43A47B8BEF6C079AB55FB0CBB 
{
	float ___X;
	float ___Y;
	float ___Z;
};
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	float ___x;
	float ___y;
};
struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A 
{
	int32_t ___m_X;
	int32_t ___m_Y;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	float ___x;
	float ___y;
	float ___z;
};
struct Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 
{
	int32_t ___m_X;
	int32_t ___m_Y;
	int32_t ___m_Z;
};
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB 
{
	float ___width;
	float ___height;
};
struct BodyPoseComparerFeatureState_t2521896B82984592C7784DA0BE6D7EB58B55829B 
{
	float ___Delta;
	float ___MaxDelta;
};
struct HapticsAmplitudeEnvelopeVibration_t2EDF01B96CFAC2D09DDCAACEDEA5C2F534F252C9 
{
	int32_t ___SamplesCount;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___Samples;
	float ___Duration;
};
struct HapticsAmplitudeEnvelopeVibration_t2EDF01B96CFAC2D09DDCAACEDEA5C2F534F252C9_marshaled_pinvoke
{
	int32_t ___SamplesCount;
	Il2CppSafeArray* ___Samples;
	float ___Duration;
};
struct HapticsAmplitudeEnvelopeVibration_t2EDF01B96CFAC2D09DDCAACEDEA5C2F534F252C9_marshaled_com
{
	int32_t ___SamplesCount;
	Il2CppSafeArray* ___Samples;
	float ___Duration;
};
struct HapticsPcmVibration_tA67530C77AF893E2DAE4AC825EB6477B76251781 
{
	int32_t ___SamplesCount;
	SingleU5BU5D_t89DEFE97BCEDB5857010E79ECE0F52CF6E93B87C* ___Samples;
	float ___SampleRateHz;
	bool ___Append;
};
struct HapticsPcmVibration_tA67530C77AF893E2DAE4AC825EB6477B76251781_marshaled_pinvoke
{
	int32_t ___SamplesCount;
	Il2CppSafeArray* ___Samples;
	float ___SampleRateHz;
	int32_t ___Append;
};
struct HapticsPcmVibration_tA67530C77AF893E2DAE4AC825EB6477B76251781_marshaled_com
{
	int32_t ___SamplesCount;
	Il2CppSafeArray* ___Samples;
	float ___SampleRateHz;
	int32_t ___Append;
};
struct MeshRendererData_t82E0EE9E624AA3082384E4FDC913E2640811BB6D 
{
	bool ___U3CIsDataValidU3Ek__BackingField;
	bool ___U3CIsDataHighConfidenceU3Ek__BackingField;
	bool ___U3CShouldUseSystemGestureMaterialU3Ek__BackingField;
};
struct MeshRendererData_t82E0EE9E624AA3082384E4FDC913E2640811BB6D_marshaled_pinvoke
{
	int32_t ___U3CIsDataValidU3Ek__BackingField;
	int32_t ___U3CIsDataHighConfidenceU3Ek__BackingField;
	int32_t ___U3CShouldUseSystemGestureMaterialU3Ek__BackingField;
};
struct MeshRendererData_t82E0EE9E624AA3082384E4FDC913E2640811BB6D_marshaled_com
{
	int32_t ___U3CIsDataValidU3Ek__BackingField;
	int32_t ___U3CIsDataHighConfidenceU3Ek__BackingField;
	int32_t ___U3CShouldUseSystemGestureMaterialU3Ek__BackingField;
};
struct Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct Vector3f_t232AF83B4642C67BE8EFF85D8E1599D3B06BD562 
{
	float ___x;
	float ___y;
	float ___z;
};
struct SkeletonRendererData_t2987D9652B016DADE9C92F3342186D5E620C0672 
{
	float ___U3CRootScaleU3Ek__BackingField;
	bool ___U3CIsDataValidU3Ek__BackingField;
	bool ___U3CIsDataHighConfidenceU3Ek__BackingField;
	bool ___U3CShouldUseSystemGestureMaterialU3Ek__BackingField;
};
struct SkeletonRendererData_t2987D9652B016DADE9C92F3342186D5E620C0672_marshaled_pinvoke
{
	float ___U3CRootScaleU3Ek__BackingField;
	int32_t ___U3CIsDataValidU3Ek__BackingField;
	int32_t ___U3CIsDataHighConfidenceU3Ek__BackingField;
	int32_t ___U3CShouldUseSystemGestureMaterialU3Ek__BackingField;
};
struct SkeletonRendererData_t2987D9652B016DADE9C92F3342186D5E620C0672_marshaled_com
{
	float ___U3CRootScaleU3Ek__BackingField;
	int32_t ___U3CIsDataValidU3Ek__BackingField;
	int32_t ___U3CIsDataHighConfidenceU3Ek__BackingField;
	int32_t ___U3CShouldUseSystemGestureMaterialU3Ek__BackingField;
};
struct unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 
{
	uint64_t ___handle;
};
struct unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 
{
	unitytls_tlsctx_read_callback_tDBE877327789CABE940C2A724EC9A5D142318851* ___read;
	unitytls_tlsctx_write_callback_t5D4B64AD846D04E819A49689F7EAA47365636611* ___write;
	void* ___data;
};
struct unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568_marshaled_pinvoke
{
	Il2CppMethodPointer ___read;
	Il2CppMethodPointer ___write;
	void* ___data;
};
struct unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568_marshaled_com
{
	Il2CppMethodPointer ___read;
	Il2CppMethodPointer ___write;
	void* ___data;
};
struct unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 
{
	uint64_t ___handle;
};
struct unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 
{
	uint64_t ___handle;
};
struct ByReference_1_t9C85BCCAAF8C525B6C06B07E922D8D217BE8D6FC 
{
	intptr_t ____value;
};
struct ByReference_1_t7BA5A6CA164F770BC688F21C5978D368716465F5 
{
	intptr_t ____value;
};
struct ByReference_1_t98B79BFB40A2CA0814BC183B09B4339A5EBF8524 
{
	intptr_t ____value;
};
struct Enumerator_t0F33943C6A5996966DA110C79285AF0AE2479E2D 
{
	Dictionary_2_tB18B7D2DF248968973DD36692A4049966CA9F348* ____dictionary;
	int32_t ____version;
	int32_t ____index;
	KeyValuePair_2_tC86602D5307A611B8935C881185C39812E7D59DF ____current;
	int32_t ____getEnumeratorRetType;
};
struct KeyValuePair_2_t472CF116DEB4A9261996D42F3FB67E7595E0EC3F 
{
	int32_t ___key;
	ValueTuple_2_tC57529B8C1EE84CA3D138FBE3836C013C6DC40AC ___value;
};
struct KeyValuePair_2_t668E6602CE2430EE46E4DEBC02427E14F51F6138 
{
	RuntimeObject* ___key;
	BodyPoseComparerFeatureState_t2521896B82984592C7784DA0BE6D7EB58B55829B ___value;
};
struct Nullable_1_tEADC262F7F8B8BC4CC0A003DBDD3CA7C1B63F9AC 
{
	bool ___hasValue;
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___value;
};
struct Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB 
{
	bool ___hasValue;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___value;
};
struct Nullable_1_t0ECB838EB0C9A81655750B26970F21CF9A83A5F7 
{
	bool ___hasValue;
	Guid_t ___value;
};
struct Nullable_1_tE151CE1F6892804B41C4004C95CB57020ABB3272 
{
	bool ___hasValue;
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___value;
};
struct ValueTuple_2_t4BC958336F5E31F49459112BAB22FC776AFF71C3 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___Item1;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___Item2;
};
struct Allocator_t996642592271AAD9EE688F142741D512C07B5824 
{
	int32_t ___value__;
};
struct BackgroundPositionKeyword_tE680A05B983D256AADC8E2CF1CA169D004B8641B 
{
	int32_t ___value__;
};
struct BackgroundSizeType_tD194B20FF5086D494ABF8D799124D2FC4FFCC674 
{
	int32_t ___value__;
};
struct BatchCullingFlags_tEECC03D99C74958416446D306EA944DBA1C8E3A2 
{
	int32_t ___value__;
};
struct BatchCullingProjectionType_tAD14BC373E72D5F74188E0899F8670FAB51FD481 
{
	int32_t ___value__;
};
struct BatchCullingViewType_tAC2682BF9A489DF44A8960693303B47248C252CF 
{
	int32_t ___value__;
};
struct Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Center;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Extents;
};
struct BoundsInt_t4E757DE5EFF9FCB42000F173360DDC63B5585485 
{
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___m_Position;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___m_Size;
};
struct ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0 
{
	bool ___isValid;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___pageAndID;
};
struct ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0_marshaled_pinvoke
{
	int32_t ___isValid;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___pageAndID;
};
struct ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0_marshaled_com
{
	int32_t ___isValid;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___pageAndID;
};
struct ConsoleKey_t90D518E97CE61D975E487810228A92BDD34D73BF 
{
	int32_t ___value__;
};
struct ConsoleModifiers_tAC0C5448495DEE5829C85FC9D3BE1C0B2A748967 
{
	int32_t ___value__;
};
struct Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82 
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___U3CtextureU3Ek__BackingField;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3ChotspotU3Ek__BackingField;
	int32_t ___U3CdefaultCursorIdU3Ek__BackingField;
};
struct Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82_marshaled_pinvoke
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___U3CtextureU3Ek__BackingField;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3ChotspotU3Ek__BackingField;
	int32_t ___U3CdefaultCursorIdU3Ek__BackingField;
};
struct Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82_marshaled_com
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___U3CtextureU3Ek__BackingField;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___U3ChotspotU3Ek__BackingField;
	int32_t ___U3CdefaultCursorIdU3Ek__BackingField;
};
struct CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02 
{
	CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F ___U3CTypedValueU3Ek__BackingField;
	bool ___U3CIsFieldU3Ek__BackingField;
	String_t* ___U3CMemberNameU3Ek__BackingField;
	Type_t* ____attributeType;
	MemberInfo_t* ____lazyMemberInfo;
};
struct CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02_marshaled_pinvoke
{
	CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F_marshaled_pinvoke ___U3CTypedValueU3Ek__BackingField;
	int32_t ___U3CIsFieldU3Ek__BackingField;
	char* ___U3CMemberNameU3Ek__BackingField;
	Type_t* ____attributeType;
	MemberInfo_t* ____lazyMemberInfo;
};
struct CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02_marshaled_com
{
	CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F_marshaled_com ___U3CTypedValueU3Ek__BackingField;
	int32_t ___U3CIsFieldU3Ek__BackingField;
	Il2CppChar* ___U3CMemberNameU3Ek__BackingField;
	Type_t* ____attributeType;
	MemberInfo_t* ____lazyMemberInfo;
};
struct DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 
{
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ____dateTime;
	int16_t ____offsetMinutes;
};
struct DragVisualMode_tC1D89AA62CEA10935372D06EB6DAEB8739356D16 
{
	int32_t ___value__;
};
struct FingerRequirement_t081327CFD0BE45214EF3E39CA6947AF50546C196 
{
	int32_t ___value__;
};
struct FingerUnselectMode_t9B7AAA7E5EC5D96A2EEE182A1B04328D08F38155 
{
	int32_t ___value__;
};
struct GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC 
{
	intptr_t ___handle;
};
struct Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E  : public RuntimeObject
{
	intptr_t ___m_Ptr;
};
struct Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E_marshaled_pinvoke
{
	intptr_t ___m_Ptr;
};
struct Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E_marshaled_com
{
	intptr_t ___m_Ptr;
};
struct GraphicsFormat_tC3D1898F3F3F1F57256C7F3FFD6BA9A37AE7E713 
{
	int32_t ___value__;
};
struct HiddenAreaMesh_t_t94244AEA54FC806F524A9FB826CBB0339D3FC8E4 
{
	intptr_t ___pVertexData;
	uint32_t ___unTriangleCount;
};
struct HmdRect2_t_tAF394D41DC1EEC399E9D2B45C173C3504AA23C74 
{
	HmdVector2_t_tCCEF5F67B49C6ABAC22E7757A470D9B127936833 ___vTopLeft;
	HmdVector2_t_tCCEF5F67B49C6ABAC22E7757A470D9B127936833 ___vBottomRight;
};
struct Int32Enum_tCBAC8BA2BFF3A845FA599F303093BBBA374B6F0C 
{
	int32_t ___value__;
};
struct LODParameters_t54D2AA0FD8E53BCF51D7A42BC1A72FCA8C78A08A 
{
	int32_t ___m_IsOrthographic;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_CameraPosition;
	float ___m_FieldOfView;
	float ___m_OrthoSize;
	int32_t ___m_CameraPixelHeight;
};
struct LoadSceneMode_t3E17ADA25A3C4F14ECF6026741219437DA054963 
{
	int32_t ___value__;
};
struct LocalPhysicsMode_tFCD6A7FC347C4D1F90CC3CE7B23D9A8509E1F6DF 
{
	int32_t ___value__;
};
struct OVRAnchor_tC6603E0C1628ACAA50D8CCDCC267BFD246F5A061 
{
	uint64_t ___U3CHandleU3Ek__BackingField;
	Guid_t ___U3CUuidU3Ek__BackingField;
};
struct PenEventType_t74D209AD8F86E35F8B5B6030115FC21FE9A322F5 
{
	int32_t ___value__;
};
struct PenStatus_tCAD6543115EF443E17410B52D37EC67BCC88ABB8 
{
	int32_t ___value__;
};
struct PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E 
{
	intptr_t ___m_Handle;
	uint32_t ___m_Version;
};
struct PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 
{
	intptr_t ___m_Handle;
	uint32_t ___m_Version;
};
struct PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 
{
	intptr_t ___m_Handle;
	uint32_t ___m_Version;
};
struct PointerEventType_tF7192FE21454935EF9D0D7BEF88C70CF05D90359 
{
	int32_t ___value__;
};
struct Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position;
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation;
};
struct ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD 
{
	intptr_t ___m_Ptr;
};
struct Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Origin;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Direction;
};
struct RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Centroid;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Point;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Normal;
	float ___m_Distance;
	float ___m_Fraction;
	int32_t ___m_Collider;
};
struct ReleaseVelocityInformation_t70EFA79B39A6D6D6B6C771E9E043F61F13B0287D 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___LinearVelocity;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___AngularVelocity;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___Origin;
	bool ___IsSelectedVelocity;
};
struct ReleaseVelocityInformation_t70EFA79B39A6D6D6B6C771E9E043F61F13B0287D_marshaled_pinvoke
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___LinearVelocity;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___AngularVelocity;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___Origin;
	int32_t ___IsSelectedVelocity;
};
struct ReleaseVelocityInformation_t70EFA79B39A6D6D6B6C771E9E043F61F13B0287D_marshaled_com
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___LinearVelocity;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___AngularVelocity;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___Origin;
	int32_t ___IsSelectedVelocity;
};
struct RenderTextureCreationFlags_t1C01993691E5BA956575134696509089FE852F50 
{
	int32_t ___value__;
};
struct RenderTextureMemoryless_tE3B7F3AE353C3E9ACF86076376EB862131D19A69 
{
	int32_t ___value__;
};
struct Repeat_tC0330B75B12D24B063BA5151AF3BB73B85D8B840 
{
	int32_t ___value__;
};
struct RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 
{
	intptr_t ___value;
};
struct RuntimeMethodHandle_tB35B96E97214DCBE20B0B02B1E687884B34680B2 
{
	intptr_t ___value;
};
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	intptr_t ___value;
};
struct Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Scale;
	bool ___m_IsNone;
};
struct Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7_marshaled_pinvoke
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Scale;
	int32_t ___m_IsNone;
};
struct Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7_marshaled_com
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Scale;
	int32_t ___m_IsNone;
};
struct ScaleMode_t16AD656758EE54C56B3DA34FE4F2033C9C2EE13D 
{
	int32_t ___value__;
};
struct ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 
{
	intptr_t ___m_Ptr;
};
struct ShadowSamplingMode_t8BE740C4258CFEDDBAC01FDC0438D8EE3F776BA8 
{
	int32_t ___value__;
};
struct StreamingContextStates_t5EE358E619B251608A9327618C7BFE8638FC33C1 
{
	int32_t ___value__;
};
struct StyleKeyword_t2812E72266C15CBA8927586972DC2FD27B10E705 
{
	int32_t ___value__;
};
struct StyleValueType_tC3253FE046DBB95224A74D13B534D015CC4AADDE 
{
	int32_t ___value__;
};
struct TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___offset;
	float ___blurRadius;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color;
};
struct TextureDimension_t8D7148B9168256EE1E9AF91378ABA148888CE642 
{
	int32_t ___value__;
};
struct TouchPhase_t54E0A1AF80465997849420A72317B733E1D49A9E 
{
	int32_t ___value__;
};
struct TouchType_t84F82C73BC1A6012141735AD84DA67AA7F7AB43F 
{
	int32_t ___value__;
};
struct UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___cursorPos;
	float ___charWidth;
};
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___position;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___normal;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___tangent;
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___color;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv0;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv1;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv2;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___uv3;
};
struct VRTextureUsage_t57FAA0077810142A461D74EDC5E33FC3D78BD2E8 
{
	int32_t ___value__;
};
struct Unit_t21DCD5C095F7DC1A0B9A47CAF8CAD3E7776CD3DB 
{
	int32_t ___value__;
};
struct Unit_t7A9C3ABB0618BEBFDC1813D07080CE0C145448ED 
{
	int32_t ___value__;
};
struct RotationType_tD58F7B6EB01892B0375178D9775F6185D4E2DC0F 
{
	int32_t ___value__;
};
struct TranslationType_t97D4BABD1EDD1C5D0030103150399BD6255EF5CB 
{
	int32_t ___value__;
};
struct MeshFlags_tC36F9F848F3DB16DF11E55B5CD6EC22E860876A3 
{
	int32_t ___value__;
};
struct BoneId_t4D3A8C103467359765D1206D493D560855F8FFAF 
{
	int32_t ___value__;
};
struct EventType_t51A504495C404B904AFBCF27C6268D87802C12E1 
{
	int32_t ___value__;
};
struct Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 
{
	Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A ___Orientation;
	Vector3f_t232AF83B4642C67BE8EFF85D8E1599D3B06BD562 ___Position;
};
struct BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357 
{
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___src;
	RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 ___srcRect;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___dstPos;
	int32_t ___border;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___tint;
};
struct BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357_marshaled_pinvoke
{
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___src;
	RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 ___srcRect;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___dstPos;
	int32_t ___border;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___tint;
};
struct BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357_marshaled_com
{
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___src;
	RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 ___srcRect;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___dstPos;
	int32_t ___border;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___tint;
};
struct unitytls_error_code_tFDFCC5EE8771438D311CAB28FC8D37025E5BD6BE 
{
	uint32_t ___value__;
};
struct unitytls_protocol_t08A2DE5BF736B214E28F8D60B84B2648DE327FD7 
{
	uint32_t ___value__;
};
struct Type_t3DBB943D82B910088B819217262B8091F1E9C18C 
{
	int32_t ___value__;
};
struct Enumerator_t5438B9989E702349A6790B901A8E6B408ED3B3CD 
{
	HashSet_1_t2EC13BE6E93BB0C99D5CF97A25799B40FD6CBAF4* ____set;
	int32_t ____index;
	int32_t ____version;
	int32_t ____current;
};
struct NativeArray_1_tBEE3484B4ABC271CFAB65039F1439061D5DF806A 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct NativeArray_1_t73992261AA60020B6BE20D83C50B3F925CC89F31 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct NativeArray_1_t4020B6981295FB915DCE82EF368535F680C13A49 
{
	void* ___m_Buffer;
	int32_t ___m_Length;
	int32_t ___m_AllocatorLabel;
};
struct Nullable_1_t5127ABE6809BA32727C69CB2E076B28D676EB15B 
{
	bool ___hasValue;
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 ___value;
};
struct ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D 
{
	ByReference_1_t9C85BCCAAF8C525B6C06B07E922D8D217BE8D6FC ____pointer;
	int32_t ____length;
};
struct ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 
{
	ByReference_1_t7BA5A6CA164F770BC688F21C5978D368716465F5 ____pointer;
	int32_t ____length;
};
struct Span_1_tDADAC65069DFE6B57C458109115ECD795ED39305 
{
	ByReference_1_t9C85BCCAAF8C525B6C06B07E922D8D217BE8D6FC ____pointer;
	int32_t ____length;
};
struct Span_1_tEDDF15FCF9EC6DEBA0F696BAACDDBAB9D92C252D 
{
	ByReference_1_t7BA5A6CA164F770BC688F21C5978D368716465F5 ____pointer;
	int32_t ____length;
};
struct Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 
{
	ByReference_1_t98B79BFB40A2CA0814BC183B09B4339A5EBF8524 ____pointer;
	int32_t ____length;
};
struct StyleEnum_1_t3DD2EBD4E359AFE77C2974ECAA1DEE50E0FACEDC 
{
	int32_t ___m_Value;
	int32_t ___m_Keyword;
};
struct ValueTuple_2_tBC19AE73793D615D180F320AB46A541EF61AFBF9 
{
	int32_t ___Item1;
	RuntimeObject* ___Item2;
};
struct Angle_t0229F612898D65B3CC646C40A32D93D8A33C1DFC 
{
	float ___m_Value;
	int32_t ___m_Unit;
};
struct BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F 
{
	int32_t ___x;
	int32_t ___y;
};
struct ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900 
{
	Il2CppChar ____keyChar;
	int32_t ____key;
	int32_t ____mods;
};
struct ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900_marshaled_pinvoke
{
	uint8_t ____keyChar;
	int32_t ____key;
	int32_t ____mods;
};
struct ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900_marshaled_com
{
	uint8_t ____keyChar;
	int32_t ____key;
	int32_t ____mods;
};
struct GrabbingRule_tBFBDE400621FCCCEB23EF9A44E42B11AE7DBF27D 
{
	int32_t ____thumbRequirement;
	int32_t ____indexRequirement;
	int32_t ____middleRequirement;
	int32_t ____ringRequirement;
	int32_t ____pinkyRequirement;
	int32_t ____unselectMode;
};
struct Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 
{
	float ___m_Value;
	int32_t ___m_Unit;
};
struct LoadSceneParameters_tFBAFEA7FA75F282D3034241AD8756A7B5578310E 
{
	int32_t ___m_LoadSceneMode;
	int32_t ___m_LocalPhysicsMode;
};
struct LocomotionEvent_tECDD51A234545203F40F068C91D2745873285642 
{
	int32_t ___U3CIdentifierU3Ek__BackingField;
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 ___U3CPoseU3Ek__BackingField;
	int32_t ___U3CTranslationU3Ek__BackingField;
	int32_t ___U3CRotationU3Ek__BackingField;
};
struct MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD 
{
	void* ____pointer;
	GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC ____handle;
	RuntimeObject* ____pinnable;
};
struct MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD_marshaled_pinvoke
{
	void* ____pointer;
	GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC ____handle;
	RuntimeObject* ____pinnable;
};
struct MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD_marshaled_com
{
	void* ____pointer;
	GCHandle_tC44F6F72EE68BD4CFABA24309DA7A179D41127DC ____handle;
	RuntimeObject* ____pinnable;
};
struct PenData_t2345B5FBD18D851528C5C18F8A667D4EF4690945 
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___position;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___tilt;
	int32_t ___penStatus;
	float ___twist;
	float ___pressure;
	int32_t ___contactType;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___deltaPos;
};
struct Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F 
{
	PlayableHandle_t5D6A01EF94382EFEDC047202F71DF882769654D4 ___m_Handle;
};
struct PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 
{
	PlayableOutputHandle_tEB217645A8C0356A3AC6F964F283003B9740E883 ___m_Handle;
};
struct PointerEvent_tAEB047AC9AE96DA96400B3C6FA88E56C917608BC 
{
	int32_t ___U3CIdentifierU3Ek__BackingField;
	int32_t ___U3CTypeU3Ek__BackingField;
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 ___U3CPoseU3Ek__BackingField;
	RuntimeObject* ___U3CDataU3Ek__BackingField;
};
struct PointerEvent_tAEB047AC9AE96DA96400B3C6FA88E56C917608BC_marshaled_pinvoke
{
	int32_t ___U3CIdentifierU3Ek__BackingField;
	int32_t ___U3CTypeU3Ek__BackingField;
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 ___U3CPoseU3Ek__BackingField;
	Il2CppIUnknown* ___U3CDataU3Ek__BackingField;
};
struct PointerEvent_tAEB047AC9AE96DA96400B3C6FA88E56C917608BC_marshaled_com
{
	int32_t ___U3CIdentifierU3Ek__BackingField;
	int32_t ___U3CTypeU3Ek__BackingField;
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 ___U3CPoseU3Ek__BackingField;
	Il2CppIUnknown* ___U3CDataU3Ek__BackingField;
};
struct RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 
{
	int32_t ___U3CwidthU3Ek__BackingField;
	int32_t ___U3CheightU3Ek__BackingField;
	int32_t ___U3CmsaaSamplesU3Ek__BackingField;
	int32_t ___U3CvolumeDepthU3Ek__BackingField;
	int32_t ___U3CmipCountU3Ek__BackingField;
	int32_t ____graphicsFormat;
	int32_t ___U3CstencilFormatU3Ek__BackingField;
	int32_t ___U3CdepthStencilFormatU3Ek__BackingField;
	int32_t ___U3CdimensionU3Ek__BackingField;
	int32_t ___U3CshadowSamplingModeU3Ek__BackingField;
	int32_t ___U3CvrUsageU3Ek__BackingField;
	int32_t ____flags;
	int32_t ___U3CmemorylessU3Ek__BackingField;
};
struct StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 
{
	String_t* ___U3CtitleU3Ek__BackingField;
	int32_t ___U3CvisualModeU3Ek__BackingField;
	Hashtable_tEFC3B6496E6747787D8BB761B51F2AE3A8CFFE2D* ___U3CgenericDataU3Ek__BackingField;
	RuntimeObject* ___U3CunityObjectReferencesU3Ek__BackingField;
};
struct StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9_marshaled_pinvoke
{
	char* ___U3CtitleU3Ek__BackingField;
	int32_t ___U3CvisualModeU3Ek__BackingField;
	Hashtable_tEFC3B6496E6747787D8BB761B51F2AE3A8CFFE2D* ___U3CgenericDataU3Ek__BackingField;
	RuntimeObject* ___U3CunityObjectReferencesU3Ek__BackingField;
};
struct StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9_marshaled_com
{
	Il2CppChar* ___U3CtitleU3Ek__BackingField;
	int32_t ___U3CvisualModeU3Ek__BackingField;
	Hashtable_tEFC3B6496E6747787D8BB761B51F2AE3A8CFFE2D* ___U3CgenericDataU3Ek__BackingField;
	RuntimeObject* ___U3CunityObjectReferencesU3Ek__BackingField;
};
struct StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 
{
	RuntimeObject* ___m_additionalContext;
	int32_t ___m_state;
};
struct StreamingContext_t56760522A751890146EE45F82F866B55B7E33677_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext;
	int32_t ___m_state;
};
struct StreamingContext_t56760522A751890146EE45F82F866B55B7E33677_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext;
	int32_t ___m_state;
};
struct StyleColor_tFC32BA34A15742AC48D6AACF8A137A6F71F04910 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610 
{
	Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610_marshaled_pinvoke
{
	Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82_marshaled_pinvoke ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610_marshaled_com
{
	Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82_marshaled_com ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFloat_t4A100BCCDC275C2302517C5858C9BE9EC43D4841 
{
	float ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFont_t9D8A6F3E224B60FD8BA1522CE8AB0E2E8BE8B77C 
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFont_t9D8A6F3E224B60FD8BA1522CE8AB0E2E8BE8B77C_marshaled_pinvoke
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFont_t9D8A6F3E224B60FD8BA1522CE8AB0E2E8BE8B77C_marshaled_com
{
	Font_tC95270EA3198038970422D78B74A7F2E218A96B6* ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleFontDefinition_t0E1130277B322724A677D489018D219F014070F4 
{
	int32_t ___m_Keyword;
	FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C ___m_Value;
};
struct StyleFontDefinition_t0E1130277B322724A677D489018D219F014070F4_marshaled_pinvoke
{
	int32_t ___m_Keyword;
	FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C_marshaled_pinvoke ___m_Value;
};
struct StyleFontDefinition_t0E1130277B322724A677D489018D219F014070F4_marshaled_com
{
	int32_t ___m_Keyword;
	FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C_marshaled_com ___m_Value;
};
struct StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC 
{
	Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC_marshaled_pinvoke
{
	Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7_marshaled_pinvoke ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC_marshaled_com
{
	Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7_marshaled_com ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTextShadow_tCDDF1FE733ADBAA5ACA3B74620D4728E83F54252 
{
	int32_t ___m_Keyword;
	TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 ___m_Value;
};
struct StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D 
{
	int32_t ___m_ValueType;
	int32_t ___valueIndex;
};
struct Touch_t03E51455ED508492B3F278903A0114FA0E87B417 
{
	int32_t ___m_FingerId;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_Position;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_RawPosition;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___m_PositionDelta;
	float ___m_TimeDelta;
	int32_t ___m_TapCount;
	int32_t ___m_Phase;
	int32_t ___m_Type;
	float ___m_Pressure;
	float ___m_maximumPossiblePressure;
	float ___m_Radius;
	float ___m_RadiusVariance;
	float ___m_AltitudeAngle;
	float ___m_AzimuthAngle;
};
struct TypedReference_tF20A82297BED597FD80BDA0E41F74746B0FD642B 
{
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___type;
	intptr_t ___Value;
	intptr_t ___Type;
};
struct Enumerator_t096B8EE1BCCF3F5C13983F76868C274C3FA83D63 
{
	int32_t ___type;
	Enumerator_t0F33943C6A5996966DA110C79285AF0AE2479E2D ___m_Object;
	Enumerator_t0C52A6D0DB109DF239C883D1C65BF615A5F42109 ___m_Array;
};
struct Enumerator_t096B8EE1BCCF3F5C13983F76868C274C3FA83D63_marshaled_pinvoke
{
	int32_t ___type;
	Enumerator_t0F33943C6A5996966DA110C79285AF0AE2479E2D ___m_Object;
	Enumerator_t0C52A6D0DB109DF239C883D1C65BF615A5F42109 ___m_Array;
};
struct Enumerator_t096B8EE1BCCF3F5C13983F76868C274C3FA83D63_marshaled_com
{
	int32_t ___type;
	Enumerator_t0F33943C6A5996966DA110C79285AF0AE2479E2D ___m_Object;
	Enumerator_t0C52A6D0DB109DF239C883D1C65BF615A5F42109 ___m_Array;
};
struct Settings_tAA0A53A14DF6D83119CDFFF37A34FED8AA055A7A 
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___colorLutTargetTexture;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___colorLutSourceTexture;
	float ___saturation;
	float ___posterize;
	float ___brightness;
	float ___contrast;
	Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E* ___gradient;
	float ___lutWeight;
	bool ___flipLutY;
};
struct Settings_tAA0A53A14DF6D83119CDFFF37A34FED8AA055A7A_marshaled_pinvoke
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___colorLutTargetTexture;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___colorLutSourceTexture;
	float ___saturation;
	float ___posterize;
	float ___brightness;
	float ___contrast;
	Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E_marshaled_pinvoke ___gradient;
	float ___lutWeight;
	int32_t ___flipLutY;
};
struct Settings_tAA0A53A14DF6D83119CDFFF37A34FED8AA055A7A_marshaled_com
{
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___colorLutTargetTexture;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___colorLutSourceTexture;
	float ___saturation;
	float ___posterize;
	float ___brightness;
	float ___contrast;
	Gradient_tA7FEBE2FDB4929FFF6C997134841046F713DAC1E_marshaled_com* ___gradient;
	float ___lutWeight;
	int32_t ___flipLutY;
};
struct Bone_tC5A0FC2629B8200D2FAA3C6E4DA8AF048617E43D 
{
	int32_t ___Id;
	int16_t ___ParentBoneIndex;
	Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 ___Pose;
};
struct EventDataBuffer_t5836E8ECE1E094863DEDCC92818AEF39C2F646E8 
{
	int32_t ___EventType;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___EventData;
};
struct EventDataBuffer_t5836E8ECE1E094863DEDCC92818AEF39C2F646E8_marshaled_pinvoke
{
	int32_t ___EventType;
	uint8_t ___EventData[4000];
};
struct EventDataBuffer_t5836E8ECE1E094863DEDCC92818AEF39C2F646E8_marshaled_com
{
	int32_t ___EventType;
	uint8_t ___EventData[4000];
};
struct SkeletonPoseData_tA0A3A4163EEA849A40C82CF95F1847A9827A5F86 
{
	Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 ___U3CRootPoseU3Ek__BackingField;
	float ___U3CRootScaleU3Ek__BackingField;
	QuatfU5BU5D_t866C516DA0FC85581934D10E587D323B1B89E3BF* ___U3CBoneRotationsU3Ek__BackingField;
	bool ___U3CIsDataValidU3Ek__BackingField;
	bool ___U3CIsDataHighConfidenceU3Ek__BackingField;
	Vector3fU5BU5D_tD8395E99259411E2F0A4F513559CC986FD7AB92E* ___U3CBoneTranslationsU3Ek__BackingField;
	int32_t ___U3CSkeletonChangedCountU3Ek__BackingField;
};
struct SkeletonPoseData_tA0A3A4163EEA849A40C82CF95F1847A9827A5F86_marshaled_pinvoke
{
	Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 ___U3CRootPoseU3Ek__BackingField;
	float ___U3CRootScaleU3Ek__BackingField;
	Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A* ___U3CBoneRotationsU3Ek__BackingField;
	int32_t ___U3CIsDataValidU3Ek__BackingField;
	int32_t ___U3CIsDataHighConfidenceU3Ek__BackingField;
	Vector3f_t232AF83B4642C67BE8EFF85D8E1599D3B06BD562* ___U3CBoneTranslationsU3Ek__BackingField;
	int32_t ___U3CSkeletonChangedCountU3Ek__BackingField;
};
struct SkeletonPoseData_tA0A3A4163EEA849A40C82CF95F1847A9827A5F86_marshaled_com
{
	Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 ___U3CRootPoseU3Ek__BackingField;
	float ___U3CRootScaleU3Ek__BackingField;
	Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A* ___U3CBoneRotationsU3Ek__BackingField;
	int32_t ___U3CIsDataValidU3Ek__BackingField;
	int32_t ___U3CIsDataHighConfidenceU3Ek__BackingField;
	Vector3f_t232AF83B4642C67BE8EFF85D8E1599D3B06BD562* ___U3CBoneTranslationsU3Ek__BackingField;
	int32_t ___U3CSkeletonChangedCountU3Ek__BackingField;
};
struct unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 
{
	uint32_t ___magic;
	uint32_t ___code;
	uint64_t ___reserved;
};
struct unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 
{
	uint32_t ___min;
	uint32_t ___max;
};
struct BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 
{
	int32_t ___keyword;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___offset;
};
struct BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 
{
	int32_t ___m_SizeType;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
};
struct BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 
{
	NativeArray_1_t4020B6981295FB915DCE82EF368535F680C13A49 ___cullingPlanes;
	NativeArray_1_t73992261AA60020B6BE20D83C50B3F925CC89F31 ___cullingSplits;
	LODParameters_t54D2AA0FD8E53BCF51D7A42BC1A72FCA8C78A08A ___lodParameters;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___localToWorldMatrix;
	int32_t ___viewType;
	int32_t ___projectionType;
	int32_t ___cullingFlags;
	BatchPackedCullingViewID_t1E7EE8631C02555CAA181FA566CDC604B9FEFEBB ___viewID;
	uint32_t ___cullingLayerMask;
	uint64_t ___sceneCullingMask;
	uint8_t ___isOrthographic;
	int32_t ___receiverPlaneOffset;
	int32_t ___receiverPlaneCount;
};
struct BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F 
{
	NativeArray_1_tBEE3484B4ABC271CFAB65039F1439061D5DF806A ___drawCommands;
};
struct Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 
{
	Angle_t0229F612898D65B3CC646C40A32D93D8A33C1DFC ___m_Angle;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Axis;
	bool ___m_IsNone;
};
struct Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7_marshaled_pinvoke
{
	Angle_t0229F612898D65B3CC646C40A32D93D8A33C1DFC ___m_Angle;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Axis;
	int32_t ___m_IsNone;
};
struct Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7_marshaled_com
{
	Angle_t0229F612898D65B3CC646C40A32D93D8A33C1DFC ___m_Angle;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___m_Axis;
	int32_t ___m_IsNone;
};
struct StyleLength_tF02B24735FC88BE29BEB36F7A87709CA28AF72D8 
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Value;
	int32_t ___m_Keyword;
};
struct TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
	float ___m_Z;
};
struct Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E 
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
	float ___m_Z;
	bool ___m_isNone;
};
struct Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E_marshaled_pinvoke
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
	float ___m_Z;
	int32_t ___m_isNone;
};
struct Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E_marshaled_com
{
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_X;
	Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 ___m_Y;
	float ___m_Z;
	int32_t ___m_isNone;
};
struct StyleBackgroundSize_t0904929E2E236696CEC8DBD4B1082E8313F84008 
{
	BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B 
{
	Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B_marshaled_pinvoke
{
	Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7_marshaled_pinvoke ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B_marshaled_com
{
	Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7_marshaled_com ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTransformOrigin_t708B2E73541ECAE23D286FE68D6BC2CCFAAB84A6 
{
	TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089 
{
	Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089_marshaled_pinvoke
{
	Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E_marshaled_pinvoke ___m_Value;
	int32_t ___m_Keyword;
};
struct StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089_marshaled_com
{
	Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E_marshaled_com ___m_Value;
	int32_t ___m_Keyword;
};
struct RectangleParams_t0B5A63548DC33EE252AF81E242B719118C235A4B 
{
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___uv;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___subRect;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionX;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionY;
	BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F ___backgroundRepeat;
	BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 ___backgroundSize;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___sprite;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___vectorImage;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material;
	int32_t ___scaleMode;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___playmodeTintColor;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___contentSize;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___textureSize;
	int32_t ___leftSlice;
	int32_t ___topSlice;
	int32_t ___rightSlice;
	int32_t ___bottomSlice;
	float ___sliceScale;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___spriteGeomRect;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___rectInset;
	ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0 ___colorPage;
	int32_t ___meshFlags;
};
struct RectangleParams_t0B5A63548DC33EE252AF81E242B719118C235A4B_marshaled_pinvoke
{
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___uv;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___subRect;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionX;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionY;
	BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F ___backgroundRepeat;
	BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 ___backgroundSize;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___sprite;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___vectorImage;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material;
	int32_t ___scaleMode;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___playmodeTintColor;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___contentSize;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___textureSize;
	int32_t ___leftSlice;
	int32_t ___topSlice;
	int32_t ___rightSlice;
	int32_t ___bottomSlice;
	float ___sliceScale;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___spriteGeomRect;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___rectInset;
	ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0_marshaled_pinvoke ___colorPage;
	int32_t ___meshFlags;
};
struct RectangleParams_t0B5A63548DC33EE252AF81E242B719118C235A4B_marshaled_com
{
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___rect;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___uv;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___color;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___subRect;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionX;
	BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 ___backgroundPositionY;
	BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F ___backgroundRepeat;
	BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 ___backgroundSize;
	Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___texture;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___sprite;
	VectorImage_t7BD8CE948377FFE95FCA0C48014ACDFC13B8F8FC* ___vectorImage;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___material;
	int32_t ___scaleMode;
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___playmodeTintColor;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___topRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomRightRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___bottomLeftRadius;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___contentSize;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___textureSize;
	int32_t ___leftSlice;
	int32_t ___topSlice;
	int32_t ___rightSlice;
	int32_t ___bottomSlice;
	float ___sliceScale;
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___spriteGeomRect;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___rectInset;
	ColorPage_t7C2B8995DE8D27CED5E55F7BFE4E6C70C971FAE0_marshaled_com ___colorPage;
	int32_t ___meshFlags;
};
struct ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093_StaticFields
{
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 ___U3CEmptyU3Ek__BackingField;
};
struct JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5_StaticFields
{
	JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5 ___Empty;
};
struct ValueTask_1_t823DE87C36EA952D24C4E64F532E9D4425B72F21_StaticFields
{
	Task_1_t4C228DE57804012969575431CFF12D57C875552D* ___s_canceledTask;
};
struct CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED_StaticFields
{
	Action_1_t6F9EB113EB3F16226AEF811A2744F4111C116C87* ___s_actionToActionObjShunt;
};
struct CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257_StaticFields
{
	CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 ___Default;
};
struct DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D_StaticFields
{
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth365;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_daysToMonth366;
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MinValue;
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___MaxValue;
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D ___UnixEpoch;
};
struct Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F_StaticFields
{
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___Zero;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___One;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MinusOne;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MaxValue;
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F ___MinValue;
};
struct GrabPoseScore_tD56B46FDCBB34DFDDCDEF7D353038F5AA21A4A82_StaticFields
{
	GrabPoseScore_tD56B46FDCBB34DFDDCDEF7D353038F5AA21A4A82 ___Max;
};
struct Guid_t_StaticFields
{
	Guid_t ___Empty;
};
struct IntPtr_t_StaticFields
{
	intptr_t ___Zero;
};
struct PoseMeasureParameters_t93F958B174ABC0A954B8A6B9B3DFA73DFE894363_StaticFields
{
	PoseMeasureParameters_t93F958B174ABC0A954B8A6B9B3DFA73DFE894363 ___DEFAULT;
};
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion;
};
struct TextureId_tFF4B4AAE53408AB10B0B89CCA5F7B50CF2535E58_StaticFields
{
	TextureId_tFF4B4AAE53408AB10B0B89CCA5F7B50CF2535E58 ___invalid;
};
struct TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A_StaticFields
{
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___Zero;
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___MaxValue;
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A ___MinValue;
};
struct ValueTask_t10B4B5DDF5C582607D0E634FA912F8CB94FCD49F_StaticFields
{
	Task_t751C4CC3ECD055BABA8A0B6A5DFBB4283DCA8572* ___s_canceledTask;
};
struct Vec3_t7EDAE0ABBD2BC4C43A47B8BEF6C079AB55FB0CBB_StaticFields
{
	Vec3_t7EDAE0ABBD2BC4C43A47B8BEF6C079AB55FB0CBB ___Zero;
};
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector;
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector;
};
struct Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A_StaticFields
{
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Zero;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_One;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Up;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Down;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Left;
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A ___s_Right;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector;
};
struct Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376_StaticFields
{
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Zero;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_One;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Up;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Down;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Left;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Right;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Forward;
	Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 ___s_Back;
};
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector;
};
struct DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4_StaticFields
{
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 ___MinValue;
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 ___MaxValue;
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 ___UnixEpoch;
};
struct OVRAnchor_tC6603E0C1628ACAA50D8CCDCC267BFD246F5A061_StaticFields
{
	OVRAnchor_tC6603E0C1628ACAA50D8CCDCC267BFD246F5A061 ___Null;
	SpaceComponentTypeU5BU5D_t0B1B0FC97F1326A6BC34EEDFB77B9BB241D9EB80* ___SupportedComponentsArray;
};
struct Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971_StaticFields
{
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 ___k_Identity;
};
struct ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36_StaticFields
{
	ShaderTagId_t453E2085B5EE9448FF75E550CAB111EFF690ECB0 ___kRenderTypeTag;
};
struct UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207_StaticFields
{
	Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B ___s_DefaultColor;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___s_DefaultTangent;
	UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 ___simpleVert;
};
struct GrabbingRule_tBFBDE400621FCCCEB23EF9A44E42B11AE7DBF27D_StaticFields
{
	GrabbingRule_tBFBDE400621FCCCEB23EF9A44E42B11AE7DBF27D ___U3CDefaultPalmRuleU3Ek__BackingField;
	GrabbingRule_tBFBDE400621FCCCEB23EF9A44E42B11AE7DBF27D ___U3CDefaultPinchRuleU3Ek__BackingField;
	GrabbingRule_tBFBDE400621FCCCEB23EF9A44E42B11AE7DBF27D ___U3CFullGrabU3Ek__BackingField;
};
struct Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F_StaticFields
{
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F ___m_NullPlayable;
};
struct PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680_StaticFields
{
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 ___m_NullPlayableOutput;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif



static  ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 UnresolvedVirtualCall_0 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	ArraySegment_1_t3DC888623B720A071D69279F1FCB95A109195093 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Enumerator_t5438B9989E702349A6790B901A8E6B408ED3B3CD UnresolvedVirtualCall_1 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Enumerator_t5438B9989E702349A6790B901A8E6B408ED3B3CD il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Enumerator_t72556E98D7DDBE118A973D782D523D15A96461C8 UnresolvedVirtualCall_2 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Enumerator_t72556E98D7DDBE118A973D782D523D15A96461C8 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  InteractableSet_t934434D7D540BCC55D05BEF15E0116ADEEF0F987 UnresolvedVirtualCall_3 (RuntimeObject* __this, const RuntimeMethod* method)
{
	InteractableSet_t934434D7D540BCC55D05BEF15E0116ADEEF0F987 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  InteractableSet_t934434D7D540BCC55D05BEF15E0116ADEEF0F987 UnresolvedVirtualCall_4 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	InteractableSet_t934434D7D540BCC55D05BEF15E0116ADEEF0F987 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5 UnresolvedVirtualCall_5 (RuntimeObject* __this, const RuntimeMethod* method)
{
	JEnumerable_1_t3B25D0E67A0BA251980CEF1E7E15E21C611B3BB5 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  KeyValuePair_2_t472CF116DEB4A9261996D42F3FB67E7595E0EC3F UnresolvedVirtualCall_6 (RuntimeObject* __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t472CF116DEB4A9261996D42F3FB67E7595E0EC3F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  KeyValuePair_2_tDC26B09C26BA829DDE331BCB6AF7C508C763D7A3 UnresolvedVirtualCall_7 (RuntimeObject* __this, const RuntimeMethod* method)
{
	KeyValuePair_2_tDC26B09C26BA829DDE331BCB6AF7C508C763D7A3 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 UnresolvedVirtualCall_8 (RuntimeObject* __this, const RuntimeMethod* method)
{
	KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  KeyValuePair_2_t668E6602CE2430EE46E4DEBC02427E14F51F6138 UnresolvedVirtualCall_9 (RuntimeObject* __this, const RuntimeMethod* method)
{
	KeyValuePair_2_t668E6602CE2430EE46E4DEBC02427E14F51F6138 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 UnresolvedVirtualCall_10 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_tEADC262F7F8B8BC4CC0A003DBDD3CA7C1B63F9AC UnresolvedVirtualCall_11 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_tEADC262F7F8B8BC4CC0A003DBDD3CA7C1B63F9AC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t5127ABE6809BA32727C69CB2E076B28D676EB15B UnresolvedVirtualCall_12 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_t5127ABE6809BA32727C69CB2E076B28D676EB15B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB UnresolvedVirtualCall_13 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 UnresolvedVirtualCall_14 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 UnresolvedVirtualCall_15 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Nullable_1_t3D746CBB6123D4569FF4DEA60BC4240F32C6FE75 UnresolvedVirtualCall_16 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	Nullable_1_t3D746CBB6123D4569FF4DEA60BC4240F32C6FE75 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D UnresolvedVirtualCall_17 (RuntimeObject* __this, const RuntimeMethod* method)
{
	ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 UnresolvedVirtualCall_18 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 UnresolvedVirtualCall_19 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleEnum_1_t3DD2EBD4E359AFE77C2974ECAA1DEE50E0FACEDC UnresolvedVirtualCall_20 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleEnum_1_t3DD2EBD4E359AFE77C2974ECAA1DEE50E0FACEDC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTask_1_t823DE87C36EA952D24C4E64F532E9D4425B72F21 UnresolvedVirtualCall_21 (RuntimeObject* __this, Memory_1_tB7CEF4416F5014E364267478CEF016A4AC5C0036 p1, CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	ValueTask_1_t823DE87C36EA952D24C4E64F532E9D4425B72F21 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTuple_2_tB358DB210B9947851BE1C2586AD7532BEB639942 UnresolvedVirtualCall_22 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	ValueTuple_2_tB358DB210B9947851BE1C2586AD7532BEB639942 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTuple_2_tBC19AE73793D615D180F320AB46A541EF61AFBF9 UnresolvedVirtualCall_23 (RuntimeObject* __this, const RuntimeMethod* method)
{
	ValueTuple_2_tBC19AE73793D615D180F320AB46A541EF61AFBF9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTuple_2_tC57529B8C1EE84CA3D138FBE3836C013C6DC40AC UnresolvedVirtualCall_24 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	ValueTuple_2_tC57529B8C1EE84CA3D138FBE3836C013C6DC40AC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTuple_2_tC3717D4552EE1E5FC27BFBA3F5155741BC04557A UnresolvedVirtualCall_25 (RuntimeObject* __this, const RuntimeMethod* method)
{
	ValueTuple_2_tC3717D4552EE1E5FC27BFBA3F5155741BC04557A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTuple_2_t4BC958336F5E31F49459112BAB22FC776AFF71C3 UnresolvedVirtualCall_26 (RuntimeObject* __this, const RuntimeMethod* method)
{
	ValueTuple_2_t4BC958336F5E31F49459112BAB22FC776AFF71C3 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 UnresolvedVirtualCall_27 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  BoundsInt_t4E757DE5EFF9FCB42000F173360DDC63B5585485 UnresolvedVirtualCall_28 (RuntimeObject* __this, const RuntimeMethod* method)
{
	BoundsInt_t4E757DE5EFF9FCB42000F173360DDC63B5585485 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_29 (RuntimeObject* __this, const RuntimeMethod* method)
{
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_29 (void* __this, const RuntimeMethod* method)
{
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_29 (const RuntimeMethod* method)
{
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_30 (RuntimeObject* __this, CustomStyleProperty_1_tE4B20CAB5BCFEE711EB4A26F077DC700987C0C2D p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_31 (RuntimeObject* __this, CustomStyleProperty_1_t6871E5DBF19AB4DC7E1134B32A03B7A458D52E9F p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_32 (RuntimeObject* __this, CustomStyleProperty_1_t697913D630F101B4E464B7E9EA06368015C9C266 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_33 (RuntimeObject* __this, CustomStyleProperty_1_t21332918528099194FD36C74FF0FA14696F39493 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_34 (RuntimeObject* __this, KeyValuePair_2_tFC32D2507216293851350D29B64D79F950B55230 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_35 (RuntimeObject* __this, Span_1_tEDDF15FCF9EC6DEBA0F696BAACDDBAB9D92C252D p1, RuntimeObject* p2, ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_36 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_36 (uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_37 (RuntimeObject* __this, uint8_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_38 (RuntimeObject* __this, uint8_t p1, uint8_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_39 (RuntimeObject* __this, uint8_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_39 (uint8_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_40 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_41 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_42 (RuntimeObject* __this, Guid_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_42 (Guid_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_43 (RuntimeObject* __this, Guid_t p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_44 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_44 (void* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_44 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_45 (RuntimeObject* __this, int32_t p1, Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 p2, Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_46 (RuntimeObject* __this, int32_t p1, BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 p2, BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_47 (RuntimeObject* __this, int32_t p1, BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F p2, BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_48 (RuntimeObject* __this, int32_t p1, BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 p2, BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_49 (RuntimeObject* __this, int32_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_50 (RuntimeObject* __this, int32_t p1, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p2, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_51 (RuntimeObject* __this, int32_t p1, FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C p2, FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_52 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_53 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_54 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_55 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6,&p7,&p8,p9};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_56 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_57 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_58 (RuntimeObject* __this, int32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_59 (RuntimeObject* __this, int32_t p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_60 (RuntimeObject* __this, int32_t p1, Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 p2, Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_61 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_61 (void* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_62 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, uint8_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_63 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_64 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_64 (int32_t p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_65 (RuntimeObject* __this, int32_t p1, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_66 (RuntimeObject* __this, int32_t p1, Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 p2, Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_67 (RuntimeObject* __this, int32_t p1, Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 p2, Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_68 (RuntimeObject* __this, int32_t p1, float p2, float p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_69 (RuntimeObject* __this, int32_t p1, float p2, float p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_69 (int32_t p1, float p2, float p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_70 (RuntimeObject* __this, int32_t p1, TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 p2, TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_71 (RuntimeObject* __this, int32_t p1, TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 p2, TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_72 (RuntimeObject* __this, int32_t p1, Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E p2, Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E p3, int32_t p4, int32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_73 (RuntimeObject* __this, int32_t p1, uint32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_73 (int32_t p1, uint32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_74 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_75 (RuntimeObject* __this, int64_t p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_76 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_76 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_76 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_77 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_77 (RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_78 (RuntimeObject* p1, uint8_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_79 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_80 (RuntimeObject* p1, Guid_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_81 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_81 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_82 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 p3, Background_t3C720DED4FAF016332D29FB86C9BE8D5D0D8F0C8 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_83 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 p3, BackgroundPosition_tF0822B29FC27A67205A9893EBE03D03B799B8B56 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_84 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F p3, BackgroundRepeat_t446EC7315DED2C6822F1047B7587C3018BFB277F p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_85 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 p3, BackgroundSize_t809883E2D7BB1D8D85B4C3E1DBE189F187DB25E7 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_86 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p3, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_87 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C p3, FontDefinition_t65281B0E106365C28AD3F2525DE148719AEEA30C p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_88 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_89 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_90 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 p3, Length_t90BB06D47DD6DB461ED21BD3E3241FAB6C824256 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_91 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_91 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_92 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_93 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_94 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_95 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 p3, Rotate_tE965CA0281A547AB38B881A3416FF97756D3F4D7 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_96 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 p3, Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_97 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, float p3, float p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_98 (RuntimeObject* p1, int32_t p2, float p3, float p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_99 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 p3, TextShadow_t6BADF37AB90ABCB63859A225B58AC5A580950A05 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_100 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 p3, TransformOrigin_tD11A368A96C0771398EBB4E6D435318AC0EF8502 p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_101 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E p3, Translate_t494F6E802F8A640D67819C9D26BE62DED1218A8E p4, int32_t p5, int32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_102 (RuntimeObject* p1, int32_t p2, uint32_t p3, RuntimeObject* p4, uint32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_103 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_103 (void* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_103 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_104 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, uint8_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6,p7,p8};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_105 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_105 (void* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_105 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_106 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_107 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_107 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_108 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint8_t p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,&p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_109 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_109 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_110 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, int32_t p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,&p5,&p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_111 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_111 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_111 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_112 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_113 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_113 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_114 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_115 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 p5, uint8_t p6, uint8_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5,&p6,&p7};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_116 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_117 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint64_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5,p6};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_118 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_119 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_119 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_120 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint64_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_120 (RuntimeObject* p1, RuntimeObject* p2, uint64_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_121 (RuntimeObject* p1, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p2, RuntimeObject* p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_122 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_122 (void* __this, RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_122 (RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_123 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_124 (RuntimeObject* p1, uint32_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_125 (RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_126 (RuntimeObject* p1, uint64_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_127 (RuntimeObject* __this, RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_127 (void* __this, RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_127 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_128 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_129 (RuntimeObject* __this, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p1, int32_t p2, PoseMeasureParameters_t93F958B174ABC0A954B8A6B9B3DFA73DFE894363 p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,p5};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_130 (RuntimeObject* __this, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p1, float p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_131 (RuntimeObject* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, RuntimeObject* p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_131 (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, RuntimeObject* p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_132 (RuntimeObject* __this, RuleMatcher_t327CFEB02C81AA20E639DE949DCBBAB5E92FF28E p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_133 (RuntimeObject* __this, float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_134 (RuntimeObject* __this, TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_135 (RuntimeObject* __this, TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_136 (RuntimeObject* __this, uint16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_137 (RuntimeObject* __this, uint16_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_138 (RuntimeObject* __this, uint16_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_139 (RuntimeObject* __this, uint16_t p1, uint16_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_140 (RuntimeObject* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedInstanceCall_140 (void* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_140 (uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_141 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_141 (uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_142 (RuntimeObject* __this, uint32_t p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_142 (uint32_t p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_143 (RuntimeObject* __this, uint32_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_143 (uint32_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_144 (RuntimeObject* __this, uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_144 (uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_145 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_145 (uint64_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_146 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedStaticCall_146 (uint64_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_147 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_148 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_149 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint8_t UnresolvedVirtualCall_150 (RuntimeObject* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	uint8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Color_tD001788D726C3A7F1379BEED0260B9591F440C1F UnresolvedVirtualCall_151 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900 UnresolvedVirtualCall_152 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	ConsoleKeyInfo_t84640C60F53D0F6946B147ADAAF0366BBF1DE900 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02 UnresolvedVirtualCall_153 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	CustomAttributeNamedArgument_t4EC1C2BB9943BEB7E77AC0870BE2A899E23B4E02 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F UnresolvedVirtualCall_154 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	CustomAttributeTypedArgument_tAAA19ADE66B16A67D030C8C67D7ADB29A7BEC75F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9 UnresolvedVirtualCall_155 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_156 (RuntimeObject* __this, const RuntimeMethod* method)
{
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_157 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_158 (RuntimeObject* __this, DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_159 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_160 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_161 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6,&p7};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_162 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6,&p7,&p8};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_163 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D UnresolvedVirtualCall_164 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 UnresolvedVirtualCall_165 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 UnresolvedVirtualCall_166 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F UnresolvedVirtualCall_167 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F UnresolvedVirtualCall_168 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB UnresolvedVirtualCall_169 (RuntimeObject* __this, const RuntimeMethod* method)
{
	DictionaryEntry_t171080F37B311C25AA9E75888F9C9D703FA721BB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_170 (RuntimeObject* __this, const RuntimeMethod* method)
{
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_171 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_172 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_173 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_174 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  double UnresolvedVirtualCall_175 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	double il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 UnresolvedVirtualCall_176 (RuntimeObject* __this, const RuntimeMethod* method)
{
	EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  GrabPoseScore_tD56B46FDCBB34DFDDCDEF7D353038F5AA21A4A82 UnresolvedVirtualCall_177 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	GrabPoseScore_tD56B46FDCBB34DFDDCDEF7D353038F5AA21A4A82 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  GrabbingRule_tBFBDE400621FCCCEB23EF9A44E42B11AE7DBF27D UnresolvedVirtualCall_178 (RuntimeObject* __this, const RuntimeMethod* method)
{
	GrabbingRule_tBFBDE400621FCCCEB23EF9A44E42B11AE7DBF27D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Guid_t UnresolvedVirtualCall_179 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Guid_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 UnresolvedVirtualCall_180 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HiddenAreaMesh_t_t94244AEA54FC806F524A9FB826CBB0339D3FC8E4 UnresolvedVirtualCall_181 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	HiddenAreaMesh_t_t94244AEA54FC806F524A9FB826CBB0339D3FC8E4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HiddenAreaMesh_t_t94244AEA54FC806F524A9FB826CBB0339D3FC8E4 UnresolvedStaticCall_181 (int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	HiddenAreaMesh_t_t94244AEA54FC806F524A9FB826CBB0339D3FC8E4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HiddenAreaMesh_t_t94244AEA54FC806F524A9FB826CBB0339D3FC8E4 UnresolvedStaticCall_182 (RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	HiddenAreaMesh_t_t94244AEA54FC806F524A9FB826CBB0339D3FC8E4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 UnresolvedVirtualCall_183 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 UnresolvedStaticCall_183 (uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 UnresolvedStaticCall_184 (RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedVirtualCall_185 (RuntimeObject* __this, const RuntimeMethod* method)
{
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedStaticCall_185 (const RuntimeMethod* method)
{
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedVirtualCall_186 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedStaticCall_186 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedStaticCall_187 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedStaticCall_188 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedStaticCall_189 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedVirtualCall_190 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 UnresolvedStaticCall_190 (uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	HmdMatrix34_t_t63D86814DA8F9D9DC7AA3143CE8C95454D5709F9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix44_t_tF23EB340D2BFF58C56BDAB1354E8BBF7FCB16FF4 UnresolvedVirtualCall_191 (RuntimeObject* __this, int32_t p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	HmdMatrix44_t_tF23EB340D2BFF58C56BDAB1354E8BBF7FCB16FF4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix44_t_tF23EB340D2BFF58C56BDAB1354E8BBF7FCB16FF4 UnresolvedStaticCall_191 (int32_t p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	HmdMatrix44_t_tF23EB340D2BFF58C56BDAB1354E8BBF7FCB16FF4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  HmdMatrix44_t_tF23EB340D2BFF58C56BDAB1354E8BBF7FCB16FF4 UnresolvedStaticCall_192 (RuntimeObject* p1, int32_t p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	HmdMatrix44_t_tF23EB340D2BFF58C56BDAB1354E8BBF7FCB16FF4 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int16_t UnresolvedVirtualCall_193 (RuntimeObject* __this, const RuntimeMethod* method)
{
	int16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int16_t UnresolvedVirtualCall_194 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_195 (RuntimeObject* __this, const RuntimeMethod* method)
{
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_195 (void* __this, const RuntimeMethod* method)
{
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_195 (const RuntimeMethod* method)
{
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_196 (RuntimeObject* __this, ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D p1, Span_1_tEDDF15FCF9EC6DEBA0F696BAACDDBAB9D92C252D p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_197 (RuntimeObject* __this, ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D p1, Span_1_tEDDF15FCF9EC6DEBA0F696BAACDDBAB9D92C252D p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_198 (RuntimeObject* __this, ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 p1, Span_1_tDADAC65069DFE6B57C458109115ECD795ED39305 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_199 (RuntimeObject* __this, Span_1_tDADAC65069DFE6B57C458109115ECD795ED39305 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_200 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_200 (void* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_201 (RuntimeObject* __this, uint8_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_202 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_203 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_204 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_205 (RuntimeObject* __this, int16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_206 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_206 (void* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_207 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_207 (void* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_208 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_209 (RuntimeObject* __this, int32_t p1, int32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, uint8_t p6, uint64_t p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5,&p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_209 (int32_t p1, int32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, uint8_t p6, uint64_t p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5,&p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_210 (RuntimeObject* __this, int32_t p1, int64_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_210 (void* __this, int32_t p1, int64_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_211 (RuntimeObject* __this, int32_t p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_211 (int32_t p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_212 (RuntimeObject* __this, int32_t p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_212 (int32_t p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_213 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_213 (int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_214 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_214 (int32_t p1, RuntimeObject* p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_215 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_215 (void* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_216 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_216 (int32_t p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_217 (RuntimeObject* __this, int32_t p1, uint32_t p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_217 (void* __this, int32_t p1, uint32_t p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_218 (RuntimeObject* __this, int32_t p1, uint64_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_218 (int32_t p1, uint64_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_219 (RuntimeObject* __this, int32_t p1, uint64_t p2, uint64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_219 (int32_t p1, uint64_t p2, uint64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_220 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_221 (RuntimeObject* __this, int64_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_222 (RuntimeObject* __this, intptr_t p1, int32_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_222 (intptr_t p1, int32_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_223 (RuntimeObject* __this, intptr_t p1, uint32_t p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_223 (intptr_t p1, uint32_t p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_224 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_224 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_224 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_225 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_225 (RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_226 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_227 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_228 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_228 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_229 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_230 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_230 (RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_231 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_232 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_233 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, uint8_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_234 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, int32_t p6, int32_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,&p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_235 (RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, uint32_t p5, RuntimeObject* p6, uint8_t p7, uint64_t p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,p6,&p7,&p8};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_236 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_236 (RuntimeObject* p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_237 (RuntimeObject* p1, int32_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_238 (RuntimeObject* p1, int32_t p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_239 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_239 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_240 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_241 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_242 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_243 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_244 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_244 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_245 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_246 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, uint32_t p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_246 (RuntimeObject* p1, int32_t p2, uint32_t p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_247 (RuntimeObject* p1, int32_t p2, uint64_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_248 (RuntimeObject* p1, int32_t p2, uint64_t p3, uint64_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_249 (RuntimeObject* p1, intptr_t p2, int32_t p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_250 (RuntimeObject* p1, intptr_t p2, uint32_t p3, RuntimeObject* p4, RuntimeObject* p5, uint32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_251 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_251 (void* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_251 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_252 (RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_253 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_254 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_254 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_255 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_255 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_256 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_257 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int64_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_258 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_259 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint32_t p4, uint32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5,p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_260 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_260 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_260 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_261 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_261 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_262 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_262 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_262 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_263 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_263 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_264 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_265 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_266 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_266 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_267 (RuntimeObject* p1, RuntimeObject* p2, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_268 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_268 (void* __this, RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_268 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_269 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_270 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_271 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, uint32_t p4, uint64_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_272 (RuntimeObject* p1, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p2, RuntimeObject* p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_273 (RuntimeObject* __this, RuntimeObject* p1, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_273 (RuntimeObject* p1, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_274 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_274 (RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_275 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_276 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_277 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5,p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_278 (RuntimeObject* p1, uint32_t p2, int32_t p3, float p4, float p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_279 (RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_280 (RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_281 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_281 (RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_282 (RuntimeObject* p1, uint32_t p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_283 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_283 (RuntimeObject* p1, uint32_t p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_284 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, uint32_t p3, uint64_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_284 (RuntimeObject* p1, uint32_t p2, uint32_t p3, uint64_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_285 (RuntimeObject* p1, uint64_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_286 (RuntimeObject* p1, uint64_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_287 (RuntimeObject* p1, uint64_t p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_288 (RuntimeObject* p1, uint64_t p2, int32_t p3, HmdVector2_t_tCCEF5F67B49C6ABAC22E7757A470D9B127936833 p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_289 (RuntimeObject* p1, uint64_t p2, int32_t p3, int32_t p4, intptr_t p5, uint32_t p6, RuntimeObject* p7, uint64_t p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7,&p8};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_290 (RuntimeObject* p1, uint64_t p2, int32_t p3, int32_t p4, RuntimeObject* p5, uint32_t p6, RuntimeObject* p7, uint8_t p8, uint64_t p9, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5,&p6,p7,&p8,&p9};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_291 (RuntimeObject* p1, uint64_t p2, int32_t p3, int32_t p4, RuntimeObject* p5, uint32_t p6, uint64_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5,&p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_292 (RuntimeObject* p1, uint64_t p2, int32_t p3, intptr_t p4, RuntimeObject* p5, RuntimeObject* p6, uint32_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5,p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_293 (RuntimeObject* p1, uint64_t p2, int32_t p3, intptr_t p4, float p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_294 (RuntimeObject* p1, uint64_t p2, int32_t p3, intptr_t p4, uint32_t p5, RuntimeObject* p6, uint32_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_295 (RuntimeObject* p1, uint64_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_296 (RuntimeObject* p1, uint64_t p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_297 (RuntimeObject* p1, uint64_t p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, uint32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_298 (RuntimeObject* p1, uint64_t p2, int32_t p3, float p4, RuntimeObject* p5, uint32_t p6, uint64_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5,&p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_299 (RuntimeObject* p1, uint64_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_300 (RuntimeObject* p1, uint64_t p2, intptr_t p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_301 (RuntimeObject* p1, uint64_t p2, intptr_t p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_302 (RuntimeObject* p1, uint64_t p2, intptr_t p3, uint32_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5,p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_303 (RuntimeObject* p1, uint64_t p2, intptr_t p3, uint32_t p4, uint32_t p5, uint32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_304 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_305 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5,p6,p7,p8,p9,p10};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_306 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_307 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_308 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_309 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_310 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, uint32_t p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_311 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, uint32_t p4, uint64_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_312 (RuntimeObject* p1, uint64_t p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_313 (RuntimeObject* p1, uint64_t p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_314 (RuntimeObject* p1, uint64_t p2, float p3, float p4, float p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_315 (RuntimeObject* p1, uint64_t p2, float p3, float p4, float p5, float p6, uint64_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_316 (RuntimeObject* p1, uint64_t p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_317 (RuntimeObject* p1, uint64_t p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_318 (RuntimeObject* p1, uint64_t p2, uint64_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_319 (RuntimeObject* p1, uint64_t p2, uint64_t p3, int32_t p4, RuntimeObject* p5, int32_t p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5,&p6,p7,p8};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_320 (RuntimeObject* p1, uint64_t p2, uint64_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_321 (RuntimeObject* p1, uint64_t p2, uint64_t p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_322 (RuntimeObject* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, RuntimeObject* p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_322 (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, RuntimeObject* p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_323 (RuntimeObject* __this, float p1, float p2, float p3, uint8_t p4, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_324 (RuntimeObject* __this, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_324 (void* __this, StyleValueHandle_t5831643AAA7AD8C5C43A4498C5E0A2545F78227D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_325 (RuntimeObject* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_325 (void* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_325 (uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_326 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_326 (uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_327 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_327 (uint32_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_328 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_328 (uint32_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_329 (RuntimeObject* __this, uint32_t p1, int32_t p2, float p3, float p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_329 (uint32_t p1, int32_t p2, float p3, float p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_330 (RuntimeObject* __this, uint32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_330 (uint32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_331 (RuntimeObject* __this, uint32_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_331 (uint32_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_332 (RuntimeObject* __this, uint32_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_332 (void* __this, uint32_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_332 (uint32_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_333 (RuntimeObject* __this, uint32_t p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_333 (uint32_t p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_334 (RuntimeObject* __this, uint32_t p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_334 (void* __this, uint32_t p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_335 (RuntimeObject* __this, uint32_t p1, uint32_t p2, uint64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedInstanceCall_335 (void* __this, uint32_t p1, uint32_t p2, uint64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_336 (RuntimeObject* __this, uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_336 (uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_337 (RuntimeObject* __this, uint64_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_337 (uint64_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_338 (RuntimeObject* __this, uint64_t p1, int32_t p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_338 (uint64_t p1, int32_t p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_339 (RuntimeObject* __this, uint64_t p1, int32_t p2, HmdVector2_t_tCCEF5F67B49C6ABAC22E7757A470D9B127936833 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_339 (uint64_t p1, int32_t p2, HmdVector2_t_tCCEF5F67B49C6ABAC22E7757A470D9B127936833 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_340 (RuntimeObject* __this, uint64_t p1, int32_t p2, int32_t p3, intptr_t p4, uint32_t p5, RuntimeObject* p6, uint64_t p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_340 (uint64_t p1, int32_t p2, int32_t p3, intptr_t p4, uint32_t p5, RuntimeObject* p6, uint64_t p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6,&p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_341 (RuntimeObject* __this, uint64_t p1, int32_t p2, int32_t p3, RuntimeObject* p4, uint32_t p5, RuntimeObject* p6, uint8_t p7, uint64_t p8, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,&p5,p6,&p7,&p8};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_341 (uint64_t p1, int32_t p2, int32_t p3, RuntimeObject* p4, uint32_t p5, RuntimeObject* p6, uint8_t p7, uint64_t p8, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,&p5,p6,&p7,&p8};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_342 (RuntimeObject* __this, uint64_t p1, int32_t p2, int32_t p3, RuntimeObject* p4, uint32_t p5, uint64_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_342 (uint64_t p1, int32_t p2, int32_t p3, RuntimeObject* p4, uint32_t p5, uint64_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_343 (RuntimeObject* __this, uint64_t p1, int32_t p2, intptr_t p3, RuntimeObject* p4, RuntimeObject* p5, uint32_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_343 (uint64_t p1, int32_t p2, intptr_t p3, RuntimeObject* p4, RuntimeObject* p5, uint32_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_344 (RuntimeObject* __this, uint64_t p1, int32_t p2, intptr_t p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_344 (uint64_t p1, int32_t p2, intptr_t p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_345 (RuntimeObject* __this, uint64_t p1, int32_t p2, intptr_t p3, uint32_t p4, RuntimeObject* p5, uint32_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_345 (uint64_t p1, int32_t p2, intptr_t p3, uint32_t p4, RuntimeObject* p5, uint32_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_346 (RuntimeObject* __this, uint64_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_346 (uint64_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_347 (RuntimeObject* __this, uint64_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_347 (uint64_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_348 (RuntimeObject* __this, uint64_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_348 (uint64_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_349 (RuntimeObject* __this, uint64_t p1, int32_t p2, float p3, RuntimeObject* p4, uint32_t p5, uint64_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_349 (uint64_t p1, int32_t p2, float p3, RuntimeObject* p4, uint32_t p5, uint64_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_350 (RuntimeObject* __this, uint64_t p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_350 (uint64_t p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_351 (RuntimeObject* __this, uint64_t p1, intptr_t p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_351 (uint64_t p1, intptr_t p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_352 (RuntimeObject* __this, uint64_t p1, intptr_t p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_352 (uint64_t p1, intptr_t p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_353 (RuntimeObject* __this, uint64_t p1, intptr_t p2, uint32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_353 (uint64_t p1, intptr_t p2, uint32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_354 (RuntimeObject* __this, uint64_t p1, intptr_t p2, uint32_t p3, uint32_t p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_354 (uint64_t p1, intptr_t p2, uint32_t p3, uint32_t p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_355 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_355 (uint64_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_356 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5,p6,p7,p8,p9};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_356 (uint64_t p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5,p6,p7,p8,p9};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_357 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_357 (uint64_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_358 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_358 (uint64_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_359 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_359 (uint64_t p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_360 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_360 (uint64_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_361 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, uint32_t p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_361 (uint64_t p1, RuntimeObject* p2, uint32_t p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_362 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, uint32_t p3, uint64_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_362 (uint64_t p1, RuntimeObject* p2, uint32_t p3, uint64_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_363 (RuntimeObject* __this, uint64_t p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_363 (uint64_t p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_364 (RuntimeObject* __this, uint64_t p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_364 (uint64_t p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_365 (RuntimeObject* __this, uint64_t p1, float p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_365 (uint64_t p1, float p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_366 (RuntimeObject* __this, uint64_t p1, float p2, float p3, float p4, float p5, uint64_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_366 (uint64_t p1, float p2, float p3, float p4, float p5, uint64_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_367 (RuntimeObject* __this, uint64_t p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_367 (uint64_t p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_368 (RuntimeObject* __this, uint64_t p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_368 (uint64_t p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_369 (RuntimeObject* __this, uint64_t p1, uint64_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_369 (uint64_t p1, uint64_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_370 (RuntimeObject* __this, uint64_t p1, uint64_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,&p5,p6,p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_370 (uint64_t p1, uint64_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,p4,&p5,p6,p7};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_371 (RuntimeObject* __this, uint64_t p1, uint64_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_371 (uint64_t p1, uint64_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_372 (RuntimeObject* __this, uint64_t p1, uint64_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedStaticCall_372 (uint64_t p1, uint64_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_373 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int32_t UnresolvedVirtualCall_374 (RuntimeObject* __this, HapticsPcmVibration_tA67530C77AF893E2DAE4AC825EB6477B76251781 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_375 (RuntimeObject* __this, const RuntimeMethod* method)
{
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedStaticCall_375 (const RuntimeMethod* method)
{
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_376 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_377 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_378 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_379 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_380 (RuntimeObject* __this, int64_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_381 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedStaticCall_381 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_382 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  int64_t UnresolvedVirtualCall_383 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	int64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_384 (RuntimeObject* __this, const RuntimeMethod* method)
{
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_385 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_385 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_386 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_386 (int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_387 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_388 (RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_389 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_390 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedInstanceCall_390 (void* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_391 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_391 (RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_392 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_393 (RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedVirtualCall_394 (RuntimeObject* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  intptr_t UnresolvedStaticCall_394 (unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	intptr_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedVirtualCall_395 (RuntimeObject* __this, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p1, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedInstanceCall_395 (void* __this, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p1, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedVirtualCall_396 (RuntimeObject* __this, RuntimeObject* p1, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p2, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedStaticCall_396 (RuntimeObject* p1, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p2, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 UnresolvedStaticCall_397 (RuntimeObject* p1, RuntimeObject* p2, BatchCullingContext_t6133D8CF3B9A93AED429E017C62DC2F5BD64A659 p3, BatchCullingOutput_tF997DE602CE8F5E44654FD157113EF455DBE785F p4, intptr_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5};
	JobHandle_t5DF5F99902FED3C801A81C05205CEA6CE039EF08 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB UnresolvedVirtualCall_398 (RuntimeObject* __this, const RuntimeMethod* method)
{
	LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  LightStats_tBB72AF16728E19482A5C8A6B65A94F7FFB9DA80C UnresolvedVirtualCall_399 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	LightStats_tBB72AF16728E19482A5C8A6B65A94F7FFB9DA80C il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD UnresolvedVirtualCall_400 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	MemoryHandle_t505785861D4FF84F850A3FF775BE6AE1833D2AFD il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  OVRSpaceUser_tF145982B655F69985F22D9AB527F17FC76CDC90F UnresolvedVirtualCall_401 (RuntimeObject* __this, const RuntimeMethod* method)
{
	OVRSpaceUser_tF145982B655F69985F22D9AB527F17FC76CDC90F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  OVRSpaceUser_tF145982B655F69985F22D9AB527F17FC76CDC90F UnresolvedVirtualCall_402 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	OVRSpaceUser_tF145982B655F69985F22D9AB527F17FC76CDC90F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_403 (RuntimeObject* __this, const RuntimeMethod* method)
{
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_403 (void* __this, const RuntimeMethod* method)
{
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_403 (const RuntimeMethod* method)
{
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_404 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_405 (RuntimeObject* __this, uint8_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_406 (RuntimeObject* __this, CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_407 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_408 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_409 (RuntimeObject* __this, DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_410 (RuntimeObject* __this, Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_411 (RuntimeObject* __this, Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_412 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_413 (RuntimeObject* __this, double p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_414 (RuntimeObject* __this, Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_415 (RuntimeObject* __this, int16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_416 (RuntimeObject* __this, int16_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_417 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_417 (void* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_417 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_418 (RuntimeObject* __this, int32_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_419 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_420 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_421 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_422 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_423 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_424 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_425 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4,p5,p6,p7};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_426 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_427 (RuntimeObject* __this, int64_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_428 (RuntimeObject* __this, intptr_t p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_428 (void* __this, intptr_t p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_429 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_429 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_429 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_430 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_431 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_432 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_433 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_434 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_434 (void* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_434 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_435 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_436 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_437 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_438 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, LoadSceneParameters_tFBAFEA7FA75F282D3034241AD8756A7B5578310E p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_439 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_440 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_441 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_442 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5,p6,p7,p8};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_443 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_443 (RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_444 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_444 (void* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_444 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_445 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_446 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_447 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_447 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_448 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_449 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint8_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_450 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_451 (RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_452 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_452 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_452 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_453 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_454 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint8_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_455 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_456 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_456 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_456 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_457 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_457 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_457 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_458 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,&p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_459 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_459 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_459 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_460 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_460 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_460 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_461 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_461 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_461 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_462 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_462 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_462 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_463 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_463 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_463 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_464 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_464 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_464 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_465 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_465 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_465 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_466 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_466 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_466 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_467 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_467 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_467 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_468 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_468 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_468 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_469 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_469 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_470 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, RuntimeObject* p17, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_471 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_472 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_472 (RuntimeObject* p1, RuntimeObject* p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_473 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_474 (RuntimeObject* p1, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_475 (RuntimeObject* __this, RuntimeObject* p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedInstanceCall_475 (void* __this, RuntimeObject* p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_476 (RuntimeObject* __this, RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_477 (RuntimeObject* p1, uint32_t p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_478 (RuntimeObject* p1, Vec3_t7EDAE0ABBD2BC4C43A47B8BEF6C079AB55FB0CBB p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_479 (RuntimeObject* p1, unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p2, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_480 (RuntimeObject* p1, unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p2, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p3, uint64_t p4, uint64_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,p6};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_481 (RuntimeObject* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, float p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_481 (Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, float p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_482 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_483 (RuntimeObject* __this, float p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_484 (RuntimeObject* __this, float p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_485 (RuntimeObject* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_486 (RuntimeObject* __this, StyleValues_t4AED947A53B84B62EF2B589A40B74911CA77D11A p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_487 (RuntimeObject* __this, TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_488 (RuntimeObject* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_489 (RuntimeObject* __this, uint32_t p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_489 (uint32_t p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_490 (RuntimeObject* __this, uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_491 (RuntimeObject* __this, Vec3_t7EDAE0ABBD2BC4C43A47B8BEF6C079AB55FB0CBB p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_491 (Vec3_t7EDAE0ABBD2BC4C43A47B8BEF6C079AB55FB0CBB p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_492 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_493 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_494 (RuntimeObject* __this, unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p1, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_494 (unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p1, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedVirtualCall_495 (RuntimeObject* __this, unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p1, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p2, uint64_t p3, uint64_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeObject* UnresolvedStaticCall_495 (unitytls_tlsctx_protocolrange_tC9BEAD436B8171684A1DE9991676D9FEFF879C56 p1, unitytls_tlsctx_callbacks_t348AE3D333ACBB2F17D4D7B8412256357B39B568 p2, uint64_t p3, uint64_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5};
	RuntimeObject* il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  PenData_t2345B5FBD18D851528C5C18F8A667D4EF4690945 UnresolvedVirtualCall_496 (RuntimeObject* __this, const RuntimeMethod* method)
{
	PenData_t2345B5FBD18D851528C5C18F8A667D4EF4690945 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F UnresolvedVirtualCall_497 (RuntimeObject* __this, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	Playable_t95C6B795846BA0C7D96E4DA14897CCCF2554334F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 UnresolvedStaticCall_498 (RuntimeObject* p1, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 UnresolvedVirtualCall_499 (RuntimeObject* __this, PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 UnresolvedStaticCall_499 (PlayableGraph_t4A5B0B45343A240F0761574FD7C672E0CFFF7A6E p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	PlayableOutput_t2F7C45A58DA3E788EEDDB439549E21CF3FCF3680 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedVirtualCall_500 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedVirtualCall_501 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedVirtualCall_502 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedInstanceCall_502 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedVirtualCall_503 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedStaticCall_503 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedStaticCall_504 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedVirtualCall_505 (RuntimeObject* __this, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 UnresolvedVirtualCall_506 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD UnresolvedVirtualCall_507 (RuntimeObject* __this, const RuntimeMethod* method)
{
	ProfilerMarker_tA256E18DA86EDBC5528CE066FC91C96EE86501AD il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 UnresolvedVirtualCall_508 (RuntimeObject* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF UnresolvedVirtualCall_509 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 UnresolvedVirtualCall_510 (RuntimeObject* __this, Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Ray_t2B1742D7958DC05BDC3EFC7461D3593E1430DC00 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA UnresolvedStaticCall_511 (RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA UnresolvedVirtualCall_512 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA UnresolvedStaticCall_512 (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	RaycastHit2D_t3EAAA06E6603C6BC61AC1291DD881C5C1E23BDFA il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D UnresolvedVirtualCall_513 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  ReleaseVelocityInformation_t70EFA79B39A6D6D6B6C771E9E043F61F13B0287D UnresolvedVirtualCall_514 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	ReleaseVelocityInformation_t70EFA79B39A6D6D6B6C771E9E043F61F13B0287D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 UnresolvedVirtualCall_515 (RuntimeObject* __this, const RuntimeMethod* method)
{
	RuntimeFieldHandle_t6E4C45B6D2EA12FC99185805A7E77527899B25C5 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeMethodHandle_tB35B96E97214DCBE20B0B02B1E687884B34680B2 UnresolvedVirtualCall_516 (RuntimeObject* __this, const RuntimeMethod* method)
{
	RuntimeMethodHandle_tB35B96E97214DCBE20B0B02B1E687884B34680B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B UnresolvedVirtualCall_517 (RuntimeObject* __this, const RuntimeMethod* method)
{
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int8_t UnresolvedVirtualCall_518 (RuntimeObject* __this, const RuntimeMethod* method)
{
	int8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  int8_t UnresolvedVirtualCall_519 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	int8_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 UnresolvedVirtualCall_520 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Scale_t5594C69C1AC9398B57ABF6C4FA0D4E791B7A4DC7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_521 (RuntimeObject* __this, const RuntimeMethod* method)
{
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_521 (const RuntimeMethod* method)
{
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_522 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_523 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_524 (RuntimeObject* __this, int32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_525 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_525 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_526 (RuntimeObject* __this, RuntimeObject* p1, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_527 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedInstanceCall_527 (void* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_528 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_528 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_529 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_530 (RuntimeObject* p1, RuntimeObject* p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_531 (RuntimeObject* __this, RuntimeObject* p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_531 (RuntimeObject* p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_532 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_533 (RuntimeObject* p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_534 (RuntimeObject* __this, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_535 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_536 (RuntimeObject* __this, float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedInstanceCall_536 (void* __this, float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_537 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_537 (uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedVirtualCall_538 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  float UnresolvedStaticCall_538 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	float il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 UnresolvedVirtualCall_539 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 UnresolvedVirtualCall_540 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 UnresolvedVirtualCall_541 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleBackgroundSize_t0904929E2E236696CEC8DBD4B1082E8313F84008 UnresolvedVirtualCall_542 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleBackgroundSize_t0904929E2E236696CEC8DBD4B1082E8313F84008 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610 UnresolvedVirtualCall_543 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleCursor_tE485E9D7E54AC3A3D514CD63313D77F75BD8C610 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleFloat_t4A100BCCDC275C2302517C5858C9BE9EC43D4841 UnresolvedVirtualCall_544 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleFloat_t4A100BCCDC275C2302517C5858C9BE9EC43D4841 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleLength_tF02B24735FC88BE29BEB36F7A87709CA28AF72D8 UnresolvedVirtualCall_545 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleLength_tF02B24735FC88BE29BEB36F7A87709CA28AF72D8 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B UnresolvedVirtualCall_546 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleRotate_t59305F0FBB44EA70AE332ECF9279C270B3F2283B il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC UnresolvedVirtualCall_547 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleScale_t45D687B313B39CD6FB3686ED44DECDDA402923BC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleTextShadow_tCDDF1FE733ADBAA5ACA3B74620D4728E83F54252 UnresolvedVirtualCall_548 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleTextShadow_tCDDF1FE733ADBAA5ACA3B74620D4728E83F54252 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleTransformOrigin_t708B2E73541ECAE23D286FE68D6BC2CCFAAB84A6 UnresolvedVirtualCall_549 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleTransformOrigin_t708B2E73541ECAE23D286FE68D6BC2CCFAAB84A6 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089 UnresolvedVirtualCall_550 (RuntimeObject* __this, const RuntimeMethod* method)
{
	StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedVirtualCall_551 (RuntimeObject* __this, const RuntimeMethod* method)
{
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedInstanceCall_551 (void* __this, const RuntimeMethod* method)
{
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedVirtualCall_552 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedStaticCall_552 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedStaticCall_553 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A UnresolvedVirtualCall_554 (RuntimeObject* __this, TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Touch_t03E51455ED508492B3F278903A0114FA0E87B417 UnresolvedVirtualCall_555 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Touch_t03E51455ED508492B3F278903A0114FA0E87B417 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD UnresolvedVirtualCall_556 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	UICharInfo_t24C2EA0F2F3A938100C271891D9DEB015ABA5FBD il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC UnresolvedVirtualCall_557 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	UILineInfo_tC6FF4F85BD2316FADA2148A1789B3FF0B05A6CAC il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 UnresolvedVirtualCall_558 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	UIVertex_tF5C663F4BBC786C9D56C28016FF66E6C6BF85207 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_559 (RuntimeObject* __this, const RuntimeMethod* method)
{
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_560 (RuntimeObject* __this, int32_t p1, uint16_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedInstanceCall_560 (void* __this, int32_t p1, uint16_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_561 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_562 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, uint16_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedStaticCall_562 (RuntimeObject* p1, int32_t p2, uint16_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedStaticCall_563 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint16_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_564 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint16_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint16_t UnresolvedVirtualCall_565 (RuntimeObject* __this, uint16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint16_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_566 (RuntimeObject* __this, const RuntimeMethod* method)
{
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_566 (void* __this, const RuntimeMethod* method)
{
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_566 (const RuntimeMethod* method)
{
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_567 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_567 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_568 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_568 (void* __this, int32_t p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_569 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, uint32_t p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_569 (int32_t p1, RuntimeObject* p2, uint32_t p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_570 (RuntimeObject* __this, intptr_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_570 (intptr_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_571 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_571 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_571 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_572 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_573 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_573 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_574 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, uint32_t p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_575 (RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_576 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_576 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_577 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, uint32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_578 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_579 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_580 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_580 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_581 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_582 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_582 (void* __this, RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_582 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_583 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_583 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_584 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_585 (RuntimeObject* p1, RuntimeObject* p2, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_586 (RuntimeObject* p1, RuntimeObject* p2, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_587 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_587 (void* __this, RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_587 (RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_588 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, uint32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_589 (RuntimeObject* p1, uint32_t p2, int32_t p3, uint32_t p4, intptr_t p5, uint32_t p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,p7};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_590 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_590 (void* __this, RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_591 (RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,&p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_592 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_592 (RuntimeObject* p1, uint32_t p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_593 (RuntimeObject* p1, uint64_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_594 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_595 (RuntimeObject* p1, uint64_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_596 (RuntimeObject* __this, RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_596 (RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_597 (RuntimeObject* __this, RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_597 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_598 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,p5,p6,p7};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_599 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,p6,p7,p8};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_600 (RuntimeObject* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_600 (void* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_601 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_601 (uint32_t p1, int32_t p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_602 (RuntimeObject* __this, uint32_t p1, int32_t p2, uint32_t p3, intptr_t p4, uint32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_602 (uint32_t p1, int32_t p2, uint32_t p3, intptr_t p4, uint32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_603 (RuntimeObject* __this, uint32_t p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_603 (uint32_t p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_604 (RuntimeObject* __this, uint32_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_604 (void* __this, uint32_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_604 (uint32_t p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_605 (RuntimeObject* __this, uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_605 (uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_606 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_606 (uint64_t p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_607 (RuntimeObject* __this, uint64_t p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_607 (uint64_t p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_608 (RuntimeObject* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_608 (void* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, uint32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_609 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedInstanceCall_609 (void* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_610 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_610 (unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,p4,p5,p6};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedVirtualCall_611 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5,p6,p7};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint32_t UnresolvedStaticCall_611 (unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,p5,p6,p7};
	uint32_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_612 (RuntimeObject* __this, const RuntimeMethod* method)
{
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedInstanceCall_612 (void* __this, const RuntimeMethod* method)
{
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_612 (const RuntimeMethod* method)
{
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_613 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedInstanceCall_613 (void* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_614 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedInstanceCall_614 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_614 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_615 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_615 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_616 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_616 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_617 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_618 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_619 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_620 (RuntimeObject* p1, uint64_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_621 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_621 (uint32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedVirtualCall_622 (RuntimeObject* __this, uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  uint64_t UnresolvedStaticCall_622 (uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	uint64_t il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  ValueTask_t10B4B5DDF5C582607D0E634FA912F8CB94FCD49F UnresolvedVirtualCall_623 (RuntimeObject* __this, ReadOnlyMemory_1_t63F301BF893B0AB689953D86A641168CA66D2399 p1, CancellationToken_t51142D9C6D7C02D314DA34A6A7988C528992FFED p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	ValueTask_t10B4B5DDF5C582607D0E634FA912F8CB94FCD49F il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 UnresolvedVirtualCall_624 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 UnresolvedVirtualCall_625 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 UnresolvedVirtualCall_626 (RuntimeObject* __this, RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, uint8_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 UnresolvedVirtualCall_627 (RuntimeObject* __this, RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, uint8_t p3, int32_t p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,p5,p6};
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 UnresolvedVirtualCall_628 (RuntimeObject* __this, float p1, int32_t p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A UnresolvedStaticCall_629 (RuntimeObject* p1, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A UnresolvedVirtualCall_630 (RuntimeObject* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A UnresolvedStaticCall_630 (Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_631 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_632 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedStaticCall_633 (RuntimeObject* p1, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_634 (RuntimeObject* __this, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedStaticCall_634 (Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_635 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 UnresolvedVirtualCall_636 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 UnresolvedVirtualCall_637 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  void UnresolvedVirtualCall_638 (RuntimeObject* __this, const RuntimeMethod* method)
{
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, NULL);
}
static  void UnresolvedInstanceCall_638 (void* __this, const RuntimeMethod* method)
{
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, NULL, NULL);
}
static  void UnresolvedStaticCall_638 (const RuntimeMethod* method)
{
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, NULL);
}
static  void UnresolvedVirtualCall_639 (RuntimeObject* __this, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_639 (void* __this, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_640 (RuntimeObject* __this, Nullable_1_t78F453FADB4A9F50F267A4E349019C34410D1A01 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_641 (RuntimeObject* __this, Nullable_1_tEB6689CC9747A3600689077DCBF77B8E8B510505 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_642 (RuntimeObject* __this, Nullable_1_tD52F1D0FC7EBB336F119BE953E59F426766032C1 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_643 (RuntimeObject* __this, Nullable_1_tEADC262F7F8B8BC4CC0A003DBDD3CA7C1B63F9AC p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_644 (RuntimeObject* __this, Nullable_1_t5127ABE6809BA32727C69CB2E076B28D676EB15B p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_645 (RuntimeObject* __this, Nullable_1_t072551AA1AA8366A46F232F8180C34AA0CFFACBB p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_646 (RuntimeObject* __this, Nullable_1_t6E154519A812D040E3016229CD7638843A2CC165 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_647 (RuntimeObject* __this, Nullable_1_t0ECB838EB0C9A81655750B26970F21CF9A83A5F7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_648 (RuntimeObject* __this, Nullable_1_t57D99A484501B89DA27E67D6D9A89722D5A7DE2C p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_649 (RuntimeObject* __this, Nullable_1_tCF32C56A2641879C053C86F273C0C6EC1B40BC28 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_650 (RuntimeObject* __this, Nullable_1_t365991B3904FDA7642A788423B28692FDC7CDB17 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_651 (RuntimeObject* __this, Nullable_1_tCF16C2638810B89EAA3EEFE6B35FC71B6AE96B2C p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_652 (RuntimeObject* __this, Nullable_1_t3D746CBB6123D4569FF4DEA60BC4240F32C6FE75 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_653 (RuntimeObject* __this, Nullable_1_tE151CE1F6892804B41C4004C95CB57020ABB3272 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_654 (RuntimeObject* __this, Nullable_1_t70F850DEE49B62D1B877D3C32F9E0EC724ADC4D9 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_655 (RuntimeObject* __this, Nullable_1_tD043F01310E483091D0E9A5526C3425F13EF2099 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_656 (RuntimeObject* __this, Nullable_1_tF8BFF19FF240C9F0A45168187CD7106BAA146A99 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_657 (RuntimeObject* __this, ReadOnlySpan_1_tA850A6C0E88ABBA37646A078ACBC24D6D5FD9B4D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_658 (RuntimeObject* __this, ReadOnlySpan_1_t59614EA6E51A945A32B02AB17FBCBDF9A5C419C1 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_659 (RuntimeObject* __this, Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_659 (Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_660 (RuntimeObject* __this, StyleEnum_1_t3DD2EBD4E359AFE77C2974ECAA1DEE50E0FACEDC p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_661 (RuntimeObject* __this, Bounds_t367E830C64BBF235ED8C3B2F8CF6254FDCAD39C3 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_662 (RuntimeObject* __this, BoundsInt_t4E757DE5EFF9FCB42000F173360DDC63B5585485 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_663 (RuntimeObject* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_663 (void* __this, uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_663 (uint8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_664 (RuntimeObject* __this, uint8_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_665 (RuntimeObject* __this, uint8_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_665 (void* __this, uint8_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_666 (RuntimeObject* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_667 (RuntimeObject* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p1, float p2, uint8_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_668 (RuntimeObject* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p1, float p2, uint8_t p3, uint8_t p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_669 (RuntimeObject* __this, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_670 (RuntimeObject* __this, CullingGroupEvent_tC79BA328A8280C29F6002F591614081A0E87D110 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_670 (CullingGroupEvent_tC79BA328A8280C29F6002F591614081A0E87D110 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_671 (RuntimeObject* __this, Cursor_t24C3B5095F65B86794C4F7EA168E324DFDA9EE82 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_672 (RuntimeObject* __this, DSAParameters_t2FA923FEA7E2DB5515EE54A7E86B0401D025E0E9 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_673 (RuntimeObject* __this, DateTime_t66193957C73913903DDAD89FEDC46139BCA5802D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_674 (RuntimeObject* __this, DateTimeOffset_t4EE701FE2F386D6F932FAC9B11E4B74A5B30F0A4 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_675 (RuntimeObject* __this, Decimal_tDA6C877282B2D789CF97C0949661CC11D643969F p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_676 (RuntimeObject* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_676 (void* __this, double p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_677 (RuntimeObject* __this, EventInterests_tF375F3296A6689BC4B016F237123DB5457515EC8 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_678 (RuntimeObject* __this, Guid_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_679 (RuntimeObject* __this, Guid_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_679 (Guid_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_680 (RuntimeObject* __this, Guid_t p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_681 (RuntimeObject* __this, Hash128_t93367F504B687578F893CDBCD13FB95AC8A87A40 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_682 (RuntimeObject* __this, HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_682 (HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_683 (RuntimeObject* __this, int16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_684 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_684 (void* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_684 (int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_685 (RuntimeObject* __this, int32_t p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_686 (RuntimeObject* __this, int32_t p1, int16_t p2, int32_t p3, int64_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_687 (RuntimeObject* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_687 (void* __this, int32_t p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_688 (RuntimeObject* __this, int32_t p1, int32_t p2, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_689 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_690 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_691 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_692 (RuntimeObject* __this, int32_t p1, int32_t p2, int32_t p3, int64_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_693 (RuntimeObject* __this, int32_t p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_694 (RuntimeObject* __this, int32_t p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_695 (RuntimeObject* __this, int32_t p1, int32_t p2, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p3, float p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_696 (RuntimeObject* __this, int32_t p1, int64_t p2, int64_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_696 (int32_t p1, int64_t p2, int64_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_697 (RuntimeObject* __this, int32_t p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_697 (void* __this, int32_t p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_698 (RuntimeObject* __this, int32_t p1, intptr_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_698 (int32_t p1, intptr_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_699 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_699 (void* __this, int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_699 (int32_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_700 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_701 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_702 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_703 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_704 (RuntimeObject* __this, int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_704 (int32_t p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_705 (RuntimeObject* __this, int32_t p1, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_706 (RuntimeObject* __this, int32_t p1, float p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_706 (void* __this, int32_t p1, float p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_707 (RuntimeObject* __this, int32_t p1, float p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_707 (int32_t p1, float p2, RuntimeObject* p3, uint32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_708 (RuntimeObject* __this, int32_t p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_709 (RuntimeObject* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_709 (void* __this, int64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_710 (RuntimeObject* __this, int64_t p1, RuntimeObject* p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_711 (RuntimeObject* __this, intptr_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_711 (intptr_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_712 (RuntimeObject* __this, intptr_t p1, intptr_t p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_712 (void* __this, intptr_t p1, intptr_t p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_713 (RuntimeObject* __this, intptr_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_713 (void* __this, intptr_t p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_714 (RuntimeObject* __this, LayerMask_t97CB6BDADEDC3D6423C7BCFEA7F86DA2EC6241DB p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_715 (RuntimeObject* __this, LocomotionEvent_tECDD51A234545203F40F068C91D2745873285642 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_716 (RuntimeObject* __this, OVRAnchor_tC6603E0C1628ACAA50D8CCDCC267BFD246F5A061 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_717 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_717 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_717 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_718 (RuntimeObject* __this, RuntimeObject* p1, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_718 (RuntimeObject* p1, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_719 (RuntimeObject* p1, Span_1_t3F436092261253E8F2AF9867CA253C3B370766C2 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_720 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_720 (RuntimeObject* p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_721 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_722 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, uint8_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_723 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_723 (RuntimeObject* p1, uint8_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_724 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_724 (void* __this, RuntimeObject* p1, uint8_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_725 (RuntimeObject* __this, RuntimeObject* p1, uint8_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_726 (RuntimeObject* p1, CullingGroupEvent_tC79BA328A8280C29F6002F591614081A0E87D110 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_727 (RuntimeObject* __this, RuntimeObject* p1, double p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_727 (RuntimeObject* p1, double p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_728 (RuntimeObject* p1, Guid_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_729 (RuntimeObject* p1, HmdColor_t_tD211FE8C3842A816107B1EA05CCFBE0C49625079 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_730 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_730 (void* __this, RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_730 (RuntimeObject* p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_731 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, uint8_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_732 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_732 (RuntimeObject* p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_733 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_734 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, int32_t p3, RuntimeObject* p4, int32_t p5, int32_t p6, uint8_t p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5,&p6,&p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_735 (RuntimeObject* p1, int32_t p2, int64_t p3, int64_t p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_736 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_736 (RuntimeObject* p1, int32_t p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_737 (RuntimeObject* p1, int32_t p2, intptr_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_738 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_738 (void* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_738 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_739 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, RuntimeObject* p3, int32_t p4, uint8_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_740 (RuntimeObject* p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_741 (RuntimeObject* __this, RuntimeObject* p1, int32_t p2, float p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_741 (RuntimeObject* p1, int32_t p2, float p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_742 (RuntimeObject* p1, int32_t p2, float p3, RuntimeObject* p4, uint32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_743 (RuntimeObject* __this, RuntimeObject* p1, int64_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_743 (RuntimeObject* p1, int64_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_744 (RuntimeObject* p1, intptr_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_745 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, intptr_t p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_745 (RuntimeObject* p1, intptr_t p2, intptr_t p3, RuntimeObject* p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_746 (RuntimeObject* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_746 (void* __this, RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_746 (RuntimeObject* p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_747 (RuntimeObject* __this, RuntimeObject* p1, LineInfo_t415DCF0EAD0FB3806F779BA170EC9058E47CCB24 p2, LineInfo_t415DCF0EAD0FB3806F779BA170EC9058E47CCB24 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_748 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_748 (void* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_748 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_749 (RuntimeObject* p1, RuntimeObject* p2, NativeArray_1_tDF6A1978B5813BF4DAD7948E398009FFC9BEA38D p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_750 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_750 (RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_751 (RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_752 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_752 (RuntimeObject* p1, RuntimeObject* p2, uint8_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_753 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, CreationContext_t9C57B5BE551CCE200C0A2C72711BFF9DA298C257 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_754 (RuntimeObject* p1, RuntimeObject* p2, double p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_755 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int16_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_756 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_756 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_757 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_758 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_759 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, int32_t p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_760 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_761 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_761 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_762 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_763 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_764 (RuntimeObject* p1, RuntimeObject* p2, int32_t p3, float p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_765 (RuntimeObject* p1, RuntimeObject* p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_766 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_766 (void* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_767 (RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, intptr_t p4, RuntimeObject* p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,p5,&p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_768 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_768 (RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_769 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_769 (void* __this, RuntimeObject* p1, RuntimeObject* p2, intptr_t p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4,&p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_770 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_770 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_770 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_771 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint8_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_772 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_772 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_773 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, int32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_774 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_774 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_775 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_776 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, intptr_t p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5,&p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_776 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, intptr_t p4, RuntimeObject* p5, intptr_t p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5,&p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_777 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_777 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_777 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_778 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, uint8_t p5, uint8_t p6, uint8_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5,&p6,&p7};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_779 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, intptr_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_780 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, intptr_t p5, RuntimeObject* p6, intptr_t p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5,p6,&p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_781 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_781 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_781 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_782 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_782 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_782 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_783 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_783 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_783 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_784 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_784 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_784 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_785 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_785 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_785 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_786 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_786 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_786 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_787 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_787 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_787 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_788 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_788 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_788 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_789 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_789 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_789 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_790 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_790 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_790 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_791 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_791 (void* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_791 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_792 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_792 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_793 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, RuntimeObject* p7, RuntimeObject* p8, RuntimeObject* p9, RuntimeObject* p10, RuntimeObject* p11, RuntimeObject* p12, RuntimeObject* p13, RuntimeObject* p14, RuntimeObject* p15, RuntimeObject* p16, RuntimeObject* p17, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_794 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, uint32_t p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,p4,&p5,p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_795 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p4, uint8_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_796 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, float p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_797 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_797 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, uint32_t p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3,&p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_798 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_798 (RuntimeObject* p1, RuntimeObject* p2, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p3, uint8_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_799 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_800 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, float p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_800 (RuntimeObject* p1, RuntimeObject* p2, float p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_801 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_801 (RuntimeObject* p1, RuntimeObject* p2, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_802 (RuntimeObject* p1, RuntimeObject* p2, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_803 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, TextureId_tFF4B4AAE53408AB10B0B89CCA5F7B50CF2535E58 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_804 (RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_805 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_805 (void* __this, RuntimeObject* p1, RuntimeObject* p2, uint32_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_806 (RuntimeObject* p1, RuntimeObject* p2, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_807 (RuntimeObject* p1, RuntimeObject* p2, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_808 (RuntimeObject* p1, PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE p2, ReadOnly_t1D4689336F49F434532D72398BFBE7BF4D6059D4 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_809 (RuntimeObject* __this, RuntimeObject* p1, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_810 (RuntimeObject* __this, RuntimeObject* p1, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_810 (void* __this, RuntimeObject* p1, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_810 (RuntimeObject* p1, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_811 (RuntimeObject* __this, RuntimeObject* p1, RenderTextureDescriptor_t69845881CE6437E4E61F92074F2F84079F23FA46 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_812 (RuntimeObject* __this, RuntimeObject* p1, ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_813 (RuntimeObject* p1, ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, RuntimeObject* p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,p4,p5,p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_814 (RuntimeObject* __this, RuntimeObject* p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_815 (RuntimeObject* p1, float p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_816 (RuntimeObject* __this, RuntimeObject* p1, float p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_816 (void* __this, RuntimeObject* p1, float p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_817 (RuntimeObject* p1, float p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_818 (RuntimeObject* p1, float p2, float p3, float p4, float p5, float p6, uint8_t p7, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5,&p6,&p7};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_819 (RuntimeObject* __this, RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_819 (RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_820 (RuntimeObject* __this, RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_820 (RuntimeObject* p1, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_821 (RuntimeObject* __this, RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_821 (RuntimeObject* p1, uint32_t p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_822 (RuntimeObject* p1, uint32_t p2, int32_t p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_823 (RuntimeObject* p1, uint32_t p2, uint32_t p3, uint16_t p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedStaticCall_824 (RuntimeObject* p1, uint64_t p2, HmdRect2_t_tAF394D41DC1EEC399E9D2B45C173C3504AA23C74 p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_825 (RuntimeObject* __this, RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_826 (RuntimeObject* __this, RuntimeObject* p1, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_827 (RuntimeObject* __this, RuntimeObject* p1, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 p2, RuntimeObject* p3, uint8_t p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_828 (RuntimeObject* __this, RuntimeObject* p1, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 p2, RuntimeObject* p3, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 p4, uint8_t p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3,&p4,&p5,&p6};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_829 (RuntimeObject* __this, RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_829 (RuntimeObject* p1, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_830 (RuntimeObject* __this, RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_830 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_831 (RuntimeObject* __this, PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE p1, ReadOnly_t1D4689336F49F434532D72398BFBE7BF4D6059D4 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_831 (PhysicsScene_t55222DD37072E8560EE054A07C0E3FE391D9D9DE p1, ReadOnly_t1D4689336F49F434532D72398BFBE7BF4D6059D4 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_832 (RuntimeObject* __this, PointerEvent_tAEB047AC9AE96DA96400B3C6FA88E56C917608BC p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_833 (RuntimeObject* __this, Pose_t06BA69EAA6E9FAF60056D519A87D25F54AFE7971 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_834 (RuntimeObject* __this, RSAParameters_t14B738B69F9D1EB594D5F391BDF8E42BA16435FF p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_835 (RuntimeObject* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_836 (RuntimeObject* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_837 (RuntimeObject* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D p1, RuntimeObject* p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_838 (RuntimeObject* __this, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_839 (RuntimeObject* __this, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_839 (void* __this, RectInt_t1744D10E1063135DA9D574F95205B98DAC600CB8 p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_840 (RuntimeObject* __this, int8_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_841 (RuntimeObject* __this, ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_842 (RuntimeObject* __this, ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_843 (RuntimeObject* __this, ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_843 (ScriptableRenderContext_t5AB09B3602BEB456E0DC3D53926D3A3BDAF08E36 p1, RuntimeObject* p2, RuntimeObject* p3, RuntimeObject* p4, RuntimeObject* p5, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3,p4,p5};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_844 (RuntimeObject* __this, float p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_845 (RuntimeObject* __this, float p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_845 (float p1, uint8_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_846 (RuntimeObject* __this, float p1, int32_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_847 (RuntimeObject* __this, float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_847 (float p1, float p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_848 (RuntimeObject* __this, float p1, float p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_849 (RuntimeObject* __this, float p1, float p2, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_850 (RuntimeObject* __this, float p1, float p2, float p3, float p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_851 (RuntimeObject* __this, float p1, float p2, float p3, float p4, float p5, uint8_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_851 (float p1, float p2, float p3, float p4, float p5, uint8_t p6, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4,&p5,&p6};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_852 (RuntimeObject* __this, StartDragArgs_tF1E3C0A058F6E7B936541CFCCFB42965A2B452C9 p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_853 (RuntimeObject* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_853 (void* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_853 (StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_854 (RuntimeObject* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_854 (void* __this, StreamingContext_t56760522A751890146EE45F82F866B55B7E33677 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_855 (RuntimeObject* __this, StyleColor_tFC32BA34A15742AC48D6AACF8A137A6F71F04910 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_856 (RuntimeObject* __this, StyleFloat_t4A100BCCDC275C2302517C5858C9BE9EC43D4841 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_857 (RuntimeObject* __this, StyleFont_t9D8A6F3E224B60FD8BA1522CE8AB0E2E8BE8B77C p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_858 (RuntimeObject* __this, StyleFontDefinition_t0E1130277B322724A677D489018D219F014070F4 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_859 (RuntimeObject* __this, StyleLength_tF02B24735FC88BE29BEB36F7A87709CA28AF72D8 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_860 (RuntimeObject* __this, StyleTranslate_tF9528CA4B45EE4EB2C4D294336A83D88DB6AF089 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_861 (RuntimeObject* __this, TimeSpan_t8195C5B013A2C532FEBDF0B64B6911982E750F5A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_862 (RuntimeObject* __this, TimerState_t82C7C29B095D6ACDC06AC172C269E9D5F0508ECE p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_863 (RuntimeObject* __this, TypedReference_tF20A82297BED597FD80BDA0E41F74746B0FD642B p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_864 (RuntimeObject* __this, uint16_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_865 (RuntimeObject* __this, uint16_t p1, uint16_t p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_866 (RuntimeObject* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_866 (void* __this, uint32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_867 (RuntimeObject* __this, uint32_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_867 (uint32_t p1, int32_t p2, RuntimeObject* p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,p4};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_868 (RuntimeObject* __this, uint32_t p1, uint32_t p2, uint16_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_868 (uint32_t p1, uint32_t p2, uint16_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_869 (RuntimeObject* __this, uint64_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_870 (RuntimeObject* __this, uint64_t p1, HmdRect2_t_tAF394D41DC1EEC399E9D2B45C173C3504AA23C74 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedStaticCall_870 (uint64_t p1, HmdRect2_t_tAF394D41DC1EEC399E9D2B45C173C3504AA23C74 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, NULL);
}
static  void UnresolvedVirtualCall_871 (RuntimeObject* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_872 (RuntimeObject* __this, Vector2Int_t69B2886EBAB732D9B880565E18E7568F3DE0CE6A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_873 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_874 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, double p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_875 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, int32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_876 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, int64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_877 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_878 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, float p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_879 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, uint32_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_880 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, int32_t p2, uint64_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_881 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 p2, uint8_t p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_882 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_883 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, RuntimeObject* p3, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_884 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, RuntimeObject* p3, float p4, float p5, float p6, float p7, Color32_t73C5004937BF5BB8AD55323D51AAA40A898EF48B p8, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3,&p4,&p5,&p6,&p7,&p8};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_885 (RuntimeObject* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p1, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p2, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_886 (RuntimeObject* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_887 (RuntimeObject* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_888 (RuntimeObject* __this, Vector3Int_t65CB06F557251D18A37BD71F3655BA836A357376 p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2,p3};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_889 (RuntimeObject* __this, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_890 (RuntimeObject* __this, RectangleParams_t0B5A63548DC33EE252AF81E242B719118C235A4B p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_891 (RuntimeObject* __this, HapticsAmplitudeEnvelopeVibration_t2EDF01B96CFAC2D09DDCAACEDEA5C2F534F252C9 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_892 (RuntimeObject* __this, Settings_tAA0A53A14DF6D83119CDFFF37A34FED8AA055A7A p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_893 (RuntimeObject* __this, EventDataBuffer_t5836E8ECE1E094863DEDCC92818AEF39C2F646E8 p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_894 (RuntimeObject* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_894 (void* __this, unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedVirtualCall_895 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, NULL);
}
static  void UnresolvedInstanceCall_895 (void* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {&p1,p2};
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, NULL);
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedStaticCall_896 (RuntimeObject* p1, RuntimeObject* p2, float p3, int32_t p4, float p5, int32_t p6, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,&p3,&p4,&p5,&p6};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedVirtualCall_897 (RuntimeObject* __this, RuntimeObject* p1, float p2, int32_t p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedStaticCall_897 (RuntimeObject* p1, float p2, int32_t p3, float p4, int32_t p5, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,&p4,&p5};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedVirtualCall_898 (RuntimeObject* __this, float p1, int32_t p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB UnresolvedInstanceCall_898 (void* __this, float p1, int32_t p2, float p3, int32_t p4, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,&p3,&p4};
	YogaSize_t3FF1BB4D590ACFC61602BA2713211CFBF1AA9DCB il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  Enumerator_t096B8EE1BCCF3F5C13983F76868C274C3FA83D63 UnresolvedVirtualCall_899 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Enumerator_t096B8EE1BCCF3F5C13983F76868C274C3FA83D63 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  MeshRendererData_t82E0EE9E624AA3082384E4FDC913E2640811BB6D UnresolvedVirtualCall_900 (RuntimeObject* __this, const RuntimeMethod* method)
{
	MeshRendererData_t82E0EE9E624AA3082384E4FDC913E2640811BB6D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Bone_tC5A0FC2629B8200D2FAA3C6E4DA8AF048617E43D UnresolvedVirtualCall_901 (RuntimeObject* __this, const RuntimeMethod* method)
{
	Bone_tC5A0FC2629B8200D2FAA3C6E4DA8AF048617E43D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Bone_tC5A0FC2629B8200D2FAA3C6E4DA8AF048617E43D UnresolvedStaticCall_901 (const RuntimeMethod* method)
{
	Bone_tC5A0FC2629B8200D2FAA3C6E4DA8AF048617E43D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  Bone_tC5A0FC2629B8200D2FAA3C6E4DA8AF048617E43D UnresolvedStaticCall_902 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	Bone_tC5A0FC2629B8200D2FAA3C6E4DA8AF048617E43D il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  SkeletonPoseData_tA0A3A4163EEA849A40C82CF95F1847A9827A5F86 UnresolvedVirtualCall_903 (RuntimeObject* __this, const RuntimeMethod* method)
{
	SkeletonPoseData_tA0A3A4163EEA849A40C82CF95F1847A9827A5F86 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  SkeletonRendererData_t2987D9652B016DADE9C92F3342186D5E620C0672 UnresolvedVirtualCall_904 (RuntimeObject* __this, const RuntimeMethod* method)
{
	SkeletonRendererData_t2987D9652B016DADE9C92F3342186D5E620C0672 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357 UnresolvedVirtualCall_905 (RuntimeObject* __this, int32_t p1, const RuntimeMethod* method)
{
	void* args[] = {&p1};
	BlitInfo_t6D4C0580BBEF65F5EAD39FB6DBC85F360CF6A357 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 UnresolvedVirtualCall_906 (RuntimeObject* __this, const RuntimeMethod* method)
{
	unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 UnresolvedStaticCall_906 (const RuntimeMethod* method)
{
	unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, NULL, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 UnresolvedStaticCall_907 (RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_errorstate_tC926EE4582920BE2C1DB1F3F65619B810D5AB902 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedVirtualCall_908 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedInstanceCall_908 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedVirtualCall_909 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedStaticCall_909 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 UnresolvedStaticCall_910 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	unitytls_key_ref_t6BD91D013DF11047C53738FEEB12CE290FDC71A2 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 UnresolvedStaticCall_911 (RuntimeObject* p1, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p2, intptr_t p3, RuntimeObject* p4, const RuntimeMethod* method)
{
	void* args[] = {p1,&p2,&p3,p4};
	unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 UnresolvedVirtualCall_912 (RuntimeObject* __this, unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 UnresolvedStaticCall_912 (unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 p1, intptr_t p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {&p1,&p2,p3};
	unitytls_x509_ref_t9CEB17766B4144117333AB50379B21A357FA4333 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedVirtualCall_913 (RuntimeObject* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedInstanceCall_913 (void* __this, RuntimeObject* p1, const RuntimeMethod* method)
{
	void* args[] = {p1};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedVirtualCall_914 (RuntimeObject* __this, RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_method_pointer(method), method, __this, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedStaticCall_914 (RuntimeObject* p1, RuntimeObject* p2, const RuntimeMethod* method)
{
	void* args[] = {p1,p2};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
static  unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 UnresolvedStaticCall_915 (RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, const RuntimeMethod* method)
{
	void* args[] = {p1,p2,p3};
	unitytls_x509list_ref_t6C5C1CF0B720516A681CB741104A164FD8B3CF17 il2cppRetVal;
	method->invoker_method(il2cpp_codegen_get_direct_method_pointer(method), method, NULL, args, &il2cppRetVal);
	return il2cppRetVal;
}
IL2CPP_EXTERN_C const Il2CppMethodPointer g_UnresolvedVirtualMethodPointers[];
const Il2CppMethodPointer g_UnresolvedVirtualMethodPointers[916] = 
{
	(const Il2CppMethodPointer)UnresolvedVirtualCall_0,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_1,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_2,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_3,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_4,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_5,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_6,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_7,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_8,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_9,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_10,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_11,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_12,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_13,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_14,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_15,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_16,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_17,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_18,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_19,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_20,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_21,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_22,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_23,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_24,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_25,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_26,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_27,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_28,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_29,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_30,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_31,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_32,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_33,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_34,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_35,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_36,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_37,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_38,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_39,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_40,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_41,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_42,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_43,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_44,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_45,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_46,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_47,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_48,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_49,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_50,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_51,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_52,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_53,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_54,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_55,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_56,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_57,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_58,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_59,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_60,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_61,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_62,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_63,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_64,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_65,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_66,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_67,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_68,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_69,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_70,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_71,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_72,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_73,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_74,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_75,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_76,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_77,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_79,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_81,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_82,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_83,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_84,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_85,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_86,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_87,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_88,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_89,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_90,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_91,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_92,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_93,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_95,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_96,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_97,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_99,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_100,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_101,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_103,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_104,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_105,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_107,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_108,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_109,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_110,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_111,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_113,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_115,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_118,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_119,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_120,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_122,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_127,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_129,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_130,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_131,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_132,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_133,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_134,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_135,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_136,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_137,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_138,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_139,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_140,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_141,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_142,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_143,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_144,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_145,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_146,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_147,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_148,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_149,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_150,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_151,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_152,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_153,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_154,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_155,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_156,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_157,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_158,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_159,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_160,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_161,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_162,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_163,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_164,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_165,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_166,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_167,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_168,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_169,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_170,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_171,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_172,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_173,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_174,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_175,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_176,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_177,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_178,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_179,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_180,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_181,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_183,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_185,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_186,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_190,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_191,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_193,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_194,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_195,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_196,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_197,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_198,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_199,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_200,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_201,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_202,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_203,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_204,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_205,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_206,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_207,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_208,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_209,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_210,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_211,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_212,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_213,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_214,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_215,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_216,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_217,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_218,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_219,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_220,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_221,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_222,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_223,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_224,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_225,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_226,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_227,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_228,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_229,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_230,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_231,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_232,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_233,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_234,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_236,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_239,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_240,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_241,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_242,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_244,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_246,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_251,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_253,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_254,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_255,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_256,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_260,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_261,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_262,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_263,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_266,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_268,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_273,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_274,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_281,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_283,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_284,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_322,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_323,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_324,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_325,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_326,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_327,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_328,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_329,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_330,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_331,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_332,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_333,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_334,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_335,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_336,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_337,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_338,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_339,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_340,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_341,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_342,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_343,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_344,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_345,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_346,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_347,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_348,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_349,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_350,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_351,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_352,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_353,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_354,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_355,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_356,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_357,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_358,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_359,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_360,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_361,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_362,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_363,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_364,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_365,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_366,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_367,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_368,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_369,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_370,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_371,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_372,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_373,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_374,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_375,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_376,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_377,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_378,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_379,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_380,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_381,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_382,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_383,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_384,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_385,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_386,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_389,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_390,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_391,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_394,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_395,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_396,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_398,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_399,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_400,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_401,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_402,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_403,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_404,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_405,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_406,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_407,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_408,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_409,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_410,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_411,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_412,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_413,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_414,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_415,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_416,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_417,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_418,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_419,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_420,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_421,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_422,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_423,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_424,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_425,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_426,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_427,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_428,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_429,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_430,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_431,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_432,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_433,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_434,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_435,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_436,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_437,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_438,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_439,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_440,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_441,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_442,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_443,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_444,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_445,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_446,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_447,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_448,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_449,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_450,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_452,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_453,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_454,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_456,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_457,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_458,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_459,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_460,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_461,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_462,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_463,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_464,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_465,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_466,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_467,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_468,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_469,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_472,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_473,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_475,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_476,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_481,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_482,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_483,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_484,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_485,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_486,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_487,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_488,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_489,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_490,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_491,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_492,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_493,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_494,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_495,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_496,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_497,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_499,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_500,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_501,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_502,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_503,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_505,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_506,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_507,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_508,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_509,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_510,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_512,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_513,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_514,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_515,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_516,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_517,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_518,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_519,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_520,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_521,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_522,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_523,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_524,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_525,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_526,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_527,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_528,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_531,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_534,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_535,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_536,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_537,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_538,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_539,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_540,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_541,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_542,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_543,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_544,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_545,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_546,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_547,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_548,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_549,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_550,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_551,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_552,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_554,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_555,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_556,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_557,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_558,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_559,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_560,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_561,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_562,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_564,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_565,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_566,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_567,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_568,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_569,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_570,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_571,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_573,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_576,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_580,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_582,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_583,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_587,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_590,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_592,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_596,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_597,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_600,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_601,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_602,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_603,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_604,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_605,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_606,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_607,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_608,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_609,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_610,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_611,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_612,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_613,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_614,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_615,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_616,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_621,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_622,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_623,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_624,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_625,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_626,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_627,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_628,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_630,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_631,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_632,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_634,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_635,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_636,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_637,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_638,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_639,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_640,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_641,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_642,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_643,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_644,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_645,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_646,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_647,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_648,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_649,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_650,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_651,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_652,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_653,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_654,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_655,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_656,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_657,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_658,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_659,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_660,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_661,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_662,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_663,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_664,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_665,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_666,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_667,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_668,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_669,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_670,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_671,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_672,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_673,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_674,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_675,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_676,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_677,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_678,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_679,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_680,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_681,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_682,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_683,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_684,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_685,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_686,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_687,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_688,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_689,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_690,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_691,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_692,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_693,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_694,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_695,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_696,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_697,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_698,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_699,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_700,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_701,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_702,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_703,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_704,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_705,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_706,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_707,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_708,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_709,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_710,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_711,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_712,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_713,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_714,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_715,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_716,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_717,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_718,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_720,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_721,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_722,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_723,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_724,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_725,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_727,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_730,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_731,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_732,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_733,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_734,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_736,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_738,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_739,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_741,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_743,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_745,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_746,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_747,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_748,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_750,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_752,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_753,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_755,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_756,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_757,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_759,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_761,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_762,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_763,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_766,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_768,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_769,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_770,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_772,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_774,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_776,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_777,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_778,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_781,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_782,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_783,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_784,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_785,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_786,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_787,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_788,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_789,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_790,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_791,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_792,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_797,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_798,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_799,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_800,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_801,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_803,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_805,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_809,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_810,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_811,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_812,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_814,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_816,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_819,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_820,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_821,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_825,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_826,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_827,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_828,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_829,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_830,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_831,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_832,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_833,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_834,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_835,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_836,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_837,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_838,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_839,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_840,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_841,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_842,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_843,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_844,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_845,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_846,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_847,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_848,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_849,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_850,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_851,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_852,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_853,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_854,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_855,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_856,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_857,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_858,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_859,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_860,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_861,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_862,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_863,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_864,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_865,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_866,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_867,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_868,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_869,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_870,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_871,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_872,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_873,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_874,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_875,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_876,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_877,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_878,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_879,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_880,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_881,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_882,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_883,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_884,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_885,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_886,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_887,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_888,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_889,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_890,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_891,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_892,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_893,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_894,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_895,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_897,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_898,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_899,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_900,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_901,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_903,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_904,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_905,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_906,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_908,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_909,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_912,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_913,
	(const Il2CppMethodPointer)UnresolvedVirtualCall_914,
	NULL,
};
IL2CPP_EXTERN_C const Il2CppMethodPointer g_UnresolvedInstanceMethodPointers[];
const Il2CppMethodPointer g_UnresolvedInstanceMethodPointers[916] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_29,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_44,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_61,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_76,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_103,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_105,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_111,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_122,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_127,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_140,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_195,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_200,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_206,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_207,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_210,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_215,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_217,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_224,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_251,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_260,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_262,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_268,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_324,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_325,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_332,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_334,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_335,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_390,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_395,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_403,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_417,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_428,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_429,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_434,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_444,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_452,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_456,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_457,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_459,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_460,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_461,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_462,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_463,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_464,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_465,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_466,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_467,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_468,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_475,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_502,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_527,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_536,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_551,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_560,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_566,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_568,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_571,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_582,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_587,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_590,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_600,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_604,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_608,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_609,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_612,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_613,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_614,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_638,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_639,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_663,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_665,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_676,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_684,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_687,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_697,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_699,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_706,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_709,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_712,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_713,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_717,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_724,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_730,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_738,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_746,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_748,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_766,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_769,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_770,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_777,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_781,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_782,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_783,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_784,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_785,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_786,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_787,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_788,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_789,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_790,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_791,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_805,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_810,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_816,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_839,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_853,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_854,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_866,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_894,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_895,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_898,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_908,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedInstanceCall_913,
	NULL,
	NULL,
};
IL2CPP_EXTERN_C const Il2CppMethodPointer g_UnresolvedStaticMethodPointers[];
const Il2CppMethodPointer g_UnresolvedStaticMethodPointers[916] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_29,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_36,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_39,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_42,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_44,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_64,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_69,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_73,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_76,
	(const Il2CppMethodPointer)UnresolvedStaticCall_77,
	(const Il2CppMethodPointer)UnresolvedStaticCall_78,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_80,
	(const Il2CppMethodPointer)UnresolvedStaticCall_81,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_91,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_94,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_98,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_102,
	(const Il2CppMethodPointer)UnresolvedStaticCall_103,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_105,
	(const Il2CppMethodPointer)UnresolvedStaticCall_106,
	(const Il2CppMethodPointer)UnresolvedStaticCall_107,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_109,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_111,
	(const Il2CppMethodPointer)UnresolvedStaticCall_112,
	(const Il2CppMethodPointer)UnresolvedStaticCall_113,
	(const Il2CppMethodPointer)UnresolvedStaticCall_114,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_116,
	(const Il2CppMethodPointer)UnresolvedStaticCall_117,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_119,
	(const Il2CppMethodPointer)UnresolvedStaticCall_120,
	(const Il2CppMethodPointer)UnresolvedStaticCall_121,
	(const Il2CppMethodPointer)UnresolvedStaticCall_122,
	(const Il2CppMethodPointer)UnresolvedStaticCall_123,
	(const Il2CppMethodPointer)UnresolvedStaticCall_124,
	(const Il2CppMethodPointer)UnresolvedStaticCall_125,
	(const Il2CppMethodPointer)UnresolvedStaticCall_126,
	(const Il2CppMethodPointer)UnresolvedStaticCall_127,
	(const Il2CppMethodPointer)UnresolvedStaticCall_128,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_131,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_140,
	(const Il2CppMethodPointer)UnresolvedStaticCall_141,
	(const Il2CppMethodPointer)UnresolvedStaticCall_142,
	(const Il2CppMethodPointer)UnresolvedStaticCall_143,
	(const Il2CppMethodPointer)UnresolvedStaticCall_144,
	(const Il2CppMethodPointer)UnresolvedStaticCall_145,
	(const Il2CppMethodPointer)UnresolvedStaticCall_146,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_181,
	(const Il2CppMethodPointer)UnresolvedStaticCall_182,
	(const Il2CppMethodPointer)UnresolvedStaticCall_183,
	(const Il2CppMethodPointer)UnresolvedStaticCall_184,
	(const Il2CppMethodPointer)UnresolvedStaticCall_185,
	(const Il2CppMethodPointer)UnresolvedStaticCall_186,
	(const Il2CppMethodPointer)UnresolvedStaticCall_187,
	(const Il2CppMethodPointer)UnresolvedStaticCall_188,
	(const Il2CppMethodPointer)UnresolvedStaticCall_189,
	(const Il2CppMethodPointer)UnresolvedStaticCall_190,
	(const Il2CppMethodPointer)UnresolvedStaticCall_191,
	(const Il2CppMethodPointer)UnresolvedStaticCall_192,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_195,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_209,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_211,
	(const Il2CppMethodPointer)UnresolvedStaticCall_212,
	(const Il2CppMethodPointer)UnresolvedStaticCall_213,
	(const Il2CppMethodPointer)UnresolvedStaticCall_214,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_216,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_218,
	(const Il2CppMethodPointer)UnresolvedStaticCall_219,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_222,
	(const Il2CppMethodPointer)UnresolvedStaticCall_223,
	(const Il2CppMethodPointer)UnresolvedStaticCall_224,
	(const Il2CppMethodPointer)UnresolvedStaticCall_225,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_228,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_230,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_235,
	(const Il2CppMethodPointer)UnresolvedStaticCall_236,
	(const Il2CppMethodPointer)UnresolvedStaticCall_237,
	(const Il2CppMethodPointer)UnresolvedStaticCall_238,
	(const Il2CppMethodPointer)UnresolvedStaticCall_239,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_243,
	(const Il2CppMethodPointer)UnresolvedStaticCall_244,
	(const Il2CppMethodPointer)UnresolvedStaticCall_245,
	(const Il2CppMethodPointer)UnresolvedStaticCall_246,
	(const Il2CppMethodPointer)UnresolvedStaticCall_247,
	(const Il2CppMethodPointer)UnresolvedStaticCall_248,
	(const Il2CppMethodPointer)UnresolvedStaticCall_249,
	(const Il2CppMethodPointer)UnresolvedStaticCall_250,
	(const Il2CppMethodPointer)UnresolvedStaticCall_251,
	(const Il2CppMethodPointer)UnresolvedStaticCall_252,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_254,
	(const Il2CppMethodPointer)UnresolvedStaticCall_255,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_257,
	(const Il2CppMethodPointer)UnresolvedStaticCall_258,
	(const Il2CppMethodPointer)UnresolvedStaticCall_259,
	(const Il2CppMethodPointer)UnresolvedStaticCall_260,
	(const Il2CppMethodPointer)UnresolvedStaticCall_261,
	(const Il2CppMethodPointer)UnresolvedStaticCall_262,
	(const Il2CppMethodPointer)UnresolvedStaticCall_263,
	(const Il2CppMethodPointer)UnresolvedStaticCall_264,
	(const Il2CppMethodPointer)UnresolvedStaticCall_265,
	(const Il2CppMethodPointer)UnresolvedStaticCall_266,
	(const Il2CppMethodPointer)UnresolvedStaticCall_267,
	(const Il2CppMethodPointer)UnresolvedStaticCall_268,
	(const Il2CppMethodPointer)UnresolvedStaticCall_269,
	(const Il2CppMethodPointer)UnresolvedStaticCall_270,
	(const Il2CppMethodPointer)UnresolvedStaticCall_271,
	(const Il2CppMethodPointer)UnresolvedStaticCall_272,
	(const Il2CppMethodPointer)UnresolvedStaticCall_273,
	(const Il2CppMethodPointer)UnresolvedStaticCall_274,
	(const Il2CppMethodPointer)UnresolvedStaticCall_275,
	(const Il2CppMethodPointer)UnresolvedStaticCall_276,
	(const Il2CppMethodPointer)UnresolvedStaticCall_277,
	(const Il2CppMethodPointer)UnresolvedStaticCall_278,
	(const Il2CppMethodPointer)UnresolvedStaticCall_279,
	(const Il2CppMethodPointer)UnresolvedStaticCall_280,
	(const Il2CppMethodPointer)UnresolvedStaticCall_281,
	(const Il2CppMethodPointer)UnresolvedStaticCall_282,
	(const Il2CppMethodPointer)UnresolvedStaticCall_283,
	(const Il2CppMethodPointer)UnresolvedStaticCall_284,
	(const Il2CppMethodPointer)UnresolvedStaticCall_285,
	(const Il2CppMethodPointer)UnresolvedStaticCall_286,
	(const Il2CppMethodPointer)UnresolvedStaticCall_287,
	(const Il2CppMethodPointer)UnresolvedStaticCall_288,
	(const Il2CppMethodPointer)UnresolvedStaticCall_289,
	(const Il2CppMethodPointer)UnresolvedStaticCall_290,
	(const Il2CppMethodPointer)UnresolvedStaticCall_291,
	(const Il2CppMethodPointer)UnresolvedStaticCall_292,
	(const Il2CppMethodPointer)UnresolvedStaticCall_293,
	(const Il2CppMethodPointer)UnresolvedStaticCall_294,
	(const Il2CppMethodPointer)UnresolvedStaticCall_295,
	(const Il2CppMethodPointer)UnresolvedStaticCall_296,
	(const Il2CppMethodPointer)UnresolvedStaticCall_297,
	(const Il2CppMethodPointer)UnresolvedStaticCall_298,
	(const Il2CppMethodPointer)UnresolvedStaticCall_299,
	(const Il2CppMethodPointer)UnresolvedStaticCall_300,
	(const Il2CppMethodPointer)UnresolvedStaticCall_301,
	(const Il2CppMethodPointer)UnresolvedStaticCall_302,
	(const Il2CppMethodPointer)UnresolvedStaticCall_303,
	(const Il2CppMethodPointer)UnresolvedStaticCall_304,
	(const Il2CppMethodPointer)UnresolvedStaticCall_305,
	(const Il2CppMethodPointer)UnresolvedStaticCall_306,
	(const Il2CppMethodPointer)UnresolvedStaticCall_307,
	(const Il2CppMethodPointer)UnresolvedStaticCall_308,
	(const Il2CppMethodPointer)UnresolvedStaticCall_309,
	(const Il2CppMethodPointer)UnresolvedStaticCall_310,
	(const Il2CppMethodPointer)UnresolvedStaticCall_311,
	(const Il2CppMethodPointer)UnresolvedStaticCall_312,
	(const Il2CppMethodPointer)UnresolvedStaticCall_313,
	(const Il2CppMethodPointer)UnresolvedStaticCall_314,
	(const Il2CppMethodPointer)UnresolvedStaticCall_315,
	(const Il2CppMethodPointer)UnresolvedStaticCall_316,
	(const Il2CppMethodPointer)UnresolvedStaticCall_317,
	(const Il2CppMethodPointer)UnresolvedStaticCall_318,
	(const Il2CppMethodPointer)UnresolvedStaticCall_319,
	(const Il2CppMethodPointer)UnresolvedStaticCall_320,
	(const Il2CppMethodPointer)UnresolvedStaticCall_321,
	(const Il2CppMethodPointer)UnresolvedStaticCall_322,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_325,
	(const Il2CppMethodPointer)UnresolvedStaticCall_326,
	(const Il2CppMethodPointer)UnresolvedStaticCall_327,
	(const Il2CppMethodPointer)UnresolvedStaticCall_328,
	(const Il2CppMethodPointer)UnresolvedStaticCall_329,
	(const Il2CppMethodPointer)UnresolvedStaticCall_330,
	(const Il2CppMethodPointer)UnresolvedStaticCall_331,
	(const Il2CppMethodPointer)UnresolvedStaticCall_332,
	(const Il2CppMethodPointer)UnresolvedStaticCall_333,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_336,
	(const Il2CppMethodPointer)UnresolvedStaticCall_337,
	(const Il2CppMethodPointer)UnresolvedStaticCall_338,
	(const Il2CppMethodPointer)UnresolvedStaticCall_339,
	(const Il2CppMethodPointer)UnresolvedStaticCall_340,
	(const Il2CppMethodPointer)UnresolvedStaticCall_341,
	(const Il2CppMethodPointer)UnresolvedStaticCall_342,
	(const Il2CppMethodPointer)UnresolvedStaticCall_343,
	(const Il2CppMethodPointer)UnresolvedStaticCall_344,
	(const Il2CppMethodPointer)UnresolvedStaticCall_345,
	(const Il2CppMethodPointer)UnresolvedStaticCall_346,
	(const Il2CppMethodPointer)UnresolvedStaticCall_347,
	(const Il2CppMethodPointer)UnresolvedStaticCall_348,
	(const Il2CppMethodPointer)UnresolvedStaticCall_349,
	(const Il2CppMethodPointer)UnresolvedStaticCall_350,
	(const Il2CppMethodPointer)UnresolvedStaticCall_351,
	(const Il2CppMethodPointer)UnresolvedStaticCall_352,
	(const Il2CppMethodPointer)UnresolvedStaticCall_353,
	(const Il2CppMethodPointer)UnresolvedStaticCall_354,
	(const Il2CppMethodPointer)UnresolvedStaticCall_355,
	(const Il2CppMethodPointer)UnresolvedStaticCall_356,
	(const Il2CppMethodPointer)UnresolvedStaticCall_357,
	(const Il2CppMethodPointer)UnresolvedStaticCall_358,
	(const Il2CppMethodPointer)UnresolvedStaticCall_359,
	(const Il2CppMethodPointer)UnresolvedStaticCall_360,
	(const Il2CppMethodPointer)UnresolvedStaticCall_361,
	(const Il2CppMethodPointer)UnresolvedStaticCall_362,
	(const Il2CppMethodPointer)UnresolvedStaticCall_363,
	(const Il2CppMethodPointer)UnresolvedStaticCall_364,
	(const Il2CppMethodPointer)UnresolvedStaticCall_365,
	(const Il2CppMethodPointer)UnresolvedStaticCall_366,
	(const Il2CppMethodPointer)UnresolvedStaticCall_367,
	(const Il2CppMethodPointer)UnresolvedStaticCall_368,
	(const Il2CppMethodPointer)UnresolvedStaticCall_369,
	(const Il2CppMethodPointer)UnresolvedStaticCall_370,
	(const Il2CppMethodPointer)UnresolvedStaticCall_371,
	(const Il2CppMethodPointer)UnresolvedStaticCall_372,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_375,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_381,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_385,
	(const Il2CppMethodPointer)UnresolvedStaticCall_386,
	(const Il2CppMethodPointer)UnresolvedStaticCall_387,
	(const Il2CppMethodPointer)UnresolvedStaticCall_388,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_391,
	(const Il2CppMethodPointer)UnresolvedStaticCall_392,
	(const Il2CppMethodPointer)UnresolvedStaticCall_393,
	(const Il2CppMethodPointer)UnresolvedStaticCall_394,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_396,
	(const Il2CppMethodPointer)UnresolvedStaticCall_397,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_403,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_417,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_429,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_434,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_443,
	(const Il2CppMethodPointer)UnresolvedStaticCall_444,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_447,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_451,
	(const Il2CppMethodPointer)UnresolvedStaticCall_452,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_455,
	(const Il2CppMethodPointer)UnresolvedStaticCall_456,
	(const Il2CppMethodPointer)UnresolvedStaticCall_457,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_459,
	(const Il2CppMethodPointer)UnresolvedStaticCall_460,
	(const Il2CppMethodPointer)UnresolvedStaticCall_461,
	(const Il2CppMethodPointer)UnresolvedStaticCall_462,
	(const Il2CppMethodPointer)UnresolvedStaticCall_463,
	(const Il2CppMethodPointer)UnresolvedStaticCall_464,
	(const Il2CppMethodPointer)UnresolvedStaticCall_465,
	(const Il2CppMethodPointer)UnresolvedStaticCall_466,
	(const Il2CppMethodPointer)UnresolvedStaticCall_467,
	(const Il2CppMethodPointer)UnresolvedStaticCall_468,
	(const Il2CppMethodPointer)UnresolvedStaticCall_469,
	(const Il2CppMethodPointer)UnresolvedStaticCall_470,
	(const Il2CppMethodPointer)UnresolvedStaticCall_471,
	(const Il2CppMethodPointer)UnresolvedStaticCall_472,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_474,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_477,
	(const Il2CppMethodPointer)UnresolvedStaticCall_478,
	(const Il2CppMethodPointer)UnresolvedStaticCall_479,
	(const Il2CppMethodPointer)UnresolvedStaticCall_480,
	(const Il2CppMethodPointer)UnresolvedStaticCall_481,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_489,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_491,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_494,
	(const Il2CppMethodPointer)UnresolvedStaticCall_495,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_498,
	(const Il2CppMethodPointer)UnresolvedStaticCall_499,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_503,
	(const Il2CppMethodPointer)UnresolvedStaticCall_504,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_511,
	(const Il2CppMethodPointer)UnresolvedStaticCall_512,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_521,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_525,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_528,
	(const Il2CppMethodPointer)UnresolvedStaticCall_529,
	(const Il2CppMethodPointer)UnresolvedStaticCall_530,
	(const Il2CppMethodPointer)UnresolvedStaticCall_531,
	(const Il2CppMethodPointer)UnresolvedStaticCall_532,
	(const Il2CppMethodPointer)UnresolvedStaticCall_533,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_537,
	(const Il2CppMethodPointer)UnresolvedStaticCall_538,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_552,
	(const Il2CppMethodPointer)UnresolvedStaticCall_553,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_562,
	(const Il2CppMethodPointer)UnresolvedStaticCall_563,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_566,
	(const Il2CppMethodPointer)UnresolvedStaticCall_567,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_569,
	(const Il2CppMethodPointer)UnresolvedStaticCall_570,
	(const Il2CppMethodPointer)UnresolvedStaticCall_571,
	(const Il2CppMethodPointer)UnresolvedStaticCall_572,
	(const Il2CppMethodPointer)UnresolvedStaticCall_573,
	(const Il2CppMethodPointer)UnresolvedStaticCall_574,
	(const Il2CppMethodPointer)UnresolvedStaticCall_575,
	(const Il2CppMethodPointer)UnresolvedStaticCall_576,
	(const Il2CppMethodPointer)UnresolvedStaticCall_577,
	(const Il2CppMethodPointer)UnresolvedStaticCall_578,
	(const Il2CppMethodPointer)UnresolvedStaticCall_579,
	(const Il2CppMethodPointer)UnresolvedStaticCall_580,
	(const Il2CppMethodPointer)UnresolvedStaticCall_581,
	(const Il2CppMethodPointer)UnresolvedStaticCall_582,
	(const Il2CppMethodPointer)UnresolvedStaticCall_583,
	(const Il2CppMethodPointer)UnresolvedStaticCall_584,
	(const Il2CppMethodPointer)UnresolvedStaticCall_585,
	(const Il2CppMethodPointer)UnresolvedStaticCall_586,
	(const Il2CppMethodPointer)UnresolvedStaticCall_587,
	(const Il2CppMethodPointer)UnresolvedStaticCall_588,
	(const Il2CppMethodPointer)UnresolvedStaticCall_589,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_591,
	(const Il2CppMethodPointer)UnresolvedStaticCall_592,
	(const Il2CppMethodPointer)UnresolvedStaticCall_593,
	(const Il2CppMethodPointer)UnresolvedStaticCall_594,
	(const Il2CppMethodPointer)UnresolvedStaticCall_595,
	(const Il2CppMethodPointer)UnresolvedStaticCall_596,
	(const Il2CppMethodPointer)UnresolvedStaticCall_597,
	(const Il2CppMethodPointer)UnresolvedStaticCall_598,
	(const Il2CppMethodPointer)UnresolvedStaticCall_599,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_601,
	(const Il2CppMethodPointer)UnresolvedStaticCall_602,
	(const Il2CppMethodPointer)UnresolvedStaticCall_603,
	(const Il2CppMethodPointer)UnresolvedStaticCall_604,
	(const Il2CppMethodPointer)UnresolvedStaticCall_605,
	(const Il2CppMethodPointer)UnresolvedStaticCall_606,
	(const Il2CppMethodPointer)UnresolvedStaticCall_607,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_610,
	(const Il2CppMethodPointer)UnresolvedStaticCall_611,
	(const Il2CppMethodPointer)UnresolvedStaticCall_612,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_614,
	(const Il2CppMethodPointer)UnresolvedStaticCall_615,
	(const Il2CppMethodPointer)UnresolvedStaticCall_616,
	(const Il2CppMethodPointer)UnresolvedStaticCall_617,
	(const Il2CppMethodPointer)UnresolvedStaticCall_618,
	(const Il2CppMethodPointer)UnresolvedStaticCall_619,
	(const Il2CppMethodPointer)UnresolvedStaticCall_620,
	(const Il2CppMethodPointer)UnresolvedStaticCall_621,
	(const Il2CppMethodPointer)UnresolvedStaticCall_622,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_629,
	(const Il2CppMethodPointer)UnresolvedStaticCall_630,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_633,
	(const Il2CppMethodPointer)UnresolvedStaticCall_634,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_638,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_659,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_663,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_670,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_679,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_682,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_684,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_696,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_698,
	(const Il2CppMethodPointer)UnresolvedStaticCall_699,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_704,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_707,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_711,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_717,
	(const Il2CppMethodPointer)UnresolvedStaticCall_718,
	(const Il2CppMethodPointer)UnresolvedStaticCall_719,
	(const Il2CppMethodPointer)UnresolvedStaticCall_720,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_723,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_726,
	(const Il2CppMethodPointer)UnresolvedStaticCall_727,
	(const Il2CppMethodPointer)UnresolvedStaticCall_728,
	(const Il2CppMethodPointer)UnresolvedStaticCall_729,
	(const Il2CppMethodPointer)UnresolvedStaticCall_730,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_732,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_735,
	(const Il2CppMethodPointer)UnresolvedStaticCall_736,
	(const Il2CppMethodPointer)UnresolvedStaticCall_737,
	(const Il2CppMethodPointer)UnresolvedStaticCall_738,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_740,
	(const Il2CppMethodPointer)UnresolvedStaticCall_741,
	(const Il2CppMethodPointer)UnresolvedStaticCall_742,
	(const Il2CppMethodPointer)UnresolvedStaticCall_743,
	(const Il2CppMethodPointer)UnresolvedStaticCall_744,
	(const Il2CppMethodPointer)UnresolvedStaticCall_745,
	(const Il2CppMethodPointer)UnresolvedStaticCall_746,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_748,
	(const Il2CppMethodPointer)UnresolvedStaticCall_749,
	(const Il2CppMethodPointer)UnresolvedStaticCall_750,
	(const Il2CppMethodPointer)UnresolvedStaticCall_751,
	(const Il2CppMethodPointer)UnresolvedStaticCall_752,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_754,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_756,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_758,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_760,
	(const Il2CppMethodPointer)UnresolvedStaticCall_761,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_764,
	(const Il2CppMethodPointer)UnresolvedStaticCall_765,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_767,
	(const Il2CppMethodPointer)UnresolvedStaticCall_768,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_770,
	(const Il2CppMethodPointer)UnresolvedStaticCall_771,
	(const Il2CppMethodPointer)UnresolvedStaticCall_772,
	(const Il2CppMethodPointer)UnresolvedStaticCall_773,
	(const Il2CppMethodPointer)UnresolvedStaticCall_774,
	(const Il2CppMethodPointer)UnresolvedStaticCall_775,
	(const Il2CppMethodPointer)UnresolvedStaticCall_776,
	(const Il2CppMethodPointer)UnresolvedStaticCall_777,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_779,
	(const Il2CppMethodPointer)UnresolvedStaticCall_780,
	(const Il2CppMethodPointer)UnresolvedStaticCall_781,
	(const Il2CppMethodPointer)UnresolvedStaticCall_782,
	(const Il2CppMethodPointer)UnresolvedStaticCall_783,
	(const Il2CppMethodPointer)UnresolvedStaticCall_784,
	(const Il2CppMethodPointer)UnresolvedStaticCall_785,
	(const Il2CppMethodPointer)UnresolvedStaticCall_786,
	(const Il2CppMethodPointer)UnresolvedStaticCall_787,
	(const Il2CppMethodPointer)UnresolvedStaticCall_788,
	(const Il2CppMethodPointer)UnresolvedStaticCall_789,
	(const Il2CppMethodPointer)UnresolvedStaticCall_790,
	(const Il2CppMethodPointer)UnresolvedStaticCall_791,
	(const Il2CppMethodPointer)UnresolvedStaticCall_792,
	(const Il2CppMethodPointer)UnresolvedStaticCall_793,
	(const Il2CppMethodPointer)UnresolvedStaticCall_794,
	(const Il2CppMethodPointer)UnresolvedStaticCall_795,
	(const Il2CppMethodPointer)UnresolvedStaticCall_796,
	(const Il2CppMethodPointer)UnresolvedStaticCall_797,
	(const Il2CppMethodPointer)UnresolvedStaticCall_798,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_800,
	(const Il2CppMethodPointer)UnresolvedStaticCall_801,
	(const Il2CppMethodPointer)UnresolvedStaticCall_802,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_804,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_806,
	(const Il2CppMethodPointer)UnresolvedStaticCall_807,
	(const Il2CppMethodPointer)UnresolvedStaticCall_808,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_810,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_813,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_815,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_817,
	(const Il2CppMethodPointer)UnresolvedStaticCall_818,
	(const Il2CppMethodPointer)UnresolvedStaticCall_819,
	(const Il2CppMethodPointer)UnresolvedStaticCall_820,
	(const Il2CppMethodPointer)UnresolvedStaticCall_821,
	(const Il2CppMethodPointer)UnresolvedStaticCall_822,
	(const Il2CppMethodPointer)UnresolvedStaticCall_823,
	(const Il2CppMethodPointer)UnresolvedStaticCall_824,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_829,
	(const Il2CppMethodPointer)UnresolvedStaticCall_830,
	(const Il2CppMethodPointer)UnresolvedStaticCall_831,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_843,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_845,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_847,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_851,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_853,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_867,
	(const Il2CppMethodPointer)UnresolvedStaticCall_868,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_870,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_896,
	(const Il2CppMethodPointer)UnresolvedStaticCall_897,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_901,
	(const Il2CppMethodPointer)UnresolvedStaticCall_902,
	NULL,
	NULL,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_906,
	(const Il2CppMethodPointer)UnresolvedStaticCall_907,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_909,
	(const Il2CppMethodPointer)UnresolvedStaticCall_910,
	(const Il2CppMethodPointer)UnresolvedStaticCall_911,
	(const Il2CppMethodPointer)UnresolvedStaticCall_912,
	NULL,
	(const Il2CppMethodPointer)UnresolvedStaticCall_914,
	(const Il2CppMethodPointer)UnresolvedStaticCall_915,
};
