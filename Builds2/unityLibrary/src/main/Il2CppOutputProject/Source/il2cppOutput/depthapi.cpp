﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>



struct Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C;
struct Action_1_t88CC03E8C305DA991BBBCEBE79519B58D52F577F;
struct Action_1_t10D7C827ADC73ED438E0CA8F04465BA6F2BAED7D;
struct Action_1_t923A20D1D4F6B55B2ED5AE21B90F1A0CE0450D99;
struct List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B;
struct List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D;
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
struct RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A;
struct __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC;
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA;
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
struct CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B;
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
struct Delegate_t;
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
struct EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76;
struct EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0;
struct HDROutputSettings_t6A590B1AA325DD7389D71F502B762BF1592A9F62;
struct ISubsystemDescriptor_tEF29944D579CC7D70F52CB883150735991D54E6E;
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3;
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
struct MethodInfo_t;
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
struct OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9;
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
struct OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3;
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27;
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF;
struct String_t;
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700;
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
struct XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OVRManager_t21429E69CA88C5E9C6EE3AAB75EAFBE6E1B129D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OVRPlugin_t0BF53CAD10A7503BB132A303469F2E0A639E696B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OculusSettings_t0584FB71432B697479FD0BFC5B68C195F17CD321_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0114358F6929D3875146F8A0822ED1E7BB7AA767;
IL2CPP_EXTERN_C String_t* _stringLiteral06E02AAD278C470CA00827078CCD2F85C887CBD0;
IL2CPP_EXTERN_C String_t* _stringLiteral1DBA9D771CF21BD0FF172F0B4156503587FD5B7F;
IL2CPP_EXTERN_C String_t* _stringLiteral39425F5927AC51C3AA3B382514ED179CB2A6AF88;
IL2CPP_EXTERN_C String_t* _stringLiteral5F224B2AEF4A637D33372AD5CB608CD3CE8013AF;
IL2CPP_EXTERN_C String_t* _stringLiteralA1043549EABEF893B4D4BD8991158711E502C6E2;
IL2CPP_EXTERN_C String_t* _stringLiteralCFA125C4DAF3362D2EBB49EDD5FAA941D4457080;
IL2CPP_EXTERN_C String_t* _stringLiteralDE36140A6B900DD92158BBA056C0056D6031D25D;
IL2CPP_EXTERN_C String_t* _stringLiteralF811479F06651C8AFA71880841D3E3465A1FE575;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisEnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_m4AE70DFD6DCD40A56A578A98B1E3B268885BF2F3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentsInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m35AC34F858BD2F34770712CD020AA0518D9409C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_TryGetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mB716E62C397CE180F1E59E318CD5073E785CCDE3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m0C67BB2FAEE25E3D9B432C9A245E62C2760A78A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mBA548D3D8366081A2D80A286DFBAE0744464D0EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m2194C411E1208AC29EFC254376D8F58E0B011CFD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EnvironmentDepthOcclusionController_HandleDepthTextureChanged_mB93C90A0789BD998D4E00C0FC43B649531294191_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m5F62EE992DBCC5323267265794235C9EEE07997B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mF472961C4665B7EE4F1C4C8A05B00B08153BB96A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m38500C20418699AEC04B1946434E06EC96FB4B1C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m48BBB83C5F748E6E6FF0731C3682092DEA6A7173_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisOVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9_m1564DCD77DA806C8E84BE6808F00823EBCA88234_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D;
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D;
struct RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A;
struct __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
struct U3CModuleU3E_t1ECE31C6C21F77F126B65E70720465F887BF11A2 
{
};
struct List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B  : public RuntimeObject
{
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ____items;
	int32_t ____size;
	int32_t ____version;
	RuntimeObject* ____syncRoot;
};
struct List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A  : public RuntimeObject
{
	__Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* ____items;
	int32_t ____size;
	int32_t ____version;
	RuntimeObject* ____syncRoot;
};
struct EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD  : public RuntimeObject
{
};
struct String_t  : public RuntimeObject
{
	int32_t ____stringLength;
	Il2CppChar ____firstChar;
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};
struct Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8 
{
	List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* ____list;
	int32_t ____index;
	int32_t ____version;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ____current;
};
typedef Il2CppFullySharedGenericStruct Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF;
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	bool ___m_value;
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	int32_t ___m_value;
};
struct IntPtr_t 
{
	void* ___m_value;
};
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 
{
	float ___m00;
	float ___m10;
	float ___m20;
	float ___m30;
	float ___m01;
	float ___m11;
	float ___m21;
	float ___m31;
	float ___m02;
	float ___m12;
	float ___m22;
	float ___m32;
	float ___m03;
	float ___m13;
	float ___m23;
	float ___m33;
};
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	float ___m_value;
};
struct UInt32_t1833D51FFA667B18A5AA4B8D34DE284F8495D29B 
{
	uint32_t ___m_value;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	float ___x;
	float ___y;
	float ___z;
};
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};
struct Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 
{
	float ___UpTan;
	float ___DownTan;
	float ___LeftTan;
	float ___RightTan;
};
struct Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A 
{
	float ___x;
	float ___y;
	float ___z;
	float ___w;
};
struct Vector3f_t232AF83B4642C67BE8EFF85D8E1599D3B06BD562 
{
	float ___x;
	float ___y;
	float ___z;
};
struct EnvironmentDepthCreateParams_tDC6F7E5ABE8081C06DAE48548AA6CA7601824CA0 
{
	bool ___removeHands;
};
struct EnvironmentDepthCreateParams_tDC6F7E5ABE8081C06DAE48548AA6CA7601824CA0_marshaled_pinvoke
{
	int32_t ___removeHands;
};
struct EnvironmentDepthCreateParams_tDC6F7E5ABE8081C06DAE48548AA6CA7601824CA0_marshaled_com
{
	int32_t ___removeHands;
};
struct Delegate_t  : public RuntimeObject
{
	intptr_t ___method_ptr;
	intptr_t ___invoke_impl;
	RuntimeObject* ___m_target;
	intptr_t ___method;
	intptr_t ___delegate_trampoline;
	intptr_t ___extra_arg;
	intptr_t ___method_code;
	intptr_t ___interp_method;
	intptr_t ___interp_invoke_impl;
	MethodInfo_t* ___method_info;
	MethodInfo_t* ___original_method_info;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data;
	bool ___method_is_virtual;
};
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr;
	intptr_t ___invoke_impl;
	Il2CppIUnknown* ___m_target;
	intptr_t ___method;
	intptr_t ___delegate_trampoline;
	intptr_t ___extra_arg;
	intptr_t ___method_code;
	intptr_t ___interp_method;
	intptr_t ___interp_invoke_impl;
	MethodInfo_t* ___method_info;
	MethodInfo_t* ___original_method_info;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data;
	int32_t ___method_is_virtual;
};
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr;
	intptr_t ___invoke_impl;
	Il2CppIUnknown* ___m_target;
	intptr_t ___method;
	intptr_t ___delegate_trampoline;
	intptr_t ___extra_arg;
	intptr_t ___method_code;
	intptr_t ___interp_method;
	intptr_t ___interp_invoke_impl;
	MethodInfo_t* ___method_info;
	MethodInfo_t* ___original_method_info;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data;
	int32_t ___method_is_virtual;
};
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3  : public RuntimeObject
{
	intptr_t ___m_Ptr;
	RuntimeObject* ___m_SubsystemDescriptor;
};
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3_marshaled_pinvoke
{
	intptr_t ___m_Ptr;
	RuntimeObject* ___m_SubsystemDescriptor;
};
struct IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3_marshaled_com
{
	intptr_t ___m_Ptr;
	RuntimeObject* ___m_SubsystemDescriptor;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr;
};
struct OcclusionType_t5D699FE24C04AC4DD039AB3F0B722BC735D6E573 
{
	int32_t ___value__;
};
struct Frustumf2_t8F41E6A272FED294AC4EA340DD240266910EB0E6 
{
	float ___zNear;
	float ___zFar;
	Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 ___Fov;
};
struct Node_t05C1ADB663B95AA4908D5BD5ABF5150FC16D7CA1 
{
	int32_t ___value__;
};
struct Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 
{
	Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A ___Orientation;
	Vector3f_t232AF83B4642C67BE8EFF85D8E1599D3B06BD562 ___Position;
};
struct Step_t6F029880C507AACA3FA392B00D098F12099F41A7 
{
	int32_t ___value__;
};
struct FoveationMethod_t65851BA24574B9ED0C35B488C956044F531CFACD 
{
	int32_t ___value__;
};
struct StereoRenderingModeAndroid_t095E76EC5E84ADC9DDD6A78D4B3BFC749AD2625D 
{
	int32_t ___value__;
};
struct StereoRenderingModeDesktop_t069DCF385D34E6DE995A1803A9F6DC2EEA8880F1 
{
	int32_t ___value__;
};
struct EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C 
{
	bool ___isValid;
	double ___createTime;
	double ___predictedDisplayTime;
	int32_t ___swapchainIndex;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___createPoseLocation;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___createPoseRotation;
	float ___fovLeftAngle;
	float ___fovRightAngle;
	float ___fovTopAngle;
	float ___fovDownAngle;
	float ___nearZ;
	float ___farZ;
	float ___minDepth;
	float ___maxDepth;
};
struct EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C_marshaled_pinvoke
{
	int32_t ___isValid;
	double ___createTime;
	double ___predictedDisplayTime;
	int32_t ___swapchainIndex;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___createPoseLocation;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___createPoseRotation;
	float ___fovLeftAngle;
	float ___fovRightAngle;
	float ___fovTopAngle;
	float ___fovDownAngle;
	float ___nearZ;
	float ___farZ;
	float ___minDepth;
	float ___maxDepth;
};
struct EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C_marshaled_com
{
	int32_t ___isValid;
	double ___createTime;
	double ___predictedDisplayTime;
	int32_t ___swapchainIndex;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___createPoseLocation;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___createPoseRotation;
	float ___fovLeftAngle;
	float ___fovRightAngle;
	float ___fovTopAngle;
	float ___fovDownAngle;
	float ___nearZ;
	float ___farZ;
	float ___minDepth;
	float ___maxDepth;
};
struct IntegratedSubsystem_1_t8312865F01EEA1EDE4B24A973E47ADD526616848  : public IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3
{
};
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct MulticastDelegate_t  : public Delegate_t
{
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates;
};
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates;
};
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates;
};
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_pinvoke : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
};
struct ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A_marshaled_com : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
};
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};
struct Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C  : public MulticastDelegate_t
{
};
struct Action_1_t923A20D1D4F6B55B2ED5AE21B90F1A0CE0450D99  : public MulticastDelegate_t
{
};
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};
struct OculusSettings_t0584FB71432B697479FD0BFC5B68C195F17CD321  : public ScriptableObject_tB3BFDB921A1B1795B38A5417D3B97A89A140436A
{
	int32_t ___m_StereoRenderingModeDesktop;
	int32_t ___m_StereoRenderingModeAndroid;
	bool ___SharedDepthBuffer;
	bool ___DepthSubmission;
	bool ___DashSupport;
	bool ___LowOverheadMode;
	bool ___OptimizeBufferDiscards;
	bool ___PhaseSync;
	bool ___SymmetricProjection;
	bool ___SubsampledLayout;
	int32_t ___FoveatedRenderingMethod;
	bool ___LateLatching;
	bool ___LateLatchingDebug;
	bool ___EnableTrackingOriginStageMode;
	bool ___SpaceWarp;
	bool ___TargetQuest2;
	bool ___TargetQuestPro;
	bool ___TargetQuest3;
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___SystemSplashScreen;
};
struct RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};
struct XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1  : public IntegratedSubsystem_1_t8312865F01EEA1EDE4B24A973E47ADD526616848
{
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___displayFocusChanged;
	HDROutputSettings_t6A590B1AA325DD7389D71F502B762BF1592A9F62* ___m_HDROutputSettings;
};
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
	CancellationTokenSource_tAAE1E0033BCFC233801F8CB4CED5C852B350CB7B* ___m_CancellationTokenSource;
};
struct EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	int32_t ____occlusionType;
	EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* ____depthTextureProvider;
};
struct EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* ___OnDepthTextureAvailabilityChanged;
	bool ___Enable6DoFCalculations;
	bool ___Enable3DoFCalculations;
	int32_t ____framesToWaitForColdStart;
	bool ____isDepthTextureAvailable;
	bool ____areHandsRemoved;
	XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1* ____xrDisplay;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ____customTrackingSpaceTransform;
	Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ____reprojectionMatrices;
};
struct OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CtrackingSpaceU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CleftEyeAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CcenterEyeAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CrightEyeAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CleftHandAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CrightHandAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CleftHandAnchorDetachedU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CrightHandAnchorDetachedU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CleftControllerInHandAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CleftHandOnControllerAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CrightControllerInHandAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CrightHandOnControllerAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CleftControllerAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CrightControllerAnchorU3Ek__BackingField;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___U3CtrackerAnchorU3Ek__BackingField;
	Action_1_t88CC03E8C305DA991BBBCEBE79519B58D52F577F* ___UpdatedAnchors;
	Action_1_t10D7C827ADC73ED438E0CA8F04465BA6F2BAED7D* ___TrackingSpaceChanged;
	bool ___usePerEyeCameras;
	bool ___useFixedUpdateForTracking;
	bool ___disableEyeAnchorCameras;
	bool ____skipUpdate;
	String_t* ___trackingSpaceName;
	String_t* ___trackerAnchorName;
	String_t* ___leftEyeAnchorName;
	String_t* ___centerEyeAnchorName;
	String_t* ___rightEyeAnchorName;
	String_t* ___leftHandAnchorName;
	String_t* ___rightHandAnchorName;
	String_t* ___leftControllerAnchorName;
	String_t* ___rightControllerAnchorName;
	String_t* ___leftHandAnchorDetachedName;
	String_t* ___rightHandAnchorDetachedName;
	String_t* ___leftControllerInHandAnchorName;
	String_t* ___leftHandOnControllerAnchorName;
	String_t* ___rightControllerInHandAnchorName;
	String_t* ___rightHandOnControllerAnchorName;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ____centerEyeCamera;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ____leftEyeCamera;
	Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* ____rightEyeCamera;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ____previousTrackingSpaceTransform;
};
struct OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	float ___U3CDepthBiasValueU3Ek__BackingField;
	bool ___DoesAffectChildren;
	List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* ____materials;
};
struct List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B_StaticFields
{
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___s_emptyArray;
};
struct List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A_StaticFields
{
	__Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* ___s_emptyArray;
};
struct EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields
{
	bool ___IsDepthRenderingRequestEnabled;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___ScalingVector3;
};
struct String_t_StaticFields
{
	String_t* ___Empty;
};
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	String_t* ___TrueString;
	String_t* ___FalseString;
};
struct IntPtr_t_StaticFields
{
	intptr_t ___Zero;
};
struct Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___zeroMatrix;
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___identityMatrix;
};
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion;
};
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector;
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector;
};
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector;
};
struct Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A_StaticFields
{
	Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A ___identity;
};
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject;
};
struct Posef_t51A2C10B4094B44A8D3C1913292B839172887B61_StaticFields
{
	Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 ___identity;
};
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	int32_t ___GenerateAllMips;
};
struct OculusSettings_t0584FB71432B697479FD0BFC5B68C195F17CD321_StaticFields
{
	OculusSettings_t0584FB71432B697479FD0BFC5B68C195F17CD321* ___s_Settings;
};
struct EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields
{
	String_t* ___HardOcclusionKeyword;
	String_t* ___SoftOcclusionKeyword;
};
struct EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields
{
	String_t* ___DepthTexturePropertyName;
	String_t* ___ReprojectionMatricesPropertyName;
	String_t* ___Reprojection3DOFMatricesPropertyName;
	String_t* ___ZBufferParamsPropertyName;
	int32_t ___DepthTextureID;
	int32_t ___ReprojectionMatricesID;
	int32_t ___Reprojection3DOFMatricesID;
	int32_t ___ZBufferParamsID;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
struct RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A  : public RuntimeArray
{
	ALIGN_FIELD (8) Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* m_Items[1];

	inline Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D  : public RuntimeArray
{
	ALIGN_FIELD (8) Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* m_Items[1];

	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
struct Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D  : public RuntimeArray
{
	ALIGN_FIELD (8) Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 m_Items[1];

	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 value)
	{
		m_Items[index] = value;
	}
};
struct __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + il2cpp_array_calc_byte_offset(this, index);
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + il2cpp_array_calc_byte_offset(this, index);
	}
};


IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0AFBAEA7EC427E32CC9CA267B1930DC5DF67A374_gshared (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR __Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* Component_GetComponentsInChildren_TisIl2CppFullySharedGenericAny_m173581815FF84722B49A5E4DE5A2820B5D2DB00E_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mD4F3498FBD3BDD3F03CBCFB38041CBAC9C28CAFC_gshared_inline (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A* __this, Il2CppFullySharedGenericAny ___0_item, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Component_TryGetComponent_TisIl2CppFullySharedGenericAny_m754E9486E0B3F9C50B4261F1F2088D02098E214B_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, Il2CppFullySharedGenericAny* ___0_component, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mD2ED26ACAF3BAF386FFEA83893BA51DB9FD8BA30_gshared_inline (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_GetEnumerator_m8B2A92ACD4FBA5FBDC3F6F4F5C23A0DDF491DA61_gshared (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A* __this, Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF* il2cppRetVal, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mFE1EBE6F6425283FEAEAE7C79D02CDE4F9D367E8_gshared (Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF* __this, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Enumerator_get_Current_m8B42D4B2DE853B9D11B997120CD0228D4780E394_gshared_inline (Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF* __this, Il2CppFullySharedGenericAny* il2cppRetVal, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m8D8E5E878AF0A88A535AB1AB5BA4F23E151A678A_gshared (Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Component_GetComponent_TisIl2CppFullySharedGenericAny_m47CBDD147982125387F078ABBFDAAB92D397A6C2_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, Il2CppFullySharedGenericAny* il2cppRetVal, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m685A441EC9FAC9D554B26FA83A08F4BEF96DFF0E_gshared (Action_1_t923A20D1D4F6B55B2ED5AE21B90F1A0CE0450D99* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_FindObjectOfType_TisRuntimeObject_m02DFBF011F3B59F777A5E521DB2A116DD496E968_gshared (const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_m5A038831CEB84A7E374FE59D43444412629F833F_gshared_inline (Action_1_t923A20D1D4F6B55B2ED5AE21B90F1A0CE0450D99* __this, Il2CppFullySharedGenericAny ___0_obj, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void List_1_AddWithResize_mA6DFDBC2B22D6318212C6989A34784BD8303AF33_gshared (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A* __this, Il2CppFullySharedGenericAny ___0_item, const RuntimeMethod* method) ;

inline void List_1__ctor_m38500C20418699AEC04B1946434E06EC96FB4B1C (List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B*, const RuntimeMethod*))List_1__ctor_m0AFBAEA7EC427E32CC9CA267B1930DC5DF67A374_gshared)(__this, method);
}
inline RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A* Component_GetComponentsInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m35AC34F858BD2F34770712CD020AA0518D9409C7 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponentsInChildren_TisIl2CppFullySharedGenericAny_m173581815FF84722B49A5E4DE5A2820B5D2DB00E_gshared)(__this, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* Renderer_get_materials_m43D33328432700524EAEAB093C67AE5689976118 (Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* __this, const RuntimeMethod* method) ;
inline void List_1_Add_m5F62EE992DBCC5323267265794235C9EEE07997B_inline (List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* __this, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A*, Il2CppFullySharedGenericAny, const RuntimeMethod*))List_1_Add_mD4F3498FBD3BDD3F03CBCFB38041CBAC9C28CAFC_gshared_inline)((List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A*)__this, (Il2CppFullySharedGenericAny)___0_item, method);
}
inline bool Component_TryGetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mB716E62C397CE180F1E59E318CD5073E785CCDE3 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF** ___0_component, const RuntimeMethod* method)
{
	return ((  bool (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF**, const RuntimeMethod*))Component_TryGetComponent_TisIl2CppFullySharedGenericAny_m754E9486E0B3F9C50B4261F1F2088D02098E214B_gshared)(__this, ___0_component, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_x, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___1_y, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float OcclusionDepthBias_get_DepthBiasValue_m45E13DE739D93382BAEDD055464BE123FEC398BE_inline (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OcclusionDepthBias_SetDepthBias_m841C8A99F01B435648951F8470A9316D834EABD4 (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, float ___0_value, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OcclusionDepthBias_set_DepthBiasValue_m46E2AE5789CAD372A97B574FE083F62E838E666B_inline (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, float ___0_value, const RuntimeMethod* method) ;
inline int32_t List_1_get_Count_m48BBB83C5F748E6E6FF0731C3682092DEA6A7173_inline (List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B*, const RuntimeMethod*))List_1_get_Count_mD2ED26ACAF3BAF386FFEA83893BA51DB9FD8BA30_gshared_inline)(__this, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m33EF1B897E0C7C6FF538989610BFAFFEF4628CA9 (RuntimeObject* ___0_message, const RuntimeMethod* method) ;
inline Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8 List_1_GetEnumerator_mF472961C4665B7EE4F1C4C8A05B00B08153BB96A (List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* __this, const RuntimeMethod* method)
{
	Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8 il2cppRetVal;
	((  void (*) (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A*, Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF*, const RuntimeMethod*))List_1_GetEnumerator_m8B2A92ACD4FBA5FBDC3F6F4F5C23A0DDF491DA61_gshared)((List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A*)__this, (Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF*)&il2cppRetVal, method);
	return il2cppRetVal;
}
inline void Enumerator_Dispose_m0C67BB2FAEE25E3D9B432C9A245E62C2760A78A8 (Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8*, const RuntimeMethod*))Enumerator_Dispose_mFE1EBE6F6425283FEAEAE7C79D02CDE4F9D367E8_gshared)(__this, method);
}
inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* Enumerator_get_Current_m2194C411E1208AC29EFC254376D8F58E0B011CFD_inline (Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8* __this, const RuntimeMethod* method)
{
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* il2cppRetVal;
	((  void (*) (Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF*, Il2CppFullySharedGenericAny*, const RuntimeMethod*))Enumerator_get_Current_m8B42D4B2DE853B9D11B997120CD0228D4780E394_gshared_inline)((Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF*)__this, (Il2CppFullySharedGenericAny*)&il2cppRetVal, method);
	return il2cppRetVal;
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___0_name, float ___1_value, const RuntimeMethod* method) ;
inline bool Enumerator_MoveNext_mBA548D3D8366081A2D80A286DFBAE0744464D0EC (Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8*, const RuntimeMethod*))Enumerator_MoveNext_m8D8E5E878AF0A88A535AB1AB5BA4F23E151A678A_gshared)(__this, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
inline EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* Component_GetComponent_TisEnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_m4AE70DFD6DCD40A56A578A98B1E3B268885BF2F3 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* il2cppRetVal;
	((  void (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, Il2CppFullySharedGenericAny*, const RuntimeMethod*))Component_GetComponent_TisIl2CppFullySharedGenericAny_m47CBDD147982125387F078ABBFDAAB92D397A6C2_gshared)((Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*)__this, (Il2CppFullySharedGenericAny*)&il2cppRetVal, method);
	return il2cppRetVal;
}
inline void Action_1__ctor_mA8C3AC97D1F076EA5D1D0C10CEE6BD3E94711501 (Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* __this, RuntimeObject* ___0_object, intptr_t ___1_method, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C*, RuntimeObject*, intptr_t, const RuntimeMethod*))Action_1__ctor_m685A441EC9FAC9D554B26FA83A08F4BEF96DFF0E_gshared)(__this, ___0_object, ___1_method, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00 (Delegate_t* ___0_a, Delegate_t* ___1_b, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t* Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3 (Delegate_t* ___0_source, Delegate_t* ___1_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_EnableOcclusionType_mA4647C9A3C31661AB488F03221FD5DF5822620F2 (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, int32_t ___0_newOcclusionType, bool ___1_updateDepthTextureProvider, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Utils_GetEnvironmentDepthSupported_m38CA4657EF6046185F2EE6E32A9737EC476F515A (const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool EnvironmentDepthTextureProvider_GetEnvironmentDepthEnabled_m4B1FF62756A178483333BC0BA1509A1A0C218268_inline (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_SetEnvironmentDepthEnabled_mBFD7A41F37FA07A8F8D4B86D7DF91B3A17BA0708 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, bool ___0_isEnabled, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_SetOcclusionShaderKeywords_mD81CA706F28D3FCCCB9D2FCD77528067FCD0FE36 (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, int32_t ___0_newOcclusionType, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_DisableKeyword_m20FCB3643CD53D86E46DA431DA971E59D49DAF88 (String_t* ___0_keyword, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_EnableKeyword_m2449D6D86962FA7F5D1ED2B165EF63B019CBCCF1 (String_t* ___0_keyword, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A (Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA* __this, bool ___0_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1* OVRManager_GetCurrentDisplaySubsystem_m9DF732778B060759D2E11E04E49A39A43451CAA8 (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___0_x, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___1_y, const RuntimeMethod* method) ;
inline OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9* Object_FindObjectOfType_TisOVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9_m1564DCD77DA806C8E84BE6808F00823EBCA88234 (const RuntimeMethod* method)
{
	return ((  OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9* (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m02DFBF011F3B59F777A5E521DB2A116DD496E968_gshared)(method);
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* OVRCameraRig_get_trackingSpace_m76339871C7804C1BD14283FBF3D91268D4D87550_inline (OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_EnableEnvironmentDepth_m78414614F91BB2CB46DAD27AA6D3745A0C92FA22 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_DisableEnvironmentDepth_m53C3B74A8EC66182C201DE715EC4FE5CCCA644A4 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetupEnvironmentDepth_mF52FEC1788E8688BD34C1D8F3DC52B167DAFE0DA (EnvironmentDepthCreateParams_tDC6F7E5ABE8081C06DAE48548AA6CA7601824CA0 ___0_createParams, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_SetEnvironmentDepthRendering_m257DC253F68E571F263D0F31E9F9C8E6B1A1070C (bool ___0_isEnabled, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Utils_ShutdownEnvironmentDepth_mCAFD47820798F457C57144C916090DB793C54EE1 (const RuntimeMethod* method) ;
inline void Action_1_Invoke_m69C8773D6967F3B224777183E24EA621CE056F8F_inline (Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* __this, bool ___0_obj, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t923A20D1D4F6B55B2ED5AE21B90F1A0CE0450D99*, Il2CppFullySharedGenericAny, const RuntimeMethod*))Action_1_Invoke_m5A038831CEB84A7E374FE59D43444412629F833F_gshared_inline)((Action_1_t923A20D1D4F6B55B2ED5AE21B90F1A0CE0450D99*)__this, (Il2CppFullySharedGenericAny)&___0_obj, method);
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Utils_SetEnvironmentDepthHandRemoval_m74A0B2D0F70133331927F4C5D9EFC89DA84F5DAE (bool ___0_isEnabled, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Utils_GetEnvironmentDepthTextureId_m986C21DB57C620DB46A88A619565A08FDF5FEDAA (uint32_t* ___0_id, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntegratedSubsystem_get_running_m18AA0D7AD1CB593DC9EE5F3DC79643717509D6E8 (IntegratedSubsystem_t990160A89854D87C0836DC589B720231C02D4CE3* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* XRDisplaySubsystem_GetRenderTexture_mABB964AEAFF9B12DB279EDECAE85A52F6253E5CA (XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1* __this, uint32_t ___0_unityXrRenderTextureId, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_SetGlobalTexture_m6DAEF0F184187427D0B7EE64827BD95A482CC09C (int32_t ___0_nameID, Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700* ___1_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_TryFetchDepthTexture_mAB64AD7AC3A40E890B65E21ABEA007EF77C07B76 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool OVRPlugin_GetNodeFrustum2_mACA9E4870E1360284D30B033B6E5488778C6487D (int32_t ___0_nodeId, Frustumf2_t8F41E6A272FED294AC4EA340DD240266910EB0E6* ___1_frustum, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C Utils_GetEnvironmentDepthFrameDesc_m8CE3DDE71EE471F4AF71009F6CDEB4813D6756FB (int32_t ___0_eye, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 EnvironmentDepthUtils_ComputeNdcToLinearDepthParameters_m484384E428A7C3B238DB84460C56AD82B8B142B8 (float ___0_near, float ___1_far, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_SetGlobalVector_mDC5F45B008D44A2C8BF6D450CFE8B58B847C8190 (int32_t ___0_nameID, Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___1_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_CalculateReprojection_m84C744E76C390BC9900DF500F279D4248194329E (EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C ___0_frameDesc, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Transform_get_worldToLocalMatrix_mB633C122A01BCE8E51B10B8B8CB95F580750B3F1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Matrix4x4_get_isIdentity_m43A35BBB229B2B7003E5DDF6293EEA114096980F (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* __this, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_op_Multiply_m75E91775655DCA8DFC8EDE0AB787285BB3935162 (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___0_lhs, Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 ___1_rhs, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Shader_SetGlobalMatrixArray_m0232FB4129D259C4C70254E2ED65A8F19CA5D2AB (int32_t ___0_nameID, Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* ___1_values, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_Calculate3DOFReprojection_m5A17FBDE007E41E6E759FCE66C03725F79200C4B (EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C ___0_frameDesc, Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 ___1_fov, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_get_identity_m6568A73831F3E2D587420D20FF423959D7D8AB56_inline (const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA (String_t* ___0_name, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Single_IsInfinity_m8D101DE5C104130734F6DCA3E6E86345B064E4AD_inline (float ___0_f, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___0_x, float ___1_y, float ___2_z, float ___3_w, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_MakeUnprojectionMatrix_m750755A8F89BCED870CF00E955C1438A8922888A (float ___0_rightTan, float ___1_leftTan, float ___2_upTan, float ___3_downTan, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_MakeProjectionMatrix_mF609E721B1DBEBBF1E2516BEDC2588FD5983FAC3 (float ___0_rightTan, float ___1_leftTan, float ___2_upTan, float ___3_downTan, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_MakeScreenToDepthMatrix_mA1013546EEA52AACCCD7EB943CBD8C89E40F95EF (EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C ___0_frameDesc, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 OVRPlugin_GetNodePose_m973B3CA31C019465A53494EB440C13C2AE229CB3 (int32_t ___0_nodeId, int32_t ___1_stepId, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 OVRExtensions_FromQuatf_m9D9957171C088A2715767B6FE9449E97CC764E23 (Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A ___0_q, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Quaternion__ctor_m868FD60AA65DD5A8AC0C5DEB0608381A8D85FCD8_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, float ___0_x, float ___1_y, float ___2_z, float ___3_w, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Inverse_mD9C060AC626A7B406F4984AC98F8358DC89EF512 (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_rotation, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_op_Multiply_mCB375FCCC12A2EC8F9EB824A1BFB4453B58C2012_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_lhs, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_rhs, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_m5BCCC19216CFAD2426F15BC51A30421880D27B73_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_euler, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_Rotate_m015442530DFF5651458BBFDFB3CBC9180FC09D9E (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_q, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_TRS_mCC04FD47347234B451ACC6CCD2CE6D02E1E0E1E3 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_pos, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_q, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___2_s, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_get_inverse_m4F4A881CD789281EA90EB68CFD39F36C8A81E6BD (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* __this, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitConverter_SingleToInt32Bits_mC760C7CFC89725E3CF68DC45BE3A9A42A7E7DA73_inline (float ___0_value, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_Internal_ToEulerRad_m5BD0EEC543120C320DC77FCCDFD2CE2E6BD3F1A8 (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_rotation, const RuntimeMethod* method) ;
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_Internal_MakePositive_m73E2D01920CB0DFE661A55022C129E8617F0C9A8 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_euler, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Internal_FromEulerRad_m66D4475341F53949471E6870FB5C5E4A5E9BA93E (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_euler, const RuntimeMethod* method) ;
inline void List_1_AddWithResize_mA6DFDBC2B22D6318212C6989A34784BD8303AF33 (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A* __this, Il2CppFullySharedGenericAny ___0_item, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A*, Il2CppFullySharedGenericAny, const RuntimeMethod*))List_1_AddWithResize_mA6DFDBC2B22D6318212C6989A34784BD8303AF33_gshared)((List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A*)__this, ___0_item, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float OcclusionDepthBias_get_DepthBiasValue_m45E13DE739D93382BAEDD055464BE123FEC398BE (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CDepthBiasValueU3Ek__BackingField;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OcclusionDepthBias_set_DepthBiasValue_m46E2AE5789CAD372A97B574FE083F62E838E666B (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		__this->___U3CDepthBiasValueU3Ek__BackingField = L_0;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OcclusionDepthBias_Awake_mEB24668EEF589CACC55E625D658DED64DE93F986 (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentsInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m35AC34F858BD2F34770712CD020AA0518D9409C7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_TryGetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mB716E62C397CE180F1E59E318CD5073E785CCDE3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m5F62EE992DBCC5323267265794235C9EEE07997B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m38500C20418699AEC04B1946434E06EC96FB4B1C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A* V_0 = NULL;
	int32_t V_1 = 0;
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* V_2 = NULL;
	int32_t V_3 = 0;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* V_4 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_5 = NULL;
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* V_6 = NULL;
	{
		List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* L_0 = (List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B*)il2cpp_codegen_object_new(List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B_il2cpp_TypeInfo_var);
		List_1__ctor_m38500C20418699AEC04B1946434E06EC96FB4B1C(L_0, List_1__ctor_m38500C20418699AEC04B1946434E06EC96FB4B1C_RuntimeMethod_var);
		__this->____materials = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____materials), (void*)L_0);
		bool L_1 = __this->___DoesAffectChildren;
		if (!L_1)
		{
			goto IL_0053;
		}
	}
	{
		RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A* L_2;
		L_2 = Component_GetComponentsInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m35AC34F858BD2F34770712CD020AA0518D9409C7(__this, Component_GetComponentsInChildren_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m35AC34F858BD2F34770712CD020AA0518D9409C7_RuntimeMethod_var);
		V_0 = L_2;
		V_1 = 0;
		goto IL_004b;
	}

IL_001e:
	{
		RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_7;
		L_7 = Renderer_get_materials_m43D33328432700524EAEAB093C67AE5689976118(L_6, NULL);
		V_2 = L_7;
		V_3 = 0;
		goto IL_0041;
	}

IL_002b:
	{
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_8 = V_2;
		int32_t L_9 = V_3;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		V_4 = L_11;
		List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* L_12 = __this->____materials;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_13 = V_4;
		NullCheck(L_12);
		List_1_Add_m5F62EE992DBCC5323267265794235C9EEE07997B_inline(L_12, L_13, List_1_Add_m5F62EE992DBCC5323267265794235C9EEE07997B_RuntimeMethod_var);
		int32_t L_14 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_14, 1));
	}

IL_0041:
	{
		int32_t L_15 = V_3;
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_16 = V_2;
		NullCheck(L_16);
		if ((((int32_t)L_15) < ((int32_t)((int32_t)(((RuntimeArray*)L_16)->max_length)))))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_17 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_17, 1));
	}

IL_004b:
	{
		int32_t L_18 = V_1;
		RendererU5BU5D_t32FDD782F67917B2291EA4FF242719877440A02A* L_19 = V_0;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)((int32_t)(((RuntimeArray*)L_19)->max_length)))))
		{
			goto IL_001e;
		}
	}
	{
		goto IL_008e;
	}

IL_0053:
	{
		bool L_20;
		L_20 = Component_TryGetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mB716E62C397CE180F1E59E318CD5073E785CCDE3(__this, (&V_5), Component_TryGetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_mB716E62C397CE180F1E59E318CD5073E785CCDE3_RuntimeMethod_var);
		Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_21 = V_5;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_22;
		L_22 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_21, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_22)
		{
			goto IL_008e;
		}
	}
	{
		Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_23 = V_5;
		NullCheck(L_23);
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_24;
		L_24 = Renderer_get_materials_m43D33328432700524EAEAB093C67AE5689976118(L_23, NULL);
		V_2 = L_24;
		V_1 = 0;
		goto IL_0088;
	}

IL_0072:
	{
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_25 = V_2;
		int32_t L_26 = V_1;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		V_6 = L_28;
		List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* L_29 = __this->____materials;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_30 = V_6;
		NullCheck(L_29);
		List_1_Add_m5F62EE992DBCC5323267265794235C9EEE07997B_inline(L_29, L_30, List_1_Add_m5F62EE992DBCC5323267265794235C9EEE07997B_RuntimeMethod_var);
		int32_t L_31 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_31, 1));
	}

IL_0088:
	{
		int32_t L_32 = V_1;
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_33 = V_2;
		NullCheck(L_33);
		if ((((int32_t)L_32) < ((int32_t)((int32_t)(((RuntimeArray*)L_33)->max_length)))))
		{
			goto IL_0072;
		}
	}

IL_008e:
	{
		float L_34;
		L_34 = OcclusionDepthBias_get_DepthBiasValue_m45E13DE739D93382BAEDD055464BE123FEC398BE_inline(__this, NULL);
		OcclusionDepthBias_SetDepthBias_m841C8A99F01B435648951F8470A9316D834EABD4(__this, L_34, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OcclusionDepthBias_SetDepthBias_m841C8A99F01B435648951F8470A9316D834EABD4 (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, float ___0_value, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m0C67BB2FAEE25E3D9B432C9A245E62C2760A78A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mBA548D3D8366081A2D80A286DFBAE0744464D0EC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m2194C411E1208AC29EFC254376D8F58E0B011CFD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mF472961C4665B7EE4F1C4C8A05B00B08153BB96A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m48BBB83C5F748E6E6FF0731C3682092DEA6A7173_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral39425F5927AC51C3AA3B382514ED179CB2A6AF88);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCFA125C4DAF3362D2EBB49EDD5FAA941D4457080);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___0_value;
		OcclusionDepthBias_set_DepthBiasValue_m46E2AE5789CAD372A97B574FE083F62E838E666B_inline(__this, L_0, NULL);
		List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* L_1 = __this->____materials;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = List_1_get_Count_m48BBB83C5F748E6E6FF0731C3682092DEA6A7173_inline(L_1, List_1_get_Count_m48BBB83C5F748E6E6FF0731C3682092DEA6A7173_RuntimeMethod_var);
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_m33EF1B897E0C7C6FF538989610BFAFFEF4628CA9(_stringLiteral39425F5927AC51C3AA3B382514ED179CB2A6AF88, NULL);
		return;
	}

IL_0020:
	{
		List_1_t386E09F4F22DDE4D2AC41A8567FFF283C254537B* L_3 = __this->____materials;
		NullCheck(L_3);
		Enumerator_tEF58C7D40DDB41B6D712E0CAD7DA2883F14744B8 L_4;
		L_4 = List_1_GetEnumerator_mF472961C4665B7EE4F1C4C8A05B00B08153BB96A(L_3, List_1_GetEnumerator_mF472961C4665B7EE4F1C4C8A05B00B08153BB96A_RuntimeMethod_var);
		V_0 = L_4;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0050:
			{
				Enumerator_Dispose_m0C67BB2FAEE25E3D9B432C9A245E62C2760A78A8((&V_0), Enumerator_Dispose_m0C67BB2FAEE25E3D9B432C9A245E62C2760A78A8_RuntimeMethod_var);
				return;
			}
		});
		try
		{
			{
				goto IL_0045_1;
			}

IL_002e_1:
			{
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_5;
				L_5 = Enumerator_get_Current_m2194C411E1208AC29EFC254376D8F58E0B011CFD_inline((&V_0), Enumerator_get_Current_m2194C411E1208AC29EFC254376D8F58E0B011CFD_RuntimeMethod_var);
				float L_6;
				L_6 = OcclusionDepthBias_get_DepthBiasValue_m45E13DE739D93382BAEDD055464BE123FEC398BE_inline(__this, NULL);
				NullCheck(L_5);
				Material_SetFloat_m879CF81D740BAE6F23C9822400679F4D16365836(L_5, _stringLiteralCFA125C4DAF3362D2EBB49EDD5FAA941D4457080, L_6, NULL);
			}

IL_0045_1:
			{
				bool L_7;
				L_7 = Enumerator_MoveNext_mBA548D3D8366081A2D80A286DFBAE0744464D0EC((&V_0), Enumerator_MoveNext_mBA548D3D8366081A2D80A286DFBAE0744464D0EC_RuntimeMethod_var);
				if (L_7)
				{
					goto IL_002e_1;
				}
			}
			{
				goto IL_005e;
			}
		}
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_005e:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OcclusionDepthBias_AdjustDepthBias_m0BB6CD07AE37DBB1DBD36E51008F19E7478EF738 (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0;
		L_0 = OcclusionDepthBias_get_DepthBiasValue_m45E13DE739D93382BAEDD055464BE123FEC398BE_inline(__this, NULL);
		float L_1 = ___0_value;
		OcclusionDepthBias_set_DepthBiasValue_m46E2AE5789CAD372A97B574FE083F62E838E666B_inline(__this, ((float)il2cpp_codegen_add(L_0, L_1)), NULL);
		float L_2;
		L_2 = OcclusionDepthBias_get_DepthBiasValue_m45E13DE739D93382BAEDD055464BE123FEC398BE_inline(__this, NULL);
		OcclusionDepthBias_SetDepthBias_m841C8A99F01B435648951F8470A9316D834EABD4(__this, L_2, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OcclusionDepthBias__ctor_mCDE6A8BF8EE2A8324E9ABFEBBDD62AB4A3C9F7CE (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_Awake_m7ABF955D433F066548397CB103BA8E30B80888F0 (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisEnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_m4AE70DFD6DCD40A56A578A98B1E3B268885BF2F3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* L_0;
		L_0 = Component_GetComponent_TisEnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_m4AE70DFD6DCD40A56A578A98B1E3B268885BF2F3(__this, Component_GetComponent_TisEnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_m4AE70DFD6DCD40A56A578A98B1E3B268885BF2F3_RuntimeMethod_var);
		__this->____depthTextureProvider = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____depthTextureProvider), (void*)L_0);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_OnEnable_mE682B5FB1B505EF191F28CBF0DF26D4EA6371DC5 (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthOcclusionController_HandleDepthTextureChanged_mB93C90A0789BD998D4E00C0FC43B649531294191_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* L_0 = __this->____depthTextureProvider;
		EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* L_1 = L_0;
		NullCheck(L_1);
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_2 = L_1->___OnDepthTextureAvailabilityChanged;
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_3 = (Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C*)il2cpp_codegen_object_new(Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var);
		Action_1__ctor_mA8C3AC97D1F076EA5D1D0C10CEE6BD3E94711501(L_3, __this, (intptr_t)((void*)EnvironmentDepthOcclusionController_HandleDepthTextureChanged_mB93C90A0789BD998D4E00C0FC43B649531294191_RuntimeMethod_var), NULL);
		Delegate_t* L_4;
		L_4 = Delegate_Combine_m1F725AEF318BE6F0426863490691A6F4606E7D00(L_2, L_3, NULL);
		NullCheck(L_1);
		L_1->___OnDepthTextureAvailabilityChanged = ((Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C*)Castclass((RuntimeObject*)L_4, Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var));
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___OnDepthTextureAvailabilityChanged), (void*)((Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C*)Castclass((RuntimeObject*)L_4, Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var)));
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_OnDisable_mE33BE2FDE18763CACD51376517607CE57DFEE72A (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthOcclusionController_HandleDepthTextureChanged_mB93C90A0789BD998D4E00C0FC43B649531294191_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* L_0 = __this->____depthTextureProvider;
		EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* L_1 = L_0;
		NullCheck(L_1);
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_2 = L_1->___OnDepthTextureAvailabilityChanged;
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_3 = (Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C*)il2cpp_codegen_object_new(Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var);
		Action_1__ctor_mA8C3AC97D1F076EA5D1D0C10CEE6BD3E94711501(L_3, __this, (intptr_t)((void*)EnvironmentDepthOcclusionController_HandleDepthTextureChanged_mB93C90A0789BD998D4E00C0FC43B649531294191_RuntimeMethod_var), NULL);
		Delegate_t* L_4;
		L_4 = Delegate_Remove_m8B7DD5661308FA972E23CA1CC3FC9CEB355504E3(L_2, L_3, NULL);
		NullCheck(L_1);
		L_1->___OnDepthTextureAvailabilityChanged = ((Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C*)Castclass((RuntimeObject*)L_4, Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var));
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___OnDepthTextureAvailabilityChanged), (void*)((Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C*)Castclass((RuntimeObject*)L_4, Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C_il2cpp_TypeInfo_var)));
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_Start_mA69B959D30A534735DA97AB3D2C78E655C552B7A (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____occlusionType;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_1 = __this->____occlusionType;
		EnvironmentDepthOcclusionController_EnableOcclusionType_mA4647C9A3C31661AB488F03221FD5DF5822620F2(__this, L_1, (bool)1, NULL);
	}

IL_0015:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_EnableOcclusionType_mA4647C9A3C31661AB488F03221FD5DF5822620F2 (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, int32_t ___0_newOcclusionType, bool ___1_updateDepthTextureProvider, const RuntimeMethod* method) 
{
	bool V_0 = false;
	EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* G_B2_0 = NULL;
	EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* G_B3_1 = NULL;
	{
		bool L_0;
		L_0 = Utils_GetEnvironmentDepthSupported_m38CA4657EF6046185F2EE6E32A9737EC476F515A(NULL);
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_000b;
		}
		G_B1_0 = __this;
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000c;
	}

IL_000b:
	{
		int32_t L_1 = ___0_newOcclusionType;
		G_B3_0 = ((int32_t)(L_1));
		G_B3_1 = G_B2_0;
	}

IL_000c:
	{
		NullCheck(G_B3_1);
		G_B3_1->____occlusionType = G_B3_0;
		int32_t L_2 = __this->____occlusionType;
		V_0 = (bool)((!(((uint32_t)L_2) <= ((uint32_t)0)))? 1 : 0);
		bool L_3 = ___1_updateDepthTextureProvider;
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* L_4 = __this->____depthTextureProvider;
		NullCheck(L_4);
		bool L_5;
		L_5 = EnvironmentDepthTextureProvider_GetEnvironmentDepthEnabled_m4B1FF62756A178483333BC0BA1509A1A0C218268_inline(L_4, NULL);
		bool L_6 = V_0;
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_0039;
		}
	}
	{
		EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* L_7 = __this->____depthTextureProvider;
		bool L_8 = V_0;
		NullCheck(L_7);
		EnvironmentDepthTextureProvider_SetEnvironmentDepthEnabled_mBFD7A41F37FA07A8F8D4B86D7DF91B3A17BA0708(L_7, L_8, NULL);
		return;
	}

IL_0039:
	{
		int32_t L_9 = __this->____occlusionType;
		EnvironmentDepthOcclusionController_SetOcclusionShaderKeywords_mD81CA706F28D3FCCCB9D2FCD77528067FCD0FE36(__this, L_9, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_SetOcclusionShaderKeywords_mD81CA706F28D3FCCCB9D2FCD77528067FCD0FE36 (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, int32_t ___0_newOcclusionType, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___0_newOcclusionType;
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000a;
		}
	}
	{
		int32_t L_1 = ___0_newOcclusionType;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_0034;
	}

IL_000a:
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var);
		String_t* L_2 = ((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___SoftOcclusionKeyword;
		Shader_DisableKeyword_m20FCB3643CD53D86E46DA431DA971E59D49DAF88(L_2, NULL);
		String_t* L_3 = ((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___HardOcclusionKeyword;
		Shader_EnableKeyword_m2449D6D86962FA7F5D1ED2B165EF63B019CBCCF1(L_3, NULL);
		return;
	}

IL_001f:
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var);
		String_t* L_4 = ((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___HardOcclusionKeyword;
		Shader_DisableKeyword_m20FCB3643CD53D86E46DA431DA971E59D49DAF88(L_4, NULL);
		String_t* L_5 = ((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___SoftOcclusionKeyword;
		Shader_EnableKeyword_m2449D6D86962FA7F5D1ED2B165EF63B019CBCCF1(L_5, NULL);
		return;
	}

IL_0034:
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var);
		String_t* L_6 = ((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___HardOcclusionKeyword;
		Shader_DisableKeyword_m20FCB3643CD53D86E46DA431DA971E59D49DAF88(L_6, NULL);
		String_t* L_7 = ((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___SoftOcclusionKeyword;
		Shader_DisableKeyword_m20FCB3643CD53D86E46DA431DA971E59D49DAF88(L_7, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController_HandleDepthTextureChanged_mB93C90A0789BD998D4E00C0FC43B649531294191 (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, bool ___0_isDepthTextureAvailable, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_isDepthTextureAvailable;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = __this->____occlusionType;
		EnvironmentDepthOcclusionController_SetOcclusionShaderKeywords_mD81CA706F28D3FCCCB9D2FCD77528067FCD0FE36(__this, L_1, NULL);
		return;
	}

IL_0010:
	{
		EnvironmentDepthOcclusionController_SetOcclusionShaderKeywords_mD81CA706F28D3FCCCB9D2FCD77528067FCD0FE36(__this, 0, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController__ctor_mAB6661EB0D26C3469E0840945B7135CBE66DBA83 (EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthOcclusionController__cctor_m98EEBC0E8086CD28ECEEF12753053C33D528E17E (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1DBA9D771CF21BD0FF172F0B4156503587FD5B7F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF811479F06651C8AFA71880841D3E3465A1FE575);
		s_Il2CppMethodInitialized = true;
	}
	{
		((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___HardOcclusionKeyword = _stringLiteral1DBA9D771CF21BD0FF172F0B4156503587FD5B7F;
		Il2CppCodeGenWriteBarrier((void**)(&((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___HardOcclusionKeyword), (void*)_stringLiteral1DBA9D771CF21BD0FF172F0B4156503587FD5B7F);
		((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___SoftOcclusionKeyword = _stringLiteralF811479F06651C8AFA71880841D3E3465A1FE575;
		Il2CppCodeGenWriteBarrier((void**)(&((EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthOcclusionController_t6A6737B94BAAB273B7394D13E33619D05BCC8D76_il2cpp_TypeInfo_var))->___SoftOcclusionKeyword), (void*)_stringLiteralF811479F06651C8AFA71880841D3E3465A1FE575);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_Start_m976066D49E432CE2989913D53E12E9849503E4F3 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OVRManager_t21429E69CA88C5E9C6EE3AAB75EAFBE6E1B129D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisOVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9_m1564DCD77DA806C8E84BE6808F00823EBCA88234_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9* G_B5_0 = NULL;
	EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* G_B5_1 = NULL;
	OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9* G_B4_0 = NULL;
	EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* G_B4_1 = NULL;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B6_0 = NULL;
	EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* G_B6_1 = NULL;
	{
		bool L_0;
		L_0 = Utils_GetEnvironmentDepthSupported_m38CA4657EF6046185F2EE6E32A9737EC476F515A(NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		__this->____isDepthTextureAvailable = (bool)0;
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(__this, (bool)0, NULL);
		return;
	}

IL_0016:
	{
		il2cpp_codegen_runtime_class_init_inline(OVRManager_t21429E69CA88C5E9C6EE3AAB75EAFBE6E1B129D4_il2cpp_TypeInfo_var);
		XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1* L_1;
		L_1 = OVRManager_GetCurrentDisplaySubsystem_m9DF732778B060759D2E11E04E49A39A43451CAA8(NULL);
		__this->____xrDisplay = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____xrDisplay), (void*)L_1);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_2 = __this->____customTrackingSpaceTransform;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mB6120F782D83091EF56A198FCEBCF066DB4A9605(L_2, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9* L_4;
		L_4 = Object_FindObjectOfType_TisOVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9_m1564DCD77DA806C8E84BE6808F00823EBCA88234(Object_FindObjectOfType_TisOVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9_m1564DCD77DA806C8E84BE6808F00823EBCA88234_RuntimeMethod_var);
		OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9* L_5 = L_4;
		if (L_5)
		{
			G_B5_0 = L_5;
			G_B5_1 = __this;
			goto IL_003c;
		}
		G_B4_0 = L_5;
		G_B4_1 = __this;
	}
	{
		G_B6_0 = ((Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*)(NULL));
		G_B6_1 = G_B4_1;
		goto IL_0041;
	}

IL_003c:
	{
		NullCheck(G_B5_0);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_6;
		L_6 = OVRCameraRig_get_trackingSpace_m76339871C7804C1BD14283FBF3D91268D4D87550_inline(G_B5_0, NULL);
		G_B6_0 = L_6;
		G_B6_1 = G_B5_1;
	}

IL_0041:
	{
		NullCheck(G_B6_1);
		G_B6_1->____customTrackingSpaceTransform = G_B6_0;
		Il2CppCodeGenWriteBarrier((void**)(&G_B6_1->____customTrackingSpaceTransform), (void*)G_B6_0);
	}

IL_0046:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_SetEnvironmentDepthEnabled_mBFD7A41F37FA07A8F8D4B86D7DF91B3A17BA0708 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, bool ___0_isEnabled, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_isEnabled;
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		EnvironmentDepthTextureProvider_EnableEnvironmentDepth_m78414614F91BB2CB46DAD27AA6D3745A0C92FA22(__this, NULL);
		return;
	}

IL_000a:
	{
		EnvironmentDepthTextureProvider_DisableEnvironmentDepth_m53C3B74A8EC66182C201DE715EC4FE5CCCA644A4(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_EnableEnvironmentDepth_m78414614F91BB2CB46DAD27AA6D3745A0C92FA22 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EnvironmentDepthCreateParams_tDC6F7E5ABE8081C06DAE48548AA6CA7601824CA0 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		bool L_0;
		L_0 = Utils_GetEnvironmentDepthSupported_m38CA4657EF6046185F2EE6E32A9737EC476F515A(NULL);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		bool L_1 = ((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___IsDepthRenderingRequestEnabled;
		if (!L_1)
		{
			goto IL_000f;
		}
	}

IL_000e:
	{
		return;
	}

IL_000f:
	{
		il2cpp_codegen_initobj((&V_0), sizeof(EnvironmentDepthCreateParams_tDC6F7E5ABE8081C06DAE48548AA6CA7601824CA0));
		bool L_2 = __this->____areHandsRemoved;
		(&V_0)->___removeHands = L_2;
		EnvironmentDepthCreateParams_tDC6F7E5ABE8081C06DAE48548AA6CA7601824CA0 L_3 = V_0;
		Utils_SetupEnvironmentDepth_mF52FEC1788E8688BD34C1D8F3DC52B167DAFE0DA(L_3, NULL);
		Utils_SetEnvironmentDepthRendering_m257DC253F68E571F263D0F31E9F9C8E6B1A1070C((bool)1, NULL);
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___IsDepthRenderingRequestEnabled = (bool)1;
		__this->____framesToWaitForColdStart = 1;
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_DisableEnvironmentDepth_m53C3B74A8EC66182C201DE715EC4FE5CCCA644A4 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* G_B4_0 = NULL;
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* G_B3_0 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		bool L_0 = ((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___IsDepthRenderingRequestEnabled;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		Utils_ShutdownEnvironmentDepth_mCAFD47820798F457C57144C916090DB793C54EE1(NULL);
		Utils_SetEnvironmentDepthRendering_m257DC253F68E571F263D0F31E9F9C8E6B1A1070C((bool)0, NULL);
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___IsDepthRenderingRequestEnabled = (bool)0;
		__this->____isDepthTextureAvailable = (bool)0;
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_1 = __this->___OnDepthTextureAvailabilityChanged;
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_2 = L_1;
		if (L_2)
		{
			G_B4_0 = L_2;
			goto IL_002b;
		}
		G_B3_0 = L_2;
	}
	{
		return;
	}

IL_002b:
	{
		NullCheck(G_B4_0);
		Action_1_Invoke_m69C8773D6967F3B224777183E24EA621CE056F8F_inline(G_B4_0, (bool)0, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool EnvironmentDepthTextureProvider_GetEnvironmentDepthEnabled_m4B1FF62756A178483333BC0BA1509A1A0C218268 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		bool L_0 = ((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___IsDepthRenderingRequestEnabled;
		return L_0;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_RemoveHands_m7897A3A329C0AA483BC69AF6B1136561CBA23655 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, bool ___0_areHandsRemoved, const RuntimeMethod* method) 
{
	{
		bool L_0 = ___0_areHandsRemoved;
		__this->____areHandsRemoved = L_0;
		bool L_1 = ___0_areHandsRemoved;
		bool L_2;
		L_2 = Utils_SetEnvironmentDepthHandRemoval_m74A0B2D0F70133331927F4C5D9EFC89DA84F5DAE(L_1, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_TryFetchDepthTexture_mAB64AD7AC3A40E890B65E21ABEA007EF77C07B76 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA1043549EABEF893B4D4BD8991158711E502C6E2);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* V_1 = NULL;
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* G_B10_0 = NULL;
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* G_B9_0 = NULL;
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* G_B14_0 = NULL;
	Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* G_B13_0 = NULL;
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		bool L_0 = ((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___IsDepthRenderingRequestEnabled;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		int32_t L_1 = __this->____framesToWaitForColdStart;
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_2 = __this->____framesToWaitForColdStart;
		__this->____framesToWaitForColdStart = ((int32_t)il2cpp_codegen_subtract(L_2, 1));
		return;
	}

IL_0020:
	{
		V_0 = 0;
		bool L_3;
		L_3 = Utils_GetEnvironmentDepthTextureId_m986C21DB57C620DB46A88A619565A08FDF5FEDAA((&V_0), NULL);
		if (!L_3)
		{
			goto IL_007e;
		}
	}
	{
		XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1* L_4 = __this->____xrDisplay;
		if (!L_4)
		{
			goto IL_007e;
		}
	}
	{
		XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1* L_5 = __this->____xrDisplay;
		NullCheck(L_5);
		bool L_6;
		L_6 = IntegratedSubsystem_get_running_m18AA0D7AD1CB593DC9EE5F3DC79643717509D6E8(L_5, NULL);
		if (!L_6)
		{
			goto IL_007e;
		}
	}
	{
		XRDisplaySubsystem_t4B00B0BF1894A039ACFA8DDC2C2EB9301118C1F1* L_7 = __this->____xrDisplay;
		uint32_t L_8 = V_0;
		NullCheck(L_7);
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_9;
		L_9 = XRDisplaySubsystem_GetRenderTexture_mABB964AEAFF9B12DB279EDECAE85A52F6253E5CA(L_7, L_8, NULL);
		V_1 = L_9;
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var);
		int32_t L_10 = ((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___DepthTextureID;
		RenderTexture_tBA90C4C3AD9EECCFDDCC632D97C29FAB80D60D27* L_11 = V_1;
		Shader_SetGlobalTexture_m6DAEF0F184187427D0B7EE64827BD95A482CC09C(L_10, L_11, NULL);
		bool L_12 = __this->____isDepthTextureAvailable;
		if (L_12)
		{
			goto IL_00ad;
		}
	}
	{
		__this->____isDepthTextureAvailable = (bool)1;
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_13 = __this->___OnDepthTextureAvailabilityChanged;
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_14 = L_13;
		if (L_14)
		{
			G_B10_0 = L_14;
			goto IL_0072;
		}
		G_B9_0 = L_14;
	}
	{
		return;
	}

IL_0072:
	{
		bool L_15 = __this->____isDepthTextureAvailable;
		NullCheck(G_B10_0);
		Action_1_Invoke_m69C8773D6967F3B224777183E24EA621CE056F8F_inline(G_B10_0, L_15, NULL);
		return;
	}

IL_007e:
	{
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_LogWarning_m33EF1B897E0C7C6FF538989610BFAFFEF4628CA9(_stringLiteralA1043549EABEF893B4D4BD8991158711E502C6E2, NULL);
		bool L_16 = __this->____isDepthTextureAvailable;
		if (!L_16)
		{
			goto IL_00ad;
		}
	}
	{
		__this->____isDepthTextureAvailable = (bool)0;
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_17 = __this->___OnDepthTextureAvailabilityChanged;
		Action_1_t10DCB0C07D0D3C565CEACADC80D1152B35A45F6C* L_18 = L_17;
		if (L_18)
		{
			G_B14_0 = L_18;
			goto IL_00a2;
		}
		G_B13_0 = L_18;
	}
	{
		return;
	}

IL_00a2:
	{
		bool L_19 = __this->____isDepthTextureAvailable;
		NullCheck(G_B14_0);
		Action_1_Invoke_m69C8773D6967F3B224777183E24EA621CE056F8F_inline(G_B14_0, L_19, NULL);
	}

IL_00ad:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider_Update_m6C6A7DBAA6E3883B50291C91C39CB7611426D6E8 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OVRPlugin_t0BF53CAD10A7503BB132A303469F2E0A639E696B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OculusSettings_t0584FB71432B697479FD0BFC5B68C195F17CD321_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Frustumf2_t8F41E6A272FED294AC4EA340DD240266910EB0E6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Frustumf2_t8F41E6A272FED294AC4EA340DD240266910EB0E6 V_1;
	memset((&V_1), 0, sizeof(V_1));
	EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C V_2;
	memset((&V_2), 0, sizeof(V_2));
	EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C V_3;
	memset((&V_3), 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_5;
	memset((&V_5), 0, sizeof(V_5));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_6;
	memset((&V_6), 0, sizeof(V_6));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_7;
	memset((&V_7), 0, sizeof(V_7));
	{
		EnvironmentDepthTextureProvider_TryFetchDepthTexture_mAB64AD7AC3A40E890B65E21ABEA007EF77C07B76(__this, NULL);
		bool L_0 = __this->____isDepthTextureAvailable;
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		return;
	}

IL_000f:
	{
		il2cpp_codegen_runtime_class_init_inline(OVRPlugin_t0BF53CAD10A7503BB132A303469F2E0A639E696B_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = OVRPlugin_GetNodeFrustum2_mACA9E4870E1360284D30B033B6E5488778C6487D(0, (&V_0), NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(OVRPlugin_t0BF53CAD10A7503BB132A303469F2E0A639E696B_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = OVRPlugin_GetNodeFrustum2_mACA9E4870E1360284D30B033B6E5488778C6487D(1, (&V_1), NULL);
		if (L_2)
		{
			goto IL_0024;
		}
	}

IL_0023:
	{
		return;
	}

IL_0024:
	{
		OculusSettings_t0584FB71432B697479FD0BFC5B68C195F17CD321* L_3 = ((OculusSettings_t0584FB71432B697479FD0BFC5B68C195F17CD321_StaticFields*)il2cpp_codegen_static_fields_for(OculusSettings_t0584FB71432B697479FD0BFC5B68C195F17CD321_il2cpp_TypeInfo_var))->___s_Settings;
		NullCheck(L_3);
		bool L_4 = L_3->___SymmetricProjection;
		if (!L_4)
		{
			goto IL_005e;
		}
	}
	{
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7* L_5 = (Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7*)(&(&V_0)->___Fov);
		Frustumf2_t8F41E6A272FED294AC4EA340DD240266910EB0E6 L_6 = V_1;
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 L_7 = L_6.___Fov;
		float L_8 = L_7.___RightTan;
		L_5->___RightTan = L_8;
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7* L_9 = (Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7*)(&(&V_1)->___Fov);
		Frustumf2_t8F41E6A272FED294AC4EA340DD240266910EB0E6 L_10 = V_0;
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 L_11 = L_10.___Fov;
		float L_12 = L_11.___LeftTan;
		L_9->___LeftTan = L_12;
	}

IL_005e:
	{
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_13;
		L_13 = Utils_GetEnvironmentDepthFrameDesc_m8CE3DDE71EE471F4AF71009F6CDEB4813D6756FB(0, NULL);
		V_2 = L_13;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_14;
		L_14 = Utils_GetEnvironmentDepthFrameDesc_m8CE3DDE71EE471F4AF71009F6CDEB4813D6756FB(1, NULL);
		V_3 = L_14;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_15 = V_2;
		float L_16 = L_15.___nearZ;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_17 = V_2;
		float L_18 = L_17.___farZ;
		V_4 = L_18;
		float L_19 = V_4;
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_20;
		L_20 = EnvironmentDepthUtils_ComputeNdcToLinearDepthParameters_m484384E428A7C3B238DB84460C56AD82B8B142B8(L_16, L_19, NULL);
		V_5 = L_20;
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var);
		int32_t L_21 = ((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ZBufferParamsID;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_22 = V_5;
		Shader_SetGlobalVector_mDC5F45B008D44A2C8BF6D450CFE8B58B847C8190(L_21, L_22, NULL);
		bool L_23 = __this->___Enable6DoFCalculations;
		if (!L_23)
		{
			goto IL_013b;
		}
	}
	{
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_24 = __this->____reprojectionMatrices;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_25 = V_2;
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_26;
		L_26 = EnvironmentDepthUtils_CalculateReprojection_m84C744E76C390BC9900DF500F279D4248194329E(L_25, NULL);
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6)L_26);
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_27 = __this->____reprojectionMatrices;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_28 = V_3;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_29;
		L_29 = EnvironmentDepthUtils_CalculateReprojection_m84C744E76C390BC9900DF500F279D4248194329E(L_28, NULL);
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(1), (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6)L_29);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_30 = __this->____customTrackingSpaceTransform;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_31;
		L_31 = Object_op_Inequality_mD0BE578448EAA61948F25C32F8DD55AB1F778602(L_30, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_31)
		{
			goto IL_012b;
		}
	}
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_32 = __this->____customTrackingSpaceTransform;
		NullCheck(L_32);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_33;
		L_33 = Transform_get_worldToLocalMatrix_mB633C122A01BCE8E51B10B8B8CB95F580750B3F1(L_32, NULL);
		V_6 = L_33;
		bool L_34;
		L_34 = Matrix4x4_get_isIdentity_m43A35BBB229B2B7003E5DDF6293EEA114096980F((&V_6), NULL);
		if (L_34)
		{
			goto IL_012b;
		}
	}
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_35 = __this->____customTrackingSpaceTransform;
		NullCheck(L_35);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_36;
		L_36 = Transform_get_worldToLocalMatrix_mB633C122A01BCE8E51B10B8B8CB95F580750B3F1(L_35, NULL);
		V_7 = L_36;
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_37 = __this->____reprojectionMatrices;
		NullCheck(L_37);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* L_38 = ((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_39 = (*(Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6*)L_38);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_40 = V_7;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_41;
		L_41 = Matrix4x4_op_Multiply_m75E91775655DCA8DFC8EDE0AB787285BB3935162(L_39, L_40, NULL);
		*(Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6*)L_38 = L_41;
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_42 = __this->____reprojectionMatrices;
		NullCheck(L_42);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6* L_43 = ((L_42)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)));
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_44 = (*(Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6*)L_43);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_45 = V_7;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_46;
		L_46 = Matrix4x4_op_Multiply_m75E91775655DCA8DFC8EDE0AB787285BB3935162(L_44, L_45, NULL);
		*(Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6*)L_43 = L_46;
	}

IL_012b:
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var);
		int32_t L_47 = ((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ReprojectionMatricesID;
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_48 = __this->____reprojectionMatrices;
		Shader_SetGlobalMatrixArray_m0232FB4129D259C4C70254E2ED65A8F19CA5D2AB(L_47, L_48, NULL);
	}

IL_013b:
	{
		bool L_49 = __this->___Enable3DoFCalculations;
		if (!L_49)
		{
			goto IL_0183;
		}
	}
	{
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_50 = __this->____reprojectionMatrices;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_51 = V_2;
		Frustumf2_t8F41E6A272FED294AC4EA340DD240266910EB0E6 L_52 = V_0;
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 L_53 = L_52.___Fov;
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_54;
		L_54 = EnvironmentDepthUtils_Calculate3DOFReprojection_m5A17FBDE007E41E6E759FCE66C03725F79200C4B(L_51, L_53, NULL);
		NullCheck(L_50);
		(L_50)->SetAt(static_cast<il2cpp_array_size_t>(0), (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6)L_54);
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_55 = __this->____reprojectionMatrices;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_56 = V_3;
		Frustumf2_t8F41E6A272FED294AC4EA340DD240266910EB0E6 L_57 = V_1;
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 L_58 = L_57.___Fov;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_59;
		L_59 = EnvironmentDepthUtils_Calculate3DOFReprojection_m5A17FBDE007E41E6E759FCE66C03725F79200C4B(L_56, L_58, NULL);
		NullCheck(L_55);
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(1), (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6)L_59);
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var);
		int32_t L_60 = ((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___Reprojection3DOFMatricesID;
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_61 = __this->____reprojectionMatrices;
		Shader_SetGlobalMatrixArray_m0232FB4129D259C4C70254E2ED65A8F19CA5D2AB(L_60, L_61, NULL);
	}

IL_0183:
	{
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider__ctor_m2CA2ED48DE1607D65C1F0F8E2FC9B31467251CA4 (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->___Enable6DoFCalculations = (bool)1;
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_0 = (Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D*)(Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D*)SZArrayNew(Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D_il2cpp_TypeInfo_var, (uint32_t)2);
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_1 = L_0;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_2;
		L_2 = Matrix4x4_get_identity_m6568A73831F3E2D587420D20FF423959D7D8AB56_inline(NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6)L_2);
		Matrix4x4U5BU5D_t9C51C93425FABC022B506D2DB3A5FA70F9752C4D* L_3 = L_1;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_4;
		L_4 = Matrix4x4_get_identity_m6568A73831F3E2D587420D20FF423959D7D8AB56_inline(NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6)L_4);
		__this->____reprojectionMatrices = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->____reprojectionMatrices), (void*)L_3);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthTextureProvider__cctor_mE7E192C6658338E98BADD805E34B07A40228E4E8 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0114358F6929D3875146F8A0822ED1E7BB7AA767);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral06E02AAD278C470CA00827078CCD2F85C887CBD0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5F224B2AEF4A637D33372AD5CB608CD3CE8013AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE36140A6B900DD92158BBA056C0056D6031D25D);
		s_Il2CppMethodInitialized = true;
	}
	{
		((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___DepthTexturePropertyName = _stringLiteralDE36140A6B900DD92158BBA056C0056D6031D25D;
		Il2CppCodeGenWriteBarrier((void**)(&((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___DepthTexturePropertyName), (void*)_stringLiteralDE36140A6B900DD92158BBA056C0056D6031D25D);
		((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ReprojectionMatricesPropertyName = _stringLiteral06E02AAD278C470CA00827078CCD2F85C887CBD0;
		Il2CppCodeGenWriteBarrier((void**)(&((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ReprojectionMatricesPropertyName), (void*)_stringLiteral06E02AAD278C470CA00827078CCD2F85C887CBD0);
		((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___Reprojection3DOFMatricesPropertyName = _stringLiteral0114358F6929D3875146F8A0822ED1E7BB7AA767;
		Il2CppCodeGenWriteBarrier((void**)(&((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___Reprojection3DOFMatricesPropertyName), (void*)_stringLiteral0114358F6929D3875146F8A0822ED1E7BB7AA767);
		((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ZBufferParamsPropertyName = _stringLiteral5F224B2AEF4A637D33372AD5CB608CD3CE8013AF;
		Il2CppCodeGenWriteBarrier((void**)(&((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ZBufferParamsPropertyName), (void*)_stringLiteral5F224B2AEF4A637D33372AD5CB608CD3CE8013AF);
		String_t* L_0 = ((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___DepthTexturePropertyName;
		int32_t L_1;
		L_1 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(L_0, NULL);
		((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___DepthTextureID = L_1;
		String_t* L_2 = ((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ReprojectionMatricesPropertyName;
		int32_t L_3;
		L_3 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(L_2, NULL);
		((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ReprojectionMatricesID = L_3;
		String_t* L_4 = ((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___Reprojection3DOFMatricesPropertyName;
		int32_t L_5;
		L_5 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(L_4, NULL);
		((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___Reprojection3DOFMatricesID = L_5;
		String_t* L_6 = ((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ZBufferParamsPropertyName;
		int32_t L_7;
		L_7 = Shader_PropertyToID_mE98523D50F5656CAE89B30695C458253EB8956CA(L_6, NULL);
		((EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0_il2cpp_TypeInfo_var))->___ZBufferParamsID = L_7;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 EnvironmentDepthUtils_ComputeNdcToLinearDepthParameters_m484384E428A7C3B238DB84460C56AD82B8B142B8 (float ___0_near, float ___1_far, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		float L_0 = ___1_far;
		float L_1 = ___0_near;
		if ((((float)L_0) < ((float)L_1)))
		{
			goto IL_000c;
		}
	}
	{
		float L_2 = ___1_far;
		bool L_3;
		L_3 = Single_IsInfinity_m8D101DE5C104130734F6DCA3E6E86345B064E4AD_inline(L_2, NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}

IL_000c:
	{
		float L_4 = ___0_near;
		V_0 = ((float)il2cpp_codegen_multiply((-2.0f), L_4));
		V_1 = (-1.0f);
		goto IL_0033;
	}

IL_001c:
	{
		float L_5 = ___1_far;
		float L_6 = ___0_near;
		float L_7 = ___1_far;
		float L_8 = ___0_near;
		V_0 = ((float)(((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply((-2.0f), L_5)), L_6))/((float)il2cpp_codegen_subtract(L_7, L_8))));
		float L_9 = ___1_far;
		float L_10 = ___0_near;
		float L_11 = ___1_far;
		float L_12 = ___0_near;
		V_1 = ((float)(((-((float)il2cpp_codegen_add(L_9, L_10))))/((float)il2cpp_codegen_subtract(L_11, L_12))));
	}

IL_0033:
	{
		float L_13 = V_0;
		float L_14 = V_1;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline((&L_15), L_13, L_14, (0.0f), (0.0f), NULL);
		return L_15;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_Calculate3DOFReprojection_m5A17FBDE007E41E6E759FCE66C03725F79200C4B (EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C ___0_frameDesc, Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 ___1_fov, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 L_0 = ___1_fov;
		float L_1 = L_0.___RightTan;
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 L_2 = ___1_fov;
		float L_3 = L_2.___LeftTan;
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 L_4 = ___1_fov;
		float L_5 = L_4.___UpTan;
		Fovf_t7A8312168C57A6CC5AA4FE685369A4618BF686E7 L_6 = ___1_fov;
		float L_7 = L_6.___DownTan;
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_8;
		L_8 = EnvironmentDepthUtils_MakeUnprojectionMatrix_m750755A8F89BCED870CF00E955C1438A8922888A(L_1, L_3, L_5, L_7, NULL);
		V_0 = L_8;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_9 = ___0_frameDesc;
		float L_10 = L_9.___fovRightAngle;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_11 = ___0_frameDesc;
		float L_12 = L_11.___fovLeftAngle;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_13 = ___0_frameDesc;
		float L_14 = L_13.___fovTopAngle;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_15 = ___0_frameDesc;
		float L_16 = L_15.___fovDownAngle;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_17;
		L_17 = EnvironmentDepthUtils_MakeProjectionMatrix_mF609E721B1DBEBBF1E2516BEDC2588FD5983FAC3(L_10, L_12, L_14, L_16, NULL);
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_18 = ___0_frameDesc;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_19;
		L_19 = EnvironmentDepthUtils_MakeScreenToDepthMatrix_mA1013546EEA52AACCCD7EB943CBD8C89E40F95EF(L_18, NULL);
		V_1 = L_19;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_20 = V_1;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_21;
		L_21 = Matrix4x4_op_Multiply_m75E91775655DCA8DFC8EDE0AB787285BB3935162(L_17, L_20, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_22 = V_0;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_23;
		L_23 = Matrix4x4_op_Multiply_m75E91775655DCA8DFC8EDE0AB787285BB3935162(L_21, L_22, NULL);
		return L_23;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_MakeScreenToDepthMatrix_mA1013546EEA52AACCCD7EB943CBD8C89E40F95EF (EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C ___0_frameDesc, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OVRPlugin_t0BF53CAD10A7503BB132A303469F2E0A639E696B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		il2cpp_codegen_runtime_class_init_inline(OVRPlugin_t0BF53CAD10A7503BB132A303469F2E0A639E696B_il2cpp_TypeInfo_var);
		Posef_t51A2C10B4094B44A8D3C1913292B839172887B61 L_0;
		L_0 = OVRPlugin_GetNodePose_m973B3CA31C019465A53494EB440C13C2AE229CB3(0, (-1), NULL);
		Quatf_t5347392804DF5326AF790F82E4EDE1578FED682A L_1 = L_0.___Orientation;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_2;
		L_2 = OVRExtensions_FromQuatf_m9D9957171C088A2715767B6FE9449E97CC764E23(L_1, NULL);
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_3 = ___0_frameDesc;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_4 = L_3.___createPoseRotation;
		float L_5 = L_4.___x;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_6 = ___0_frameDesc;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_7 = L_6.___createPoseRotation;
		float L_8 = L_7.___y;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_9 = ___0_frameDesc;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_10 = L_9.___createPoseRotation;
		float L_11 = L_10.___z;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_12 = ___0_frameDesc;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_13 = L_12.___createPoseRotation;
		float L_14 = L_13.___w;
		Quaternion__ctor_m868FD60AA65DD5A8AC0C5DEB0608381A8D85FCD8_inline((&V_0), ((-L_5)), ((-L_8)), L_11, L_14, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_15;
		L_15 = Quaternion_Inverse_mD9C060AC626A7B406F4984AC98F8358DC89EF512(L_2, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_16 = V_0;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_17;
		L_17 = Quaternion_op_Multiply_mCB375FCCC12A2EC8F9EB824A1BFB4453B58C2012_inline(L_15, L_16, NULL);
		V_2 = L_17;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18;
		L_18 = Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline((&V_2), NULL);
		V_1 = L_18;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_19 = V_1;
		float L_20 = L_19.___z;
		(&V_1)->___z = ((-L_20));
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21 = V_1;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_22;
		L_22 = Quaternion_Euler_m5BCCC19216CFAD2426F15BC51A30421880D27B73_inline(L_21, NULL);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_23;
		L_23 = Matrix4x4_Rotate_m015442530DFF5651458BBFDFB3CBC9180FC09D9E(L_22, NULL);
		return L_23;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_MakeProjectionMatrix_mF609E721B1DBEBBF1E2516BEDC2588FD5983FAC3 (float ___0_rightTan, float ___1_leftTan, float ___2_upTan, float ___3_downTan, const RuntimeMethod* method) 
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0;
		L_0 = Matrix4x4_get_identity_m6568A73831F3E2D587420D20FF423959D7D8AB56_inline(NULL);
		V_0 = L_0;
		float L_1 = ___0_rightTan;
		float L_2 = ___1_leftTan;
		V_1 = ((float)il2cpp_codegen_add(L_1, L_2));
		float L_3 = ___2_upTan;
		float L_4 = ___3_downTan;
		V_2 = ((float)il2cpp_codegen_add(L_3, L_4));
		float L_5 = V_1;
		(&V_0)->___m00 = ((float)((1.0f)/L_5));
		float L_6 = V_2;
		(&V_0)->___m11 = ((float)((1.0f)/L_6));
		float L_7 = ___1_leftTan;
		float L_8 = V_1;
		(&V_0)->___m03 = ((float)(L_7/L_8));
		float L_9 = ___3_downTan;
		float L_10 = V_2;
		(&V_0)->___m13 = ((float)(L_9/L_10));
		(&V_0)->___m23 = (-1.0f);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_11 = V_0;
		return L_11;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_MakeUnprojectionMatrix_m750755A8F89BCED870CF00E955C1438A8922888A (float ___0_rightTan, float ___1_leftTan, float ___2_upTan, float ___3_downTan, const RuntimeMethod* method) 
{
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0;
		L_0 = Matrix4x4_get_identity_m6568A73831F3E2D587420D20FF423959D7D8AB56_inline(NULL);
		V_0 = L_0;
		float L_1 = ___0_rightTan;
		float L_2 = ___1_leftTan;
		(&V_0)->___m00 = ((float)il2cpp_codegen_add(L_1, L_2));
		float L_3 = ___2_upTan;
		float L_4 = ___3_downTan;
		(&V_0)->___m11 = ((float)il2cpp_codegen_add(L_3, L_4));
		float L_5 = ___1_leftTan;
		(&V_0)->___m03 = ((-L_5));
		float L_6 = ___3_downTan;
		(&V_0)->___m13 = ((-L_6));
		(&V_0)->___m23 = (1.0f);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 EnvironmentDepthUtils_CalculateReprojection_m84C744E76C390BC9900DF500F279D4248194329E (EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C ___0_frameDesc, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 V_13;
	memset((&V_13), 0, sizeof(V_13));
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_14;
	memset((&V_14), 0, sizeof(V_14));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_15;
	memset((&V_15), 0, sizeof(V_15));
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_16;
	memset((&V_16), 0, sizeof(V_16));
	{
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_0 = ___0_frameDesc;
		float L_1 = L_0.___fovLeftAngle;
		V_0 = L_1;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_2 = ___0_frameDesc;
		float L_3 = L_2.___fovRightAngle;
		V_1 = L_3;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_4 = ___0_frameDesc;
		float L_5 = L_4.___fovDownAngle;
		V_2 = L_5;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_6 = ___0_frameDesc;
		float L_7 = L_6.___fovTopAngle;
		V_3 = L_7;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_8 = ___0_frameDesc;
		float L_9 = L_8.___nearZ;
		V_4 = L_9;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_10 = ___0_frameDesc;
		float L_11 = L_10.___farZ;
		V_5 = L_11;
		float L_12 = V_1;
		float L_13 = V_0;
		V_6 = ((float)((2.0f)/((float)il2cpp_codegen_add(L_12, L_13))));
		float L_14 = V_3;
		float L_15 = V_2;
		V_7 = ((float)((2.0f)/((float)il2cpp_codegen_add(L_14, L_15))));
		float L_16 = V_1;
		float L_17 = V_0;
		float L_18 = V_1;
		float L_19 = V_0;
		V_8 = ((float)(((float)il2cpp_codegen_subtract(L_16, L_17))/((float)il2cpp_codegen_add(L_18, L_19))));
		float L_20 = V_3;
		float L_21 = V_2;
		float L_22 = V_3;
		float L_23 = V_2;
		V_9 = ((float)(((float)il2cpp_codegen_subtract(L_20, L_21))/((float)il2cpp_codegen_add(L_22, L_23))));
		float L_24 = V_5;
		bool L_25;
		L_25 = Single_IsInfinity_m8D101DE5C104130734F6DCA3E6E86345B064E4AD_inline(L_24, NULL);
		if (!L_25)
		{
			goto IL_0070;
		}
	}
	{
		V_10 = (-1.0f);
		float L_26 = V_4;
		V_11 = ((float)il2cpp_codegen_multiply((-2.0f), L_26));
		goto IL_0092;
	}

IL_0070:
	{
		float L_27 = V_5;
		float L_28 = V_4;
		float L_29 = V_5;
		float L_30 = V_4;
		V_10 = ((float)(((-((float)il2cpp_codegen_add(L_27, L_28))))/((float)il2cpp_codegen_subtract(L_29, L_30))));
		float L_31 = V_5;
		float L_32 = V_4;
		float L_33 = V_5;
		float L_34 = V_4;
		V_11 = ((float)(((-((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply((2.0f), L_31)), L_32))))/((float)il2cpp_codegen_subtract(L_33, L_34))));
	}

IL_0092:
	{
		V_12 = (-1.0f);
		il2cpp_codegen_initobj((&V_16), sizeof(Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6));
		float L_35 = V_6;
		(&V_16)->___m00 = L_35;
		(&V_16)->___m01 = (0.0f);
		float L_36 = V_8;
		(&V_16)->___m02 = L_36;
		(&V_16)->___m03 = (0.0f);
		(&V_16)->___m10 = (0.0f);
		float L_37 = V_7;
		(&V_16)->___m11 = L_37;
		float L_38 = V_9;
		(&V_16)->___m12 = L_38;
		(&V_16)->___m13 = (0.0f);
		(&V_16)->___m20 = (0.0f);
		(&V_16)->___m21 = (0.0f);
		float L_39 = V_10;
		(&V_16)->___m22 = L_39;
		float L_40 = V_11;
		(&V_16)->___m23 = L_40;
		(&V_16)->___m30 = (0.0f);
		(&V_16)->___m31 = (0.0f);
		float L_41 = V_12;
		(&V_16)->___m32 = L_41;
		(&V_16)->___m33 = (0.0f);
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_42 = V_16;
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_43 = ___0_frameDesc;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_44 = L_43.___createPoseRotation;
		V_13 = L_44;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_45 = V_13;
		float L_46 = L_45.___x;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_47 = V_13;
		float L_48 = L_47.___y;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_49 = V_13;
		float L_50 = L_49.___z;
		Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 L_51 = V_13;
		float L_52 = L_51.___w;
		Quaternion__ctor_m868FD60AA65DD5A8AC0C5DEB0608381A8D85FCD8_inline((&V_14), L_46, L_48, L_50, L_52, NULL);
		EnvironmentDepthFrameDesc_t54EAD2C9E064A249F726E96306B9615EA364918C L_53 = ___0_frameDesc;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_54 = L_53.___createPoseLocation;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_55 = V_14;
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_56 = ((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___ScalingVector3;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_57;
		L_57 = Matrix4x4_TRS_mCC04FD47347234B451ACC6CCD2CE6D02E1E0E1E3(L_54, L_55, L_56, NULL);
		V_16 = L_57;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_58;
		L_58 = Matrix4x4_get_inverse_m4F4A881CD789281EA90EB68CFD39F36C8A81E6BD((&V_16), NULL);
		V_15 = L_58;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_59 = V_15;
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_60;
		L_60 = Matrix4x4_op_Multiply_m75E91775655DCA8DFC8EDE0AB787285BB3935162(L_42, L_59, NULL);
		return L_60;
	}
}
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvironmentDepthUtils__cctor_m7CAF8DDC16C13EFBA77633C5789BDFBC90848049 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_0), (1.0f), (1.0f), (-1.0f), NULL);
		((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___ScalingVector3 = L_0;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float OcclusionDepthBias_get_DepthBiasValue_m45E13DE739D93382BAEDD055464BE123FEC398BE_inline (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, const RuntimeMethod* method) 
{
	{
		float L_0 = __this->___U3CDepthBiasValueU3Ek__BackingField;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OcclusionDepthBias_set_DepthBiasValue_m46E2AE5789CAD372A97B574FE083F62E838E666B_inline (OcclusionDepthBias_t860059C5252B018EA56831FA4D8790ECD9B4C9F3* __this, float ___0_value, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_value;
		__this->___U3CDepthBiasValueU3Ek__BackingField = L_0;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool EnvironmentDepthTextureProvider_GetEnvironmentDepthEnabled_m4B1FF62756A178483333BC0BA1509A1A0C218268_inline (EnvironmentDepthTextureProvider_t8C8EA1C43A3030FCF1B9DB63C597D35C64D807A0* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_runtime_class_init_inline(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var);
		bool L_0 = ((EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_StaticFields*)il2cpp_codegen_static_fields_for(EnvironmentDepthUtils_t01835EF5F26A9EB5230297CD0C1899770284DBAD_il2cpp_TypeInfo_var))->___IsDepthRenderingRequestEnabled;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* OVRCameraRig_get_trackingSpace_m76339871C7804C1BD14283FBF3D91268D4D87550_inline (OVRCameraRig_t7FC2BB0D30DED2B7F0C8914AF2B66E9F4CF891A9* __this, const RuntimeMethod* method) 
{
	{
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0 = __this->___U3CtrackingSpaceU3Ek__BackingField;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 Matrix4x4_get_identity_m6568A73831F3E2D587420D20FF423959D7D8AB56_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_0 = ((Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_StaticFields*)il2cpp_codegen_static_fields_for(Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6_il2cpp_TypeInfo_var))->___identityMatrix;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Matrix4x4_tDB70CF134A14BA38190C59AA700BCE10E2AED3E6 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Single_IsInfinity_m8D101DE5C104130734F6DCA3E6E86345B064E4AD_inline (float ___0_f, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_f;
		int32_t L_1;
		L_1 = BitConverter_SingleToInt32Bits_mC760C7CFC89725E3CF68DC45BE3A9A42A7E7DA73_inline(L_0, NULL);
		return (bool)((((int32_t)((int32_t)(L_1&((int32_t)2147483647LL)))) == ((int32_t)((int32_t)2139095040)))? 1 : 0);
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector4__ctor_m96B2CD8B862B271F513AF0BDC2EABD58E4DBC813_inline (Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3* __this, float ___0_x, float ___1_y, float ___2_z, float ___3_w, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x = L_0;
		float L_1 = ___1_y;
		__this->___y = L_1;
		float L_2 = ___2_z;
		__this->___z = L_2;
		float L_3 = ___3_w;
		__this->___w = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Quaternion__ctor_m868FD60AA65DD5A8AC0C5DEB0608381A8D85FCD8_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, float ___0_x, float ___1_y, float ___2_z, float ___3_w, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x = L_0;
		float L_1 = ___1_y;
		__this->___y = L_1;
		float L_2 = ___2_z;
		__this->___z = L_2;
		float L_3 = ___3_w;
		__this->___w = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_op_Multiply_mCB375FCCC12A2EC8F9EB824A1BFB4453B58C2012_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___0_lhs, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___1_rhs, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = ___0_lhs;
		float L_1 = L_0.___w;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_2 = ___1_rhs;
		float L_3 = L_2.___x;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_4 = ___0_lhs;
		float L_5 = L_4.___x;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = ___1_rhs;
		float L_7 = L_6.___w;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_8 = ___0_lhs;
		float L_9 = L_8.___y;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_10 = ___1_rhs;
		float L_11 = L_10.___z;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_12 = ___0_lhs;
		float L_13 = L_12.___z;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_14 = ___1_rhs;
		float L_15 = L_14.___y;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_16 = ___0_lhs;
		float L_17 = L_16.___w;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_18 = ___1_rhs;
		float L_19 = L_18.___y;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_20 = ___0_lhs;
		float L_21 = L_20.___y;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_22 = ___1_rhs;
		float L_23 = L_22.___w;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_24 = ___0_lhs;
		float L_25 = L_24.___z;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_26 = ___1_rhs;
		float L_27 = L_26.___x;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_28 = ___0_lhs;
		float L_29 = L_28.___x;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_30 = ___1_rhs;
		float L_31 = L_30.___z;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_32 = ___0_lhs;
		float L_33 = L_32.___w;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_34 = ___1_rhs;
		float L_35 = L_34.___z;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_36 = ___0_lhs;
		float L_37 = L_36.___z;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_38 = ___1_rhs;
		float L_39 = L_38.___w;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_40 = ___0_lhs;
		float L_41 = L_40.___x;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_42 = ___1_rhs;
		float L_43 = L_42.___y;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_44 = ___0_lhs;
		float L_45 = L_44.___y;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_46 = ___1_rhs;
		float L_47 = L_46.___x;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_48 = ___0_lhs;
		float L_49 = L_48.___w;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_50 = ___1_rhs;
		float L_51 = L_50.___w;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_52 = ___0_lhs;
		float L_53 = L_52.___x;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_54 = ___1_rhs;
		float L_55 = L_54.___x;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_56 = ___0_lhs;
		float L_57 = L_56.___y;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_58 = ___1_rhs;
		float L_59 = L_58.___y;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_60 = ___0_lhs;
		float L_61 = L_60.___z;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_62 = ___1_rhs;
		float L_63 = L_62.___z;
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_64;
		memset((&L_64), 0, sizeof(L_64));
		Quaternion__ctor_m868FD60AA65DD5A8AC0C5DEB0608381A8D85FCD8_inline((&L_64), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_1, L_3)), ((float)il2cpp_codegen_multiply(L_5, L_7)))), ((float)il2cpp_codegen_multiply(L_9, L_11)))), ((float)il2cpp_codegen_multiply(L_13, L_15)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_17, L_19)), ((float)il2cpp_codegen_multiply(L_21, L_23)))), ((float)il2cpp_codegen_multiply(L_25, L_27)))), ((float)il2cpp_codegen_multiply(L_29, L_31)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_add(((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_33, L_35)), ((float)il2cpp_codegen_multiply(L_37, L_39)))), ((float)il2cpp_codegen_multiply(L_41, L_43)))), ((float)il2cpp_codegen_multiply(L_45, L_47)))), ((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_subtract(((float)il2cpp_codegen_multiply(L_49, L_51)), ((float)il2cpp_codegen_multiply(L_53, L_55)))), ((float)il2cpp_codegen_multiply(L_57, L_59)))), ((float)il2cpp_codegen_multiply(L_61, L_63)))), NULL);
		V_0 = L_64;
		goto IL_00e5;
	}

IL_00e5:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_65 = V_0;
		return L_65;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = (*(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974*)__this);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Quaternion_Internal_ToEulerRad_m5BD0EEC543120C320DC77FCCDFD2CE2E6BD3F1A8(L_0, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_1, (57.2957802f), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Quaternion_Internal_MakePositive_m73E2D01920CB0DFE661A55022C129E8617F0C9A8(L_2, NULL);
		V_0 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_m5BCCC19216CFAD2426F15BC51A30421880D27B73_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_euler, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_euler;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline(L_0, (0.0174532924f), NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_2;
		L_2 = Quaternion_Internal_FromEulerRad_m66D4475341F53949471E6870FB5C5E4A5E9BA93E(L_1, NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___0_x, float ___1_y, float ___2_z, const RuntimeMethod* method) 
{
	{
		float L_0 = ___0_x;
		__this->___x = L_0;
		float L_1 = ___1_y;
		__this->___y = L_1;
		float L_2 = ___2_z;
		__this->___z = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mD4F3498FBD3BDD3F03CBCFB38041CBAC9C28CAFC_gshared_inline (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A* __this, Il2CppFullySharedGenericAny ___0_item, const RuntimeMethod* method) 
{
	const uint32_t SizeOf_T_t664E2061A913AF1FEE499655BC64F0FDE10D2A5E = il2cpp_codegen_sizeof(il2cpp_rgctx_data_no_init(method->klass->rgctx_data, 9));
	const Il2CppFullySharedGenericAny L_8 = alloca(SizeOf_T_t664E2061A913AF1FEE499655BC64F0FDE10D2A5E);
	const Il2CppFullySharedGenericAny L_9 = L_8;
	const Il2CppFullySharedGenericAny L_10 = alloca(SizeOf_T_t664E2061A913AF1FEE499655BC64F0FDE10D2A5E);
	__Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->____version;
		__this->____version = ((int32_t)il2cpp_codegen_add(L_0, 1));
		__Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* L_1 = __this->____items;
		V_0 = L_1;
		int32_t L_2 = __this->____size;
		V_1 = L_2;
		int32_t L_3 = V_1;
		__Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* L_4 = V_0;
		NullCheck(L_4);
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size = ((int32_t)il2cpp_codegen_add(L_5, 1));
		__Il2CppFullySharedGenericTypeU5BU5D_tCAB6D060972DD49223A834B7EEFEB9FE2D003BEC* L_6 = V_0;
		int32_t L_7 = V_1;
		il2cpp_codegen_memcpy(L_8, (il2cpp_codegen_class_is_value_type(il2cpp_rgctx_data_no_init(method->klass->rgctx_data, 9)) ? ___0_item : &___0_item), SizeOf_T_t664E2061A913AF1FEE499655BC64F0FDE10D2A5E);
		NullCheck(L_6);
		il2cpp_codegen_memcpy((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)), L_8, SizeOf_T_t664E2061A913AF1FEE499655BC64F0FDE10D2A5E);
		Il2CppCodeGenWriteBarrierForClass(il2cpp_rgctx_data(method->klass->rgctx_data, 9), (void**)(L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)), (void*)L_8);
		return;
	}

IL_0034:
	{
		il2cpp_codegen_memcpy(L_9, (il2cpp_codegen_class_is_value_type(il2cpp_rgctx_data_no_init(method->klass->rgctx_data, 9)) ? ___0_item : &___0_item), SizeOf_T_t664E2061A913AF1FEE499655BC64F0FDE10D2A5E);
		List_1_AddWithResize_mA6DFDBC2B22D6318212C6989A34784BD8303AF33(__this, (il2cpp_codegen_class_is_value_type(il2cpp_rgctx_data_no_init(method->klass->rgctx_data, 9)) ? il2cpp_codegen_memcpy(L_10, L_9, SizeOf_T_t664E2061A913AF1FEE499655BC64F0FDE10D2A5E): *(void**)L_9), il2cpp_rgctx_method(method->klass->rgctx_data, 14));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mD2ED26ACAF3BAF386FFEA83893BA51DB9FD8BA30_gshared_inline (List_1_tDBA89B0E21BAC58CFBD3C1F76E4668E3B562761A* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____size;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Enumerator_get_Current_m8B42D4B2DE853B9D11B997120CD0228D4780E394_gshared_inline (Enumerator_tF5AC6CD19D283FBD724440520CEE68FE2602F7AF* __this, Il2CppFullySharedGenericAny* il2cppRetVal, const RuntimeMethod* method) 
{
	const uint32_t SizeOf_T_t010616E3077234188F9BB4FAF369F8571BC5F2E1 = il2cpp_codegen_sizeof(il2cpp_rgctx_data_no_init(InitializedTypeInfo(method->klass)->rgctx_data, 2));
	const Il2CppFullySharedGenericAny L_0 = alloca(SizeOf_T_t010616E3077234188F9BB4FAF369F8571BC5F2E1);
	{
		il2cpp_codegen_memcpy(L_0, il2cpp_codegen_get_instance_field_data_pointer(__this, il2cpp_rgctx_field(il2cpp_rgctx_data_no_init(InitializedTypeInfo(method->klass)->rgctx_data, 1),3)), SizeOf_T_t010616E3077234188F9BB4FAF369F8571BC5F2E1);
		il2cpp_codegen_memcpy(il2cppRetVal, L_0, SizeOf_T_t010616E3077234188F9BB4FAF369F8571BC5F2E1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Action_1_Invoke_m5A038831CEB84A7E374FE59D43444412629F833F_gshared_inline (Action_1_t923A20D1D4F6B55B2ED5AE21B90F1A0CE0450D99* __this, Il2CppFullySharedGenericAny ___0_obj, const RuntimeMethod* method) 
{
	typedef void (*FunctionPointerType) (RuntimeObject*, Il2CppFullySharedGenericAny, const RuntimeMethod*);
	((FunctionPointerType)__this->___invoke_impl)((Il2CppObject*)__this->___method_code, ___0_obj, reinterpret_cast<RuntimeMethod*>(__this->___method));
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BitConverter_SingleToInt32Bits_mC760C7CFC89725E3CF68DC45BE3A9A42A7E7DA73_inline (float ___0_value, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = *((int32_t*)((uintptr_t)(&___0_value)));
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m87BA7C578F96C8E49BB07088DAAC4649F83B0353_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___0_a, float ___1_d, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___0_a;
		float L_1 = L_0.___x;
		float L_2 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___0_a;
		float L_4 = L_3.___y;
		float L_5 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___0_a;
		float L_7 = L_6.___z;
		float L_8 = ___1_d;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
