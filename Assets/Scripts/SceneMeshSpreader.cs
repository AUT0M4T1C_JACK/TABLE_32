﻿using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using UnityEngine;

public class SceneMeshSpreader : MonoBehaviour
{
    [Tooltip("The number of items to spawn")]
    public float fireDensity = 0.5f;

    [Tooltip("The GameObject that you want to spread through the scene")]
    public GameObject spreadPrefab;


    [Tooltip("The transform to spawn fire")]
    public Transform spawnTransform;

    private void Start()
    {
        var sceneManager = FindObjectOfType<OVRSceneManager>();
        sceneManager.SceneModelLoadedSuccessfully += SpreadWithScene;
    }

    private async void SpreadWithScene()
    {
        foreach (Transform child in spawnTransform)
            Destroy(child.gameObject);
        
        var mainCamera = Camera.main;
        var anchors = new List<OVRAnchor>();
        
        await OVRAnchor.FetchAnchorsAsync<OVRRoomLayout>(anchors);

        var room = anchors.First();
        if (!room.TryGetComponent(out OVRAnchorContainer container))
            return;

        anchors.Clear();
        await container.FetchChildrenAsync(anchors);

        (Vector3 worldPos, Quaternion worldRotation, Vector2 size) floor = default;
        foreach (var anchor in anchors)
        {
            if (!anchor.TryGetComponent(out OVRSemanticLabels labels) ||
                !labels.Labels.Contains(OVRSceneManager.Classification.Floor))
                continue;

            // enable locatable/tracking
            if (!anchor.TryGetComponent(out OVRLocatable locatable))
                continue;
            await locatable.SetEnabledAsync(true);

            locatable.TryGetSceneAnchorPose(out var pose);
            if (pose.ComputeWorldPosition(mainCamera) is not { } floorWorldPos)
                continue;

            if (pose.ComputeWorldRotation(mainCamera) is not { } floorWorldRotation)
                continue;

            if (!anchor.TryGetComponent(out OVRBounded2D bounded2D))
                continue;

            var floorSize = bounded2D.BoundingBox.size;
            floor = (floorWorldPos, floorWorldRotation, floorSize);
            break;
        }

        var (pos, rot, size) = floor;
        var area = size.x * size.y;
        var firesToSpawn = Mathf.CeilToInt(area * fireDensity);
        for (var i = 0; i < firesToSpawn; i++)
        {
            var randomPos = FlameUtility.GetRandomSpawnPositionWithinRect(size.x, size.y);
            var unityForward = rot * Vector3.up;
            var unityRot = Quaternion.LookRotation(unityForward);
            var flameSpawnPos = (unityRot * randomPos) + pos;

            var flame = Instantiate(spreadPrefab, Vector3.zero, Quaternion.identity, spawnTransform);
            flame.transform.position = flameSpawnPos;
            flame.transform.rotation = unityRot;
        }
    }
}