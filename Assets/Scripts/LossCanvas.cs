using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LossCanvas : MonoBehaviour
{
    [SerializeField] private GameObject canvasGameObject;
    [SerializeField] private Transform centerEyeTransform;
    [SerializeField] private float posLerp = 0.5f;
    [SerializeField] private float rotLerp = 0.5f;
    [SerializeField] private float forwardDistance = 1f;

    private void Awake()
    {
        canvasGameObject.SetActive(false);
        var gameManager = FindObjectOfType<GameManager>();
        gameManager.onNewGameState.AddListener(OnNewGameState);
    }

    private void Update()
    {
        if (!canvasGameObject.activeSelf)
            return;
        
        var currentPos = canvasGameObject.transform.position;
        var currentRot = canvasGameObject.transform.rotation;
        var targetPos = centerEyeTransform.position + forwardDistance * centerEyeTransform.forward;
        var targetRot = Quaternion.LookRotation(centerEyeTransform.forward);
        canvasGameObject.transform.position = Vector3.Lerp(currentPos, targetPos, posLerp);
        canvasGameObject.transform.rotation = Quaternion.Lerp(currentRot, targetRot, rotLerp);
    }

    private void OnNewGameState(GameManager.GameState newGameState)
    {
        if (newGameState != GameManager.GameState.Loss)
            return;
        
        canvasGameObject.SetActive(true);
    }
}
