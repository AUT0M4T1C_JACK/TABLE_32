using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private GameObject signPrefab;
    [SerializeField] private DoorCanvas uiPrefab;
    [SerializeField] private Transform signTransform;
    [SerializeField] private Transform uiTransform;
    [SerializeField] private float signHeight;
    [SerializeField] private float uiForward = 0.1f;
    [SerializeField, TextArea] private string winText;

    private GameManager _gameManager;
    private DoorCanvas _doorCanvas;
    private void Awake()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _gameManager.onNewGameState.AddListener(OnNewGameState);
    }

    private void Start()
    {
        var signPos = signTransform.position + (signHeight * Vector3.up);
        Instantiate(signPrefab, signPos, signTransform.rotation);

        var uiPos = uiTransform.position + (uiForward * uiTransform.forward);
        _doorCanvas = Instantiate(uiPrefab, uiPos, uiTransform.rotation);
        _doorCanvas.gameObject.SetActive(false);
        
    }

    private void OnNewGameState(GameManager.GameState newGameState)
    {
        if (newGameState != GameManager.GameState.Win)
            return;

        _doorCanvas.gameObject.SetActive(true);
        _doorCanvas.winLabel.text = winText.Replace("XX%", $"{(int)_gameManager.health}%");
    }
}
