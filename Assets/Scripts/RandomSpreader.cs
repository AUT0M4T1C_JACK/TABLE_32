using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomSpreader : MonoBehaviour
{
    [Tooltip("The GameObject that you want to spread through the scene")]
    public GameObject spreadPrefab;

    [Tooltip("Where to spawn the spawn prefabs under")]
    public Transform spawnTransform;

    [Tooltip("The number of items to spawn")]
    public int numSpawns;

    [Tooltip("The size of the spawn region")]
    public Vector2 spawnRectSize;

    public void Spread()
    {
        // Remove existing children
        foreach (Transform fireTransform in spawnTransform)
        {
            Destroy(fireTransform.gameObject);
        }
        
        for (var i = 0; i < numSpawns; i++)
        {
            var randomSpawnPos = FlameUtility.GetRandomSpawnPositionWithinRect(spawnRectSize.x, spawnRectSize.y);
            Instantiate(spreadPrefab, randomSpawnPos, Quaternion.identity, spawnTransform);
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (!spawnTransform)
            return;

        Gizmos.matrix = spawnTransform.localToWorldMatrix;
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(spawnRectSize.x, 0, spawnRectSize.y));
    }
}