using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    [SerializeField] private TMP_Text scoreText;
    [SerializeField] private GameObject eyeAnchor;

    private GameManager _gameManager;

    private void Awake()
    {
        _gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = $"{_gameManager.health:F0}% survivable";
        scoreText.color = _gameManager.isOnFire ? Color.red : Color.white;
        transform.position = eyeAnchor.transform.position - new Vector3(0f, 1.75f, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Door"))
        {
            _gameManager.Win();
        }
        else if (other.gameObject.CompareTag("Fire"))
        {
            _gameManager.SetOnFire(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Fire"))
        {
            _gameManager.SetOnFire(false);
        }
    }
}