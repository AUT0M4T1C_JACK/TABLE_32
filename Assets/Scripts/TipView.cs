using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class TipView : MonoBehaviour
{
    [SerializeField] private RawImage image;
    [SerializeField] private TextMeshProUGUI label;
    [SerializeField] private Graphic background;
    private Coroutine _currentDismissCoroutine;
    public void SetTip(Texture icon, string tipText, Color backgroundColor)
    {
        image.enabled = icon;
        image.texture = icon;
        background.color = backgroundColor;

        label.text = tipText;
    }

    public void DismissAfterSeconds(float time, UnityAction onDismiss = null)
    {
        if (_currentDismissCoroutine != null)
            StopCoroutine(_currentDismissCoroutine);
        
        _currentDismissCoroutine = StartCoroutine(DismissCoroutine(time, onDismiss));
    }

    private IEnumerator DismissCoroutine(float time, UnityAction onDismiss)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
        onDismiss?.Invoke();
    }
}
