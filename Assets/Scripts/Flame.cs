using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flame : MonoBehaviour
{
    [SerializeField] private MeshRenderer renderer;

    public void SetColor(Color flameColor)
    {
        renderer.material.color = flameColor;
    }
}
