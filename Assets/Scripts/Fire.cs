using System.Collections;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Pool;

public class Fire : MonoBehaviour
{
    [SerializeField] private Flame flamePrefab;
    
    [Header("Flame Spawn")]
    [SerializeField] private float spawnRadius;
    [SerializeField] private float minFlameLifetime = 1f;
    [SerializeField] private float maxFlameLifetime = 3f;
    [SerializeField] private int maxFlames = 50;
    [SerializeField] private int maxFlameSpawnPerTick = 10;
    [SerializeField] private float flameSpawnInterval = 0.2f;

    [Header("Flame Height")] [SerializeField]
    private float minFlameHeight = 1.5f;

    [SerializeField] private float maxFlameHeight = 2.5f;

    [Header("Flame Scale")] [SerializeField]
    private float minFlameScale = 0.25f;

    [SerializeField] private float maxFlameScale = 0.5f;


    [SerializeField] private Gradient flameGradient;

    [Header("Flame Curves")] [SerializeField]
    private AnimationCurve flameRiseCurve;

    [SerializeField] private AnimationCurve flameScaleCurve;

    private ObjectPool<Flame> flamePool;

    private void Awake()
    {
        flamePool = new ObjectPool<Flame>(CreateFlame, OnFlameTake, OnFlameReturn, OnDestroyFlame);
    }

    private void Start()
    {
        StartCoroutine(SpawnFlamesCoroutine());
    }

    private IEnumerator SpawnFlamesCoroutine()
    {
        while (true)
        {
            var active = flamePool.CountActive;
            var flamesToSpawn = Mathf.Min(maxFlames - active, maxFlameSpawnPerTick);
            for (var i = 0; i < flamesToSpawn; i++)
            {
                var flame = flamePool.Get();
                StartCoroutine(SpawnFlameCoroutine(flame));
            }

            yield return new WaitForSeconds(flameSpawnInterval);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(spawnRadius, 0, spawnRadius));
    }

    private IEnumerator SpawnFlameCoroutine(Flame flame)
    {
        var elapsed = 0f;
        var spawnPos = FlameUtility.GetRandomSpawnPositionWithinRect(spawnRadius);
        flame.transform.localPosition = spawnPos;

        // scale the flame
        var flameScale = Random.Range(minFlameScale, maxFlameScale);
        var flameTransform = flame.transform;
        flameTransform.localScale *= flameScale;

        // add color
        var flameColor = flameGradient.Evaluate(Random.value);
        flame.SetColor(flameColor);

        // calculate height
        var flameHeight = Random.Range(minFlameHeight, maxFlameHeight);
        
        // calculate lifetime
        var lifetime = Random.Range(minFlameLifetime, maxFlameLifetime);

        while (elapsed <= lifetime)
        {
            var elapsedT = elapsed / lifetime;

            // Make the flame rise
            var riseT = flameRiseCurve.Evaluate(elapsedT);
            var newPos = flame.transform.localPosition;
            newPos.y = riseT * flameHeight;
            flameTransform.localPosition = newPos;

            // Scale the flame
            var scaleT = flameScaleCurve.Evaluate(elapsedT);
            flameTransform.localScale = Vector3.one * (flameScale * scaleT);

            elapsed += Time.deltaTime;
            yield return null;
        }

        flamePool.Release(flame);
    }
    
    #region Flame Pool

    private Flame CreateFlame()
    {
        var flame = Instantiate(flamePrefab, Vector3.zero, Quaternion.identity, transform);

        return flame;
    }

    // Called when an item is taken from the pool using Get
    private void OnFlameTake(Flame flame)
    {
        flame.gameObject.SetActive(true);
    }
    
    // Called when an item is returned to the pool using Release
    private void OnFlameReturn(Flame flame)
    {
        flame.gameObject.SetActive(false);
    }

    private void OnDestroyFlame(Flame flame)
    {
        Destroy(flame.gameObject);
    }

    #endregion
}