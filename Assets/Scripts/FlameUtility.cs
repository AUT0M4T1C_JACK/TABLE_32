﻿using UnityEngine;

namespace DefaultNamespace
{
    public static class FlameUtility
    {
        /// <summary>
        /// Randomly samples a spawn location from within the specified region.
        /// </summary>
        /// <returns>The random spawn location</returns>
        public static Vector3 GetRandomSpawnPositionWithinRect(float width, float height)
        {
            var halfWidth =  width / 2f;
            var halfHeight = height/ 2f;
            var x = Random.Range(-halfWidth, halfWidth);
            var z = Random.Range(-halfHeight, halfHeight);
            var pos = new Vector3(x, 0, z);

            return pos;
        }

        public static Vector3 GetRandomSpawnPositionWithinRect(float squareLength) =>
            GetRandomSpawnPositionWithinRect(squareLength, squareLength);
    }
}