using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TipManager : MonoBehaviour
{
    public Tip[] tips;
    [SerializeField] private HorizontalOrVerticalLayoutGroup tipContainer;
    [SerializeField] private TipView tipViewPrefab;
    [SerializeField] private AudioSource audioSource;

    private Dictionary<string, TipView> visibleKeys = new();

    // Start is called before the first frame update
    void Start()
    {
        foreach (var tip in tips.Where(t => t.spawnOnStart))
        {
            InstantiateTip(tip);
        }
    }

    public void ShowTip(string key)
    {
        var tip = tips.FirstOrDefault(t => t.key == key);
        if (tip == null)
            return;
        
        InstantiateTip(tip);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            ShowTip("fire");
    }

    private void InstantiateTip(Tip tip)
    {
        var keyValid = !string.IsNullOrWhiteSpace(tip.key);

        TipView view;
        if (keyValid && visibleKeys.TryGetValue(tip.key, out var existingTipView))
        {
            view = existingTipView;
        }
        else
        {
            view = Instantiate(tipViewPrefab, tipContainer.transform);
            var viewTransform = view.transform;
            viewTransform.localPosition = Vector3.zero;
            viewTransform.localRotation = Quaternion.identity;
            viewTransform.localScale = Vector3.one;
            view.SetTip(tip.icon, tip.text, tip.backgroundColor);
        }
        
        if (keyValid)
            visibleKeys[tip.key] = view;

        // Play a sound to indicate a new notification
        if (tip.duration > 0f)
        {
            audioSource.Play();

            // Queue dismissal
            view.DismissAfterSeconds(tip.duration, () =>
            {
                if (keyValid)
                    visibleKeys.Remove(tip.key);
            });
        }
    }

    [Serializable]
    public class Tip
    {
        public string key;
        public bool spawnOnStart;
        [TextArea]
        public string text;
        public Texture icon;
        public Color backgroundColor = Color.clear;
        public float duration;
    }
}
