using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public float health = 100f;
    public bool isOnFire = false;
    public float idleDrainRate = 0.5f;
    public float fireDrainRate = 5f;
    public string fireTipName = "fire";
    public GameState gameState = GameState.Running;
    public UnityEvent<GameState> onNewGameState;

    public enum GameState
    {
        Running,
        Win,
        Loss
    }

    private TipManager _tipManager;
    
    private void Awake()
    {
        _tipManager = FindObjectOfType<TipManager>();
    }
    

    // Update is called once per frame
    void Update()
    {
        if (gameState != GameState.Running)
            return;
        
        var newHealth = Mathf.Max(0, health - ((isOnFire ? fireDrainRate : idleDrainRate) * Time.deltaTime));
        
        // Check loss
        if (newHealth <= 0f && health > 0f)
        {
            SetGameState(GameState.Loss);
        }
        
        health = newHealth;
    }

    public void SetOnFire(bool onFire)
    {
        if (isOnFire == onFire)
            return;

        isOnFire = onFire;
        if (isOnFire)
        {
            _tipManager.ShowTip(fireTipName);
        }
    }

    public void Win()
    {
        if (gameState != GameState.Loss)
            SetGameState(GameState.Win);
    }

    private void SetGameState(GameState newGameState)
    {
        gameState = newGameState;
        onNewGameState.Invoke(gameState);
    }
}