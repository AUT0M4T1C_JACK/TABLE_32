using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class DoorFrame
{
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 scale;
    
    public DoorFrame(Vector3 position, Quaternion rotation, Vector3 scale)
    {
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }
}
public class DoorSpawner : MonoBehaviour
{
    //List<OVRSceneAnchor> m_sceneAnchors = new List<OVRSceneAnchor>();
    public List<DoorFrame> DoorFrames = new List<DoorFrame>();
    void Start()
    {
        LoadDoorAnchors();
        /*OVRSceneAnchor.GetSceneAnchors(m_sceneAnchors);
        foreach (var anchor in m_sceneAnchors) { 
            anchor.Anchor.
        }*/

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    async void LoadDoorAnchors()
    {
        var anchors = new List<OVRAnchor>();
        await OVRAnchor.FetchAnchorsAsync<OVRRoomLayout>(anchors);

        // no rooms - call Space Setup or check Scene permission
        if (anchors.Count == 0)
            return;

        // get the component to access its data
        var room = anchors.First();
        if (!room.TryGetComponent(out OVRAnchorContainer container))
            return;

        // use the component helper function to access all child anchors
        await container.FetchChildrenAsync(anchors);

        foreach (var roomAnchor in anchors)
        {
            // check that this anchor is the floor
            if (!roomAnchor.TryGetComponent(out OVRSemanticLabels labels) ||
                !labels.Labels.Contains(OVRSceneManager.Classification.DoorFrame))
            {
                continue;
            }

            // enable locatable/tracking
            if (!roomAnchor.TryGetComponent(out OVRLocatable locatable))
                continue;
            await locatable.SetEnabledAsync(true);

            // localize the anchor
            locatable.TryGetSceneAnchorPose(out var pose);
            var doorFrame = new DoorFrame(pose.ComputeWorldPosition(Camera.main).Value, pose.ComputeWorldRotation(Camera.main).Value, Vector3.zero);
            

            // get the floor dimensions
            roomAnchor.TryGetComponent(out OVRBounded2D bounded2D);
            var size = bounded2D.BoundingBox.size;

            doorFrame.scale = size;

            DoorFrames.Add(doorFrame);
            
            // spawning le door
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = doorFrame.position;
            cube.transform.rotation = doorFrame.rotation;
            cube.transform.localScale = doorFrame.scale;
            cube.gameObject.name = "Le Door";
            print("Le Door");
            // only interested in the first floor anchor
            break;
        }
    }
}
