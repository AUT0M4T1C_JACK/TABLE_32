using System;
using System.Collections;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Pool;
using Random = UnityEngine.Random;

public class FastFire : MonoBehaviour
{
    [SerializeField] private Mesh flameMesh;
    [SerializeField] private Material flameMaterial;

    [Header("Flame Spawn")] [SerializeField]
    private float spawnRadius;

    [SerializeField] private float minFlameLifetime = 1f;
    [SerializeField] private float maxFlameLifetime = 3f;
    [SerializeField] private int maxFlames = 50;
    [SerializeField] private int maxFlameSpawnPerTick = 10;
    [SerializeField] private float flameSpawnInterval = 0.2f;

    [Header("Flame Height")] [SerializeField]
    private float minFlameHeight = 1.5f;

    [SerializeField] private float maxFlameHeight = 2.5f;

    [Header("Flame Scale")] [SerializeField]
    private float minFlameScale = 0.25f;

    [SerializeField] private float maxFlameScale = 0.5f;


    [SerializeField] private Gradient flameGradient;

    [Header("Flame Curves")] [SerializeField]
    private AnimationCurve flameRiseCurve;

    [SerializeField] private AnimationCurve flameScaleCurve;

    private FlameState[] _flames;
    private Matrix4x4[] _matrices;
    private Vector4[] _colors;
    private static readonly int s_colors = Shader.PropertyToID("_Colors");

    private void Awake()
    {
        _flames = new FlameState[maxFlames];
        _matrices = new Matrix4x4[maxFlames];
        _colors = new Vector4[maxFlames];

        for (var i = 0; i < _flames.Length; i++)
        {
            InitFlame(ref _flames[i]);
        }
    }

    private void FixedUpdate()
    {
        var localToWorld = transform.localToWorldMatrix;
        for (var i = 0; i < _flames.Length; i++)
        {
            var done = _flames[i].Tick(
                ref _matrices[i], out _colors[i],  localToWorld, flameRiseCurve, flameScaleCurve);
            if (done)
                InitFlame(ref _flames[i]);
        }
    }

    private void Update()
    {
        Graphics.DrawMeshInstanced(flameMesh, 0, flameMaterial, _matrices);
    }

    private void InitFlame(ref FlameState state)
    {
        state.elapsed = 0f;
        state.flamePos = FlameUtility.GetRandomSpawnPositionWithinRect(spawnRadius);

        // scale the flame
        state.flameScale = Random.Range(minFlameScale, maxFlameScale);

        // add color
        state.flameColor = flameGradient.Evaluate(Random.value);

        // calculate height
        state.flameHeight = Random.Range(minFlameHeight, maxFlameHeight);

        // calculate lifetime
        state.flameLifetime = Random.Range(minFlameLifetime, maxFlameLifetime);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(Vector3.zero, new Vector3(spawnRadius, 0, spawnRadius));
    }

    private struct FlameState
    {
        public float elapsed;
        public float flameLifetime;
        public float flameHeight;
        public float flameScale;
        public Color flameColor;
        public Vector3 flamePos;

        public bool Tick(ref Matrix4x4 matrix, out Vector4 color, in Matrix4x4 parentMatrix, AnimationCurve riseCurve,
            AnimationCurve scaleCurve)
        {
            color = flameColor;

            if (elapsed > flameLifetime)
                return true;

            var elapsedT = elapsed / flameLifetime;

            // Make the flame rise
            var riseT = riseCurve.Evaluate(elapsedT);
            flamePos.y = riseT * flameHeight;

            // Scale the flame
            var scaleT = scaleCurve.Evaluate(elapsedT);
            var flameScaleVector = Vector3.one * (flameScale * scaleT);

            var fireMatrix = Matrix4x4.identity;
            fireMatrix.SetTRS(flamePos, Quaternion.identity, flameScaleVector);
            matrix = parentMatrix * fireMatrix;

            elapsed += Time.deltaTime;

            return false;
        }
    }
}