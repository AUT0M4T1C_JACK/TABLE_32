using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void LoadAScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void ResetGaurdian()
    {
        var sceneManager = FindObjectOfType<OVRSceneManager>();
        if (sceneManager.RequestSceneCapture()) Debug.Log("Scene Loaded Successfully");
        else Debug.Log("Scene Loaded Unsuccessfully");
    }
}
