using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogApplier : MonoBehaviour
{
    public Camera cam;
    private MeshRenderer mesh;
    void Start()
    {
        mesh = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        mesh.material.SetVector("_CameraPosition", cam.transform.position);
    }
}
